import asyncio
import aiohttp
import pytest
from unittest import mock
from unittest.mock import call
import json
import time
import serial
import sys

import fuzzy.itf
import fuzzy

@pytest.yield_fixture
def fz(event_loop, request, mocker, tmpdir, unused_tcp_port, passer):

    rest = fuzzy.itf.rest.REST(event_loop, unused_tcp_port)

    mocker.patch.object(fuzzy.fuzzy.can, "CAN", spec=fuzzy.itf.can.CAN)
    mocker.patch.object(fuzzy.fuzzy.rest, "REST", return_value=rest)

    f = fuzzy.fuzzy.Fuzzy(
        loop=event_loop,
        web_dir=str(tmpdir))
    yield f
    f.stop()

def test_fuzzy_launches_all(fz, event_loop, mocker, tmpdir):

    assert fuzzy.fuzzy.can.CAN.call_count == 1
    assert fuzzy.fuzzy.can.CAN.call_args[0] == ("vcan0", 1, event_loop)

    assert fuzzy.fuzzy.rest.REST.call_count == 1
    assert fuzzy.fuzzy.rest.REST.call_args[0] == (event_loop, 8080)
