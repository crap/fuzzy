import pytest
import math
import fuzzy.geo as geo

def test_vector():
    v = geo.Vector()
    assert v == [0, 0, 0]

    v = geo.Vector([1, 2, 3])
    assert v == [1, 2, 3]

    v = geo.Vector(1, 2, 3)
    assert v == [1, 2, 3]

    v2 = geo.Vector(v)
    assert v2 == [1, 2, 3]

    assert v.x == 1
    assert v.y == 2
    assert v.theta == 3

    v.x = 10
    assert v == [10, 2, 3]
    v.y = 10
    assert v == [10, 10, 3]
    v.theta = 10
    assert v == [10, 10, 10]

    assert not(v == 1.2)

    v = geo.Vector()
    v[1] = 2.2
    assert v == [0, 2.2, 0]

def test_vector_xy_norm():
    v = geo.Vector(0, 1, 0)
    assert v.xy_norm == 1

    v = geo.Vector(1, 1, 1000)
    assert v.xy_norm == math.sqrt(2)

def test_vector_xy_angle():
    v = geo.Vector(0, 1, 0)
    assert v.xy_angle == math.pi/2

def test_vector_th_norm():
    v = geo.Vector(0, 1, 0)
    assert v.theta_norm == 0

    v = geo.Vector(1, 1, -1000)
    assert v.theta_norm == 1000

def test_vector_diff():
    v1 = geo.Vector()
    v2 = geo.Vector(0, 1, 0)

    v = v2 - v1
    assert v == [0, 1, 0]

    # With theta being None
    v1 = geo.Vector()
    v2 = geo.Vector(0, 1, None)

    v = v2 - v1
    assert v == [0, 1, None]

def test_vector_sum():
    v1 = geo.Vector()
    v2 = geo.Vector(0, 1, 0)

    v3 = v1 + v2
    assert v3 == v2

def test_vector_mult():
    v = geo.Vector(1, 2, 3)
    v *= -2.5
    assert v == [-2.5, -5, -7.5]

    v = geo.Vector(10, 20, 30)
    v /= 10
    assert v == [1, 2, 3]

    v = geo.Vector(1, 2, None)
    v *= 3
    assert v == [3, 6, None]

    v = geo.Vector(1, 2, None)
    v = 3 * v
    assert v == [3, 6, None]

def test_vector_unit():
    v = geo.Vector(1, 0, -2)
    assert v.unit == [1, 0, -1]

    v = geo.Vector(1, 1, 1)
    assert v.unit == [1/math.sqrt(2), 1/math.sqrt(2), 1]

    v = geo.Vector()
    assert v.unit == [0, 0, 0]

def test_vector_repr():
    v = geo.Vector(1, 2, 3)
    assert repr(v) == "Vector(1, 2, 3)"

def test_vector_round():
    v = geo.Vector(1.1, 2.9, 3.01)
    v.round()
    assert v == [1, 3, 3]


    v = geo.Vector(1.5, 2.1, None)
    v.round()
    assert v == [2, 2, None]

def test_vector_only():
    v = geo.Vector(1, 2, 3)
    assert v.only_xy == [1, 2, 0]
    assert v.only_theta == [0, 0, 3]

def test_vector_theta_wraps():
    v = geo.Vector(0, 0, 4)
    v.mod2pi()
    assert v.theta == 4-2*math.pi

    v = geo.Vector(0, 0, -4)
    v.mod2pi()
    assert v.theta == -4 + 2*math.pi

    v = geo.Vector(0, 0, 3)
    v.mod2pi()
    assert v.theta == 3

    v = geo.Vector(0, 0, -3)
    v.mod2pi()
    assert v.theta == -3

def test_vector_rotate(is_close):
    v = geo.Vector(1, 0, 2)
    v.rotate(math.pi/2)
    assert is_close(v, [0, 1, 2])

def test_vector_rotated(is_close):
    v = geo.Vector(1, 0, 2)
    assert is_close(v.rotated(math.pi/2), [0, 1, 2])

def test_errors():
    v1 = geo.Vector()
    with pytest.raises(TypeError):
        v1 + 2
    with pytest.raises(TypeError):
        v1 - 2

def test_replace():
    v = geo.Vector(0, None, 0)
    v.replace(None, 5)
    assert v == [0, 5, 0]

    v.replace(5, None)
    assert v == [0, None, 0]

def test_cross():
    # geo-wise
    a = geo.Vector(1, 0)
    b = geo.Vector(0, 1)

    assert a.cross(b) == 1

    # counter geo wise
    a = geo.Vector(1, 0)
    b = geo.Vector(0, -1)

    assert a.cross(b) == -1

    # Colinear
    a = geo.Vector(1, 0)
    b = geo.Vector(1, 0)

    assert a.cross(b) == 0


def test_dot():
    # geo-wise
    a = geo.Vector(1, 0)
    b = geo.Vector(0, 1)

    assert a.dot(b) == 0

    # counter geo wise
    a = geo.Vector(1, 0)
    b = geo.Vector(0, -1)

    assert a.dot(b) == 0

    # Colinear
    a = geo.Vector(1, 0)
    b = geo.Vector(1, 0)

    assert a.dot(b) == 1

def test_str_repr():
    x = geo.Vector(1, 2, 3)
    assert str(x) == "<1.000, 2.000, 3.000>"

    x = geo.Vector(1, 2, None)
    assert str(x) == "<1.000, 2.000>"

def test_json():
    x = geo.Vector(1, 2, 3)
    assert x.to_json() == {"x": 1, "y": 2, "theta": 3}

    y = geo.Vector.from_json( {"x": 1, "y": 2, "theta": 3} )
    assert y == x

def test_reverse():
    x = geo.Vector(1, 2, 3)
    y = x.reversed()
    assert y == geo.Vector(1, 1, 3)

def test_hash():
    l = set([geo.Vector(0, 1), geo.Vector(1, 0)])
    assert geo.Vector(0, 1) in l
    assert geo.Vector(1, 1) not in l
