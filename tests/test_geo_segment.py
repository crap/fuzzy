import pytest
import math
from fuzzy.geo import Vector, Segment

def test_print():
    x = Segment(Vector(0, 0), Vector(10, 10))
    str(x)
    repr(x)

def test_translate():
    s1 = Segment(Vector(0, 0), Vector(10, 10))

    s1.translate(Vector(1, 2))

    assert s1.A == Vector(1, 2)
    assert s1.B == Vector(11, 12)
    assert s1.x == 10
    assert s1.y == 10

def test_seg_intersect(is_close):
    s1 = Segment(Vector(0, 0), Vector(10, 10))
    s2 = Segment(Vector(10, 0), Vector(0, 10))

    assert s1.intersect(s2) == Vector(5, 5)
    assert s2.intersect(s1) == Vector(5, 5)

    s2.translate(Vector(100, 100))
    assert s1.intersect(s2) == None

    s1 = Segment(Vector(1,0.2), Vector(1, 0.6))
    s2 = Segment(Vector(0.5,0.4), Vector(1.5,0.4))
    assert is_close(s1.intersect(s2), Vector(1, 0.4))

    # Test parallel
    s1 = Segment(Vector(0, 0), Vector(1, 0))
    s2 = Segment(Vector(0, 1), Vector(1, 1))
    assert s1.intersect(s2) == None


def test_closest_point():
    l = Segment(Vector(0, 2), Vector(5, 2))

    # Closest in segment
    assert l.closest_point(Vector(3, 2)) == Vector(3, 2)
    assert l.closest_point(Vector(3, 100)) == Vector(3, 2)

    # Closest outside segment
    assert l.closest_point(Vector(-1, 3), respect_boundaries=True) == Vector(0, 2)
    assert l.closest_point(Vector(6, 0), respect_boundaries=True) == Vector(5, 2)

    assert l.closest_point(Vector(-1, 3)) == Vector(-1, 2)
    assert l.closest_point(Vector(6, 0)) == Vector(6, 2)

def test_no_collision():
    obs = Segment(Vector(1,0.2), Vector(1, 0.6))
    obs.translate(Vector(0.7, 0))
    mov = Segment(Vector(0.5,0.4), Vector(1.5,0.4))

    obs.circle_moving_collision(mov, 0.1) == None

    obs = Segment(Vector(1,0.2), Vector(1, 0.6))
    obs.translate(Vector(0, 0.35))
    mov = Segment(Vector(0.5,0.4), Vector(1.5,0.4))

    obs.circle_moving_collision(mov, 0.1) == None

def test_simple_collision(is_close):
    obs = Segment(Vector(1,0.2), Vector(1, 0.6))
    mov = Segment(Vector(0.5,0.4), Vector(1.5,0.4))

    assert is_close(
        obs.circle_moving_collision(mov, 0.1) ,
        Vector(0.9, 0.4)
        )

def test_A_collision(is_close):
    obs = Segment(Vector(1,0.45), Vector(1, 0.85))
    mov = Segment(Vector(0.5,0.4), Vector(1.5,0.4))

    assert is_close(
        obs.circle_moving_collision(mov, 0.1) ,
        Vector(1-(0.1**2 - 0.05**2)**0.5, 0.4)
        )

def test_B_collision(is_close):
    obs = Segment(Vector(1,0), Vector(1, 0.35))
    mov = Segment(Vector(0.5,0.4), Vector(1.5,0.4))

    assert is_close(
        obs.circle_moving_collision(mov, 0.1) ,
        Vector(1-(0.1**2 - 0.05**2)**0.5, 0.4)
        )

def test_start_collision(is_close):
    obs = Segment(Vector(1, 0), Vector(1, 0.35))
    mov = Segment(Vector(1, 0), Vector(1.5,0.4))

    assert is_close(
        obs.circle_moving_collision(mov, 0.1) ,
        Vector(1, 0)
        )

def test_collision_real():
    obs = Segment(Vector(0.75, 2.1), Vector(0.75, 0.9))
    mov = Segment(Vector(0.57, 1.144), Vector(1, 0.473))

    assert obs.circle_moving_collision(mov, 0.15) is not None

def test_collision_real_2():
    obs1 = Segment(Vector(0.3, 0.5), Vector(1.3, 0.5))
    obs2 = Segment(Vector(1.3, 0.5), Vector(1.3, 0.6))
    mov = Segment(Vector(1.4, 0.4), Vector(0.6, 1))

    x = obs1.circle_moving_collision(mov, 0.1)
    print("AB collision at ", x, "d=", (x - mov.A).xy_norm)

    x = obs2.circle_moving_collision(mov, 0.1)
    print("BC collision at ", x, "d=", (x - mov.A).xy_norm)
