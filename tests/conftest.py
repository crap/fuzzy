import pytest
import asyncio

import fuzzy.itf.rest

import logging
logging.basicConfig(format='TEST %(levelname)s:%(message)s', level=logging.DEBUG)


def _is_close(obj, other_obj, p=1e-4):
    if isinstance(obj, (int, float)):
        return abs(obj-other_obj) < p

    if isinstance(obj, dict):
        return all(_is_close(obj[k], other_obj[k], p) for k in obj.keys())

    if isinstance(obj, str):
        return obj == other_obj

    try:
        for (a, b) in zip(obj, other_obj):
            if not _is_close(a, b, p): return False
        return True
    except TypeError:
        pass

    return obj == other_obj

@pytest.fixture
def is_close():
    return _is_close

@pytest.fixture
def passerz():
    def p1(*args):
        x = args
        async def p(*args, **kwargs):
            if len(x) == 1: return x[0]
            else: return x
        return p

    return p1

@pytest.fixture
def passer(passerz):
    return passerz()

@pytest.yield_fixture
def restitf(request, event_loop, unused_tcp_port):
    rest = fuzzy.itf.rest.REST(event_loop, unused_tcp_port)
    event_loop.run_until_complete(rest.start())
    yield rest
    try:
        event_loop.run_until_complete(rest.stop())
    except Exception: pass
