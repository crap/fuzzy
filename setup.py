#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='crap-fuzzy',
      version='2.0.363',
      description='CRAP Robot control software',
      author='CRAP team',
      author_email='crap@crap.org',
      packages=find_packages(),
      install_requires=['pyserial',
                        'aiohttp',
                        ],
      entry_points={
        'console_scripts': [
            'fuzzy=fuzzy.main:main',
            'hokcmd=fuzzy.detect.hokcmd:main',
        ],
      }
      )
