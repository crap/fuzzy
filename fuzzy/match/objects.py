import logging
import math
from fuzzy import geo
from fuzzy import color

logger = logging.getLogger("objects")


class ObjectWrapper(geo.Vector):
    def reversed(self, mode):
        v = geo.Vector.reversed(self, mode)
        return self.__class__(*v._val)


class Distributor(ObjectWrapper):
    def __init__(self, x, y, theta, mixed=False):
        geo.Vector.__init__(self, x, y, theta)
        self.mixed = mixed

    def reversed(self, mode):
        v = ObjectWrapper.reversed(self, mode)
        v.mixed = self.mixed
        return v


class BeeStart(ObjectWrapper):
    pass


class Switch(ObjectWrapper):
    pass


class Cubes(ObjectWrapper):
    pass


class ConstructionZone(ObjectWrapper):
    X_WIDTH = 180
    Y_WIDTH = 560


# Known distributors
distrib_green = Distributor(0.840, 0.0725, 0, mixed=False)
distrib_orange_green = Distributor(2-0.0725, 0.610, -math.pi/2, mixed=True)

# Bee start
beestart_green = BeeStart(2, 0.21, math.pi/2)

# Pannel Switch
switch_green = Switch(0, 1.13, math.pi)

# Cubes
cubes_green = [
    Cubes(0.54, 0.85, math.pi/4),
    Cubes(1.19, 0.30, -math.pi/4),
    Cubes(1.50, 0.11, math.pi/4),
]

# Zone
construct_green = ConstructionZone(0.09, 0.67, math.pi)


class ColorObjects:
    def __init__(self, distrib, mixed_distrib, bee_start, switch, cubes,
                 construction_zone):
        self.distrib = distrib
        self.mixed_distrib = mixed_distrib
        self.bee_start = bee_start
        self.switch = switch
        self.cubes = cubes
        self.construction_zone = construction_zone

    def reversed(self):
        d = {}
        for k, v in self.__dict__.items():
            if isinstance(v, list):
                d[k] = [x.reversed("oppo") for x in v]
            elif isinstance(v, dict):
                for k2, v2 in v.items():
                    v[k2] = v2.reversed("oppo")
            else:
                d[k] = v.reversed("oppo")

        return ColorObjects(**d)


green_objects = ColorObjects(
    distrib_green,
    distrib_orange_green.reversed("oppo"),
    beestart_green,
    switch_green,
    cubes_green,
    construct_green
    )

objects = {
    color.Color.green: green_objects,
    color.Color.orange: green_objects.reversed()
}
