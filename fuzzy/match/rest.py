import json
import logging
import time

import aiohttp

logger = logging.getLogger("MatchRest")


class MatchRest:
    def __init__(self):
        self._rest = self._fz.rest
        self._rest.register_get("/match", self._handle_get)
        self._rest.register_put("/match/scan", self._handle_scan)
        self._rest.register_put("/match/start", self._handle_start)
        self._rest.register_put("/match/reset", self._handle_reset)
        self._rest.register_get("/match/jobs/{job}", self._handle_get_job)
        self._rest.register_put("/match/jobs/{job}/execute",
                                self._handle_execute_job)

        self._rest.register_websocket("/match/scanner",
                                      self._handle_ws_events)

        # List of connected websockets
        self._scanner_ws = []
        self._scanner_last_sent = time.time()

    async def _handle_get(self, req):
        dat = {
            "started": self.started,
            "remaining": self.remaining,
            "jobs": [str(i) for i in self._jobs]
        }
        return dat

    async def _handle_scan(self, req):
        if self._fz.obst_scanner:
            self._fz.obst_scanner.start()
        return "OK"

    async def _handle_start(self, req):
        req = await req.json()
        self.start(**req)
        return "OK"

    async def _handle_reset(self, req):
        self.reset()
        return "OK"

    async def _handle_get_job(self, req):
        job = req.match_info['job']
        logger.debug("GET job {}".format(job))

        jobs = filter(lambda x: str(x) == job, self.jobs)
        try:
            job = next(jobs)
        except StopIteration:
            return self._rest.resp_404()

        dat = {
            "start": job.start_position().to_json(),
            "points": job.points()
            }
        return dat

    async def _handle_execute_job(self, req):
        job = req.match_info['job']
        logger.info("Execute job {}".format(job))

        jobs = filter(lambda x: str(x) == job, self.jobs)

        try:
            job = next(jobs)
        except StopIteration:
            return self._rest.resp_404()

        p_start = job.start_position()
        await self._run_single_job(job, p_start)

        return "OK"

    async def _handle_ws_events(self, ws):
        self._scanner_ws.append(ws)
        while True:
            # no inbound message
            msg = await ws.receive()
            if msg.tp in (aiohttp.MsgType.closed,
                          aiohttp.MsgType.error):
                break

        print("WS closed: ", ws)
        self._scanner_ws.remove(ws)

    def _ws_notify_clusters(self, clusters, scan):
        dt = time.time() - self._scanner_last_sent
        if dt < 0.3:
            return

        dat = {
            "scan": scan.json,
            "clusters": [c.json for c in clusters],
        }

        dat = json.dumps(dat)

        for ws in self._scanner_ws:
            try:
                ws.send_str(dat)
            except Exception:
                logger.exception("Removing websocket {}".format(ws))
                self._scanner_ws.remove(ws)
