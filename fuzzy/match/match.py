import asyncio
import logging
import time

from .rest import MatchRest
from .job import Job

from fuzzy.move import GotoTolerance, MoveTimeout, MoveNoPathFound

logger = logging.getLogger("Match")


class Match(MatchRest):
    DURATION = 100
    MOVE_TO_START_TIMEOUT = 10

    def __init__(self, fz):
        self._fz = fz
        self._loop = fz._loop

        MatchRest.__init__(self)

        # Time counting
        self._started = False
        self._end_time = 0
        self._end_wait = None

        # Jobs to-do or not-do
        self._jobs = []
        self._job_task = None
        self._cur_job = None
        self._done_jobs = []
        self._skip_job = None

        self._failed_jobs = {}

        # Handlers
        self._stop_handlers = set()

        # Travel
        self._travel_tol = GotoTolerance(
            vmax=1,
            dist=0.05,
            theta=0.10,
            num=1
            )

    def add_job(self, job):
        """Add an job to the list of jobectives to do"""
        if job not in self._jobs:
            self._jobs.append(job)

    def clear_jobs(self):
        """Clear the internal job list"""
        del self._jobs[:]

    @property
    def jobs(self):
        """Get a list of all jobs"""
        return list(self._jobs)

    def is_job_done(self, job):
        """Tell if the given job is done"""
        return job in self._done_jobs

    def update_color(self):
        """Update all jobs to indicate the new color"""
        if self._started:
            logger.warning("Can't update color if match started!!!")
            return

        # Notify all jobs
        for o in self._jobs:
            o.update_color(self._fz.color)

    @property
    def started(self):
        """Indicate if match has started, may be finished!"""
        return self._started

    @property
    def remaining(self):
        """Return the remaining time in the match"""
        if not self._started:
            return Match.DURATION

        rem = self._end_time - time.time()
        if rem < 0:
            return 0
        return rem

    def add_stopped_handler(self, handler):
        """Add a handler to be called on match stopped"""
        self._stop_handlers.add(handler)

    def start(self, duration=None):
        """Start the match for the given duration"""

        if duration is None:
            duration = Match.DURATION

        if self.started:
            logger.warning("Trying to start match again")
            raise Exception("Match Already Started")

        logger.info("Starting match for {}s".format(duration))
        self._fz.model.led_start()

        self._started = True
        self._end_time = time.time() + duration

        if self._fz.obst_scanner:
            self._fz.obst_scanner.start()

        self._end_wait = asyncio.ensure_future(
            self.wait_until_stop(),
            loop=self._loop)

        logger.info("Match Started")

        # Run jobs
        self._job_task = asyncio.ensure_future(
            self._run_jobs(),
            loop=self._loop
            )

    def reset(self):
        """Abort current match, and prepare for next"""
        if not self._started:
            return

        # stop obstacle detection
        if self._fz.obst_scanner:
            self._fz.obst_scanner.stop()

        # Stop end timer
        self._started = False
        self._end_time = 0
        self._end_wait.cancel()

        # Reset job runner
        if self._job_task:
            self._job_task.cancel()
            self._job_task = None
        self._cur_job = None

        # Reset all jobs
        for o in self._jobs:
            o.reset()

        # Reset done list
        self._done_jobs.clear()

    async def _run_jobs(self):
        """Elect next job and run, until end"""

        while self.remaining > 0:
            try:
                best_o, p_start = self._elect_next_job()
            except Exception:
                logger.exception("Failed to elect")
                best_o = None

            if best_o:
                logger.info("Elected next job {}, moving to {}"
                            .format(best_o, p_start))
                self._cur_job = best_o

                success = await self._run_single_job(self._cur_job, p_start)

                if success is not True:
                    self._skip_job = self._cur_job

                    if success != Job.Reschedule:
                        # Mark job as failed
                        fail_count = self._failed_jobs.get(self._cur_job, 0)
                        fail_count += 1
                        self._failed_jobs[self._cur_job] = fail_count
                        logger.info("Job {} marked as failed {} times"
                                    .format(self._cur_job,
                                            self._failed_jobs[self._cur_job]))

                    self._cur_job = None
                    # Wait a litle and chose next
                    await asyncio.sleep(0.2)
                    continue

                logger.info("Job {} appended to done list"
                            .format(self._cur_job))
                self._done_jobs.append(self._cur_job)
                self._cur_job = None
            else:
                logger.warning("No job to do, sleeping a little")
                await asyncio.sleep(0.5)

    def _elect_next_job(self):
        """Select best next job, based on stats"""
        best_o = None
        best_start = None

        for o in self._jobs:
            # Don't re-do done jobs
            if o in self._done_jobs:
                continue

            if o == self._skip_job:
                self._skip_job = None
                continue

            if self._failed_jobs.get(o, 0) >= 2:
                continue

            if not o.doable():
                continue

            pos = o.start_position()

            if best_o is None:
                best_o = o
                best_start = pos

            # TODO select best

        return best_o, best_start

    async def _run_single_job(self, job, p_start):
        """Move to the start position of the job and execute it"""

        # Move to its start pos
        try:
            tolerance = self._travel_tol

            await self._fz.move.move_to(
                p_start,
                mode="dijkstra",
                timeout=self.MOVE_TO_START_TIMEOUT,
                tolerance=tolerance,
                travel_tolerance=self._travel_tol)

        except (MoveTimeout, MoveNoPathFound):
            logger.exception("Move to start of {} ({}) failed, but it's ok"
                             .format(self._cur_job, p_start))
            return Job.MoveToFailedButItsOK

        except Exception:
            logger.exception("Move to start of {} ({}) failed"
                             .format(self._cur_job, p_start))

            return False

        logger.info("Executing {}".format(job))

        # Run!
        success = False
        try:
            success = await job.execute()
            logger.info("Job {} returned {}".format(job, success))
        except Exception:
            logger.exception("Obj {} Failed while executing".format(job))

        if not success:
            logger.info("Obj {} to be rescheduled".format(job))

        return success

    async def wait_until_stop(self):
        """Internal processing of match stop"""
        while self.remaining:
            logger.debug("Remaining {:.1f} all {} done {} current {} pos {}"
                         .format(
                            self.remaining,
                            [str(i) for i in self._jobs],
                            [str(i) for i in self._done_jobs],
                            self._cur_job, self._fz.move.position))

            dt = min(self.remaining, 1)

            await asyncio.sleep(dt)

        logger.info("Match Stopped current {} handlers {}"
                    .format(self._cur_job, self._stop_handlers))

        asyncio.ensure_future(self._fz.move.stop(), loop=self._loop)

        if self._job_task:
            try:
                logger.warning(
                    "Cancelling job {} ({})"
                    .format(str(self._cur_job), str(self._job_task)))
                self._job_task.cancel()
                self._job_task = None
            except Exception:
                logger.exception("Error while cancelling")

        for h in self._stop_handlers:
            try:
                await h()
            except Exception:
                pass
