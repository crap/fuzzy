
class Job:
    Reschedule = "reschedule"
    MoveToFailedButItsOK = "move_failed_but_ok"

    def __init__(self, name):
        """Create the job, with at least a name"""
        self.name = name
        self._points_marked = 0

    def __str__(self):
        return "{}".format(self.name)

    def update_color(self, color):
        """Update color-dependent parts of the job"""
        pass

    def start_position(self):
        """Compute the start position to do this job"""
        raise NotImplementedError()

    def doable(self):
        """Get the doability of the job

        Return True if doable
        """
        raise NotImplementedError()

    def points_marked(self):
        """Get the doability of the job

        Return >0 if doable
        """
        return self._points_marked

    async def execute(self):
        """Execute the job process

        The coroutine should return True of False wheter the job has
        succeeded or not.
        """
        return False

    def reset(self):
        """Reset to start state, cancel any pending action"""
        pass
