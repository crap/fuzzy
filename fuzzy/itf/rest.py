import aiohttp.web
import json
import functools

import logging


logger = logging.getLogger("REST")


class REST:
    url_prefix = "/api"

    def __init__(self, loop, tcp_port):
        self._loop = loop
        self._tcp_port = tcp_port
        self._app = aiohttp.web.Application(loop=self._loop)
        self._handler = None
        self._srv = None

        self._static_dir = None

        logging.getLogger('aiohttp').setLevel(logging.WARNING)

    def enable_static(self, static_dir):
        """Serve the given folder for URL /

        Note: call this after registering all required methods
        """
        self._static_dir = static_dir

        # Add static folder and a redirect from / to /index.html
        self._app.router.add_route("GET",
                                   "/",
                                   self._redirect_to_index)
        self._app.router.add_static("/", static_dir)

    async def _redirect_to_index(self, req):
        return aiohttp.web.Response(status=301,
                                    headers={"location": "/index.html"})

    async def start(self):
        self._handler = self._app.make_handler()
        self._srv = await \
            self._loop.create_server(self._handler, '0.0.0.0', self._tcp_port,
                                     reuse_address=True)

    async def stop(self):
        await self._app.finish()

    def _response_wrapper(self, handler):
        async def wrapped_handler(req):
            try:
                json_resp = await handler(req)
            except Exception as e:
                logger.exception("REST error")
                json_resp = {
                    "status": "error",
                    "error": repr(e)
                }
            return aiohttp.web.Response(
                    body=json.dumps(
                        json_resp,
                        indent=4,
                        separators=(',', ': ')).encode()+b"\n",
                    content_type="application/json")

        return wrapped_handler

    def register_get(self, url, handler):
        """Register a GET handler, prefixed with /api

        Arguments:
            url: the URL to register (str)
            handler: coroutine called for each request, should return
                a Response
        """
        self._app.router.add_route('GET', self.url_prefix + url,
                                   self._response_wrapper(handler))

    def register_put(self, url, handler):
        """Register a PUT handler, prefixed with /api

        Arguments:
            url: the URL to register (str)
            handler: coroutine called for each request, should return
                a Response"""
        self._app.router.add_route('PUT', self.url_prefix + url,
                                   self._response_wrapper(handler))

    def register_post(self, url, handler):
        """Register a POST handler, prefixed with /api

        Arguments:
            url: the URL to register (str)
            handler: coroutine called for each request, should return
                a Response"""
        self._app.router.add_route('POST', self.url_prefix + url,
                                   self._response_wrapper(handler))

    async def _websocket_handler(self, handler, request):
        ws = aiohttp.web.WebSocketResponse()
        await ws.prepare(request)
        await handler(ws)
        return ws

    def register_websocket(self, url, handler):
        """Register a Websocket handler, prefixed with /api

        Arguments:
            url: the URL to register (str)
            handler: coroutine called for each request, called with the
                     websocket handler"""
        self._app.router.add_route('GET', self.url_prefix + url,
                                   functools.partial(self._websocket_handler,
                                                     handler))

    def resp_404(self, msg=b"404 Not Found\n"):
        return aiohttp.web.Response(body=msg, status=404)
