import logging
logger = logging.getLogger("CanRest")


class CANREST:
    def __init__(self, can, rest):
        self._can = can
        self._rest = rest

        self._rest.register_get("/can/{dst}/{cat}/{res}", self._get)
        self._rest.register_put("/can/{dst}/{cat}/{res}", self._put)

    async def _get(self, req):
        return await self._req(req, target="get")

    async def _put(self, req):
        return await self._req(req, target="set")

    async def _req(self, req, target):
        dst = int(req.match_info['dst'])
        cat = int(req.match_info['cat'])
        res = int(req.match_info['res'])

        resp = None
        try:
            if target == "get":
                logger.debug("Requesting GET to {} {} {}"
                             .format(dst, cat, res))
                resp = await self._can.request_get(dst, cat, res)
            else:
                data = bytearray(await req.json())
                logger.debug(
                    "Requesting SET to {} {} {} with dat {}"
                    .format(dst, cat, res, data))
                resp = await self._can.request_set(dst, cat, res, data)
        except Exception:
            logger.exception("Error processing request")

        logger.debug("Response is {}".format(resp))
        if resp is None:
            return {"status": "error", "error": "dev did not respond"}

        return {
            "status": "ok",
            "data": ":".join(["{:02x}".format(b) for b in resp.data]),
        }
