import asyncio
import socket
import struct
import fcntl
import copy
import logging

from .can_obj import FrameType, Message

logger = logging.getLogger("CAN")
SIOCGSTAMP = 0x8906


class TimeoutException(Exception):
    pass


class CAN:
    def __init__(self, device="can0", device_id=1, loop=None):
        """Create a CAN interface

        :param device: the device name, e.g. can0
        :param device_id: the device ID to use, usually 1
        """
        self._socket = None
        if device != "fake0":
            self._socket = socket.socket(
                socket.AF_CAN,
                socket.SOCK_RAW,
                socket.CAN_RAW)
            self._socket.bind((device,))

            # Register for error frames
            self._socket.setsockopt(
                socket.SOL_CAN_RAW,
                socket.CAN_RAW_ERR_FILTER,
                socket.CAN_ERR_MASK)

        self._device_id = device_id
        self._loop = loop
        if self._loop is None:
            self._loop = asyncio.get_event_loop()

        if self._socket:
            self._loop.add_reader(self._socket, self._read_frame)
        self._message_handler = None

        # Storage for pending requests
        self._pending_req = {}
        # Storage for frames being reconstructed
        self._incomplete_frames = {}

        self._lock = asyncio.Lock()

    @property
    def dev_id(self):
        """Simple access to the device ID"""
        return self._device_id

    async def request_get(self, dst_id, cat, res, dat=b"", timeout=0.2):
        """Send a GET request to the given destination"""
        resp = await self._request(dst_id, cat, res, dat, FrameType.Get,
                                   timeout)
        if resp is None:
            raise asyncio.TimeoutError("No response from {}".format(dst_id))
        return resp

    async def request_set(self, dst_id, cat, res, dat=b"", timeout=0.2):
        """Send a SET request to the given destination"""
        resp = await self._request(dst_id, cat, res, dat, FrameType.Set,
                                   timeout)
        if resp is None:
            raise asyncio.TimeoutError("No response from {}".format(dst_id))
        return resp

    async def request_notif(self, dst_id, cat, res, dat=b""):
        """Send a NOTIF request to the given destination"""
        await self._request(dst_id, cat, res, dat, FrameType.Notif,
                            timeout=None)

    async def _request(self, dst_id, cat, res, dat, frame_type, timeout):

        # Forge base message
        base_msg = Message(
            self._device_id,
            dst_id,
            frame_type,
            cat,
            res)

        async with self._lock:
            # Store pending request if timeout is specified
            if timeout is not None:
                fut = self._pending_req[base_msg.key] = asyncio.Future()

            await self._split_and_send(base_msg, dat)

            if timeout is not None:
                # Wait for it
                try:
                    await asyncio.wait_for(fut, timeout=timeout)
                    return fut.result()

                except asyncio.TimeoutError:
                    logger.warning("CAN Timeout for %s", base_msg)
                    return None
                finally:
                    del self._pending_req[base_msg.key]

        return None

    def set_message_handler(self, handler):
        self._message_handler = handler

    async def _split_and_send(self, base_msg, dat):

        num_msg = 1 + max(0, len(dat) - 1) // 8

        for i in range(num_msg):
            # Prepare next message chunk
            msg = copy.deepcopy(base_msg)
            if i > 0:
                msg.frame_type = FrameType.Continuous
            msg.remaining = num_msg - i - 1
            msg.data = dat[8*i:8*(i+1)]

            await self._send_frame(msg)
            await asyncio.sleep(0.001)

    async def _send_frame(self, msg):
        """Send a CAN message, nothing more"""
        assert isinstance(msg, Message)

        # Ensure source ID is real
        msg.src_id = self._device_id

        if self._socket:
            await self._loop.sock_sendall(self._socket, msg.packed)

    def _read_frame(self):
        """Called each time a frame is available for reading.
        """
        # Fetch received frame
        raw = self._socket.recv(16)

        # Fetch timestamp
        res = fcntl.ioctl(self._socket, SIOCGSTAMP, struct.pack("@LL", 0, 0))
        t_s, t_us = struct.unpack("@LL", res)
        timestamp = t_s + t_us * 1e-6

        msg = Message.unpack(raw, timestamp)
        if not msg:
            return

        if msg.frame_type == FrameType.Continuous:
            # Search in frames
            if msg.key not in self._incomplete_frames:
                logger.warning("Continuous but no start %s", msg)
                return
            frame = self._incomplete_frames[msg.key]
            if frame.remaining != msg.remaining + 1:
                logger.warning("Continuous but no remaining mismatch %s", msg)
                del self._incomplete_frames[msg.key]
                return

            frame.data += msg.data
            frame.remaining -= 1
            if frame.remaining > 0:
                return

            # Complete frame, parse as if a simple one
            msg = frame
            del self._incomplete_frames[msg.key]

        elif msg.remaining:
            self._incomplete_frames[msg.key] = msg
            return

        # If response, set result in future
        if msg.frame_type in (FrameType.GetResp, FrameType.SetResp):
            if msg.key in self._pending_req:
                self._pending_req[msg.key].set_result(msg)
            else:
                logger.warning("Resp but no match %s", msg)
        else:
            # Process frame handling in asyncio context
            asyncio.ensure_future(
                self._process_req(
                    msg
                ),
                loop=self._loop
            )

    async def _process_req(self, msg):
        """Process an input request"""

        resp = None
        if self._message_handler:
            try:
                resp = await self._message_handler(msg)
            except Exception:
                logger.exception("Message Handler failed")

        if resp is not None:
            # TODO construct from request
            base_msg = Message()

            await self._split_and_send(base_msg, resp)
