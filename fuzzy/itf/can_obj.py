import socket
import struct
import enum

import logging
LOGGER = logging.getLogger("CAN")


class Category(enum.IntEnum):
    General = 1
    Servos = 2
    Motor = 3
    Indus = 4
    Move = 5
    Futabas = 6
    Pow = 7
    Pumps = 8
    Radio = 9
    Vlx = 10


class FrameType(enum.IntEnum):
    Get = 0
    GetResp = 1
    Set = 2
    SetResp = 3
    Continuous = 4
    Notif = 5


class Flags(enum.IntEnum):
    Extended = socket.CAN_EFF_FLAG % 2**32
    Error = socket.CAN_ERR_FLAG % 2**32
    Remote = socket.CAN_RTR_FLAG % 2**32


class Message:
    """
    CAN Message.
    the extended identifier is 29bit
    we use:
        5bit source ID
        5bit target ID
        4bit category (general, move, motor, servo, ...)
        6bit resource (reset, pwm, inputs, ...)
        3bit request type (get, getresp, set, setresp, ...)
        6bit remaining frames
    """
    # pylint: disable=too-many-instance-attributes

    CAN_FORMAT = "=IB3x8s"

    def __init__(self, src_id=0, dst_id=0, frame_type=0, cat=0, resource=0,
                 remaining=0, data=b'', timestamp=None, flags=None):
        # pylint: disable=too-many-arguments

        self.src_id = src_id
        self.dst_id = dst_id
        self.frame_type = FrameType(frame_type)
        self.cat = Category(cat)
        self.resource = resource
        self.remaining = remaining
        self.data = data

        self.timestamp = timestamp
        self.flags = flags

    def __repr__(self):
        return ("Message(src_id={src_id}, dst_id={dst_id}, frame_type={frame_type}"
                ", cat={cat}, resource={resource}, remaining={remaining}, "
                "data={data}, timestamp={timestamp}, flags={flags})") \
                .format(**self.__dict__)

    @property
    def key(self):
        """Return a key for matching response to requests"""
        if self.frame_type in (FrameType.Get, FrameType.Set):
            return (self.dst_id, self.cat, self.resource)
        else:
            return (self.src_id, self.cat, self.resource)

    @property
    def packed(self):
        data = self.data.ljust(8, b"\x00")

        msg_id = Flags.Extended + \
            (self.src_id << 24) + \
            (self.dst_id << 19) + \
            (self.cat << 15) + \
            (self.resource << 9) + \
            (self.frame_type << 6) + \
            (self.remaining << 0)

        return struct.pack(self.CAN_FORMAT, msg_id, len(self.data), data)

    @classmethod
    def unpack(cls, frame, timestamp):
        msg_id, dlen, data = struct.unpack(cls.CAN_FORMAT, frame)
        data = data[:dlen]

        flags = set([x for x in Flags if x & msg_id])
        if Flags.Extended not in flags:
            LOGGER.warning("Not extended frame received")
            return None

        src_id = (msg_id >> 24) & 0x1F
        dst_id = (msg_id >> 19) & 0x1F
        cat = (msg_id >> 15) & 0xF
        resource = (msg_id >> 9) & 0x3F
        frame_type = (msg_id >> 6) & 0x07
        remaining = (msg_id >> 0) & 0x3F
        return cls(src_id, dst_id, frame_type, cat, resource, remaining, data,
                   timestamp, flags)
