import math
import struct


def mod2pi(a, centered=True):
    """Compute the module 2 pi of the given angle, in radians

    if centered, return a value between -PI and PI, otherwise between 0 and 2PI
    """
    a %= 2*math.pi
    if centered and a > math.pi:
        a -= 2*math.pi
    return a


class Vector:
    """Class representing a Vector

    Instantiate with
        Vector() -> vector [0, 0, 0]
        Vector(l) -> vector [l[0], l[1], l[2]]
        Vector(x, y) -> vector [x, y, 0]
        Vector(x, y, th) -> vector [x, y, th]
    """
    def __init__(self, *args):

        if len(args) == 0:
            self._val = [0, 0, 0]
        elif len(args) == 1:
            self._val = list(args[0])[:3]
        else:
            self._val = list(args)[:3]
            self._val.extend([0]*(3 - len(self._val)))

    @property
    def x(self):
        return self._val[0]

    @x.setter
    def x(self, val):
        self._val[0] = val

    @property
    def y(self):
        return self._val[1]

    @y.setter
    def y(self, val):
        self._val[1] = val

    @property
    def theta(self):
        return self._val[2]

    @theta.setter
    def theta(self, val):
        self._val[2] = val

    @property
    def xy_norm(self):
        """Compute the XY norm of the Vector"""
        return math.hypot(self.x, self.y)

    @property
    def theta_norm(self):
        """Compute the theta norm of the Vector"""
        return abs(self.theta)

    @property
    def xy_angle(self):
        """Compute the angle of the XY subvector"""
        return math.atan2(self.y, self.x)

    @property
    def only_xy(self):
        """Create a Vector with X and Y kept, theta=0"""
        return Vector(self.x, self.y, 0)

    @property
    def only_theta(self):
        """Compute a Vector with only theta kept, X=Y=0"""
        return Vector(0, 0, self.theta)

    def round(self):
        """Round all members of the Vector to closest int
        Keep None values in place
        """
        for i in range(3):
            if self._val[i] is not None:
                self._val[i] = round(self._val[i])

    def replace(self, value, new_value):
        """Replace matching values in Vector"""
        for i in range(3):
            if self._val[i] == value:
                self._val[i] = new_value

    def mod2pi(self, coeff=1):
        """Apply modulo 2 PI over theta value

        Arguments:
            coeff: optional coefficient, set to 1e-3 if value stored in mrad
        """
        self.theta *= coeff
        self.theta = mod2pi(self.theta)
        self.theta /= coeff

    @property
    def unit(self):
        """Compute the unit vector of the Vector

        Unit'ed on XY and Theta separately
        """
        v = Vector(self)
        n = v.xy_norm
        if n != 0:
            v.x /= n
            v.y /= n
        if v.theta != 0:
            v.theta /= abs(v.theta)
        return v

    def add(self, v):
        """Add a vector to this one

        Arguments:
            v: Vector or list of length 3
        Returns:
            None
        """
        for i in range(3):
            if self._val[i] is not None and v._val[i] is not None:
                self._val[i] += v[i]
            else:
                self._val[i] = None

    def rotate(self, angle):
        """Rotate the Vector (X and Y coordinates)"""
        s, c = math.sin(angle), math.cos(angle)
        self.x, self.y = c*self.x - s*self.y, s*self.x + c*self.y

    def rotated(self, angle):
        """Return a copy of the vector, rotated by angle"""
        v = Vector(self)
        v.rotate(angle)
        return v

    def to_json(self, prefix="", no_y=False):
        """Create a json ready object"""
        dat = {
            prefix + "x": self.x,
            prefix + "theta": self.theta
            }
        if not no_y:
            dat[prefix + "y"] = self.y
        return dat

    def cross(self, b):
        """Return the 2D cross product of this vector with an other"""
        return self.x * b.y - self.y * b.x

    def dot(self, b):
        """Return the 2D dot product of this vector with an other"""
        return self.x * b.x + self.y * b.y

    @classmethod
    def from_json(cls, dat, prefix="", no_y=False):
        """Create a Vector from json object"""
        x = dat[prefix+"x"]
        y = 0
        if not no_y:
            y = dat[prefix+"y"]
        theta = dat[prefix+"theta"]
        return cls(x, y, theta)

    def reversed(self, mode="keep"):
        """Return reversed copy of the Vector"""
        v = Vector(self.x, 3-self.y, self.theta)

        if "opp" in mode:  # opposite mode
            v.theta = -v.theta

        return v

    def __iter__(self):
        return self._val.__iter__()

    def __getitem__(self, ix):
        return self._val.__getitem__(ix)

    def __setitem__(self, ix, value):
        return self._val.__setitem__(ix, value)

    def __eq__(self, other):
        if isinstance(other, list) and len(other) == 3:
            return self._val == other
        if isinstance(other, Vector):
            return self._val == other._val

        return NotImplemented

    def __add__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        v = Vector(self)
        v.add(other)
        return v

    def __sub__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        v = Vector(self)
        v.add(other*-1)
        return v

    def __mul__(self, value):
        v = Vector(self)
        for i in range(3):
            if v._val[i] is not None:
                v._val[i] *= value
        return v

    def __rmul__(self, value):
        return self * value

    def __truediv__(self, value):
        return self * (1/value)

    def __repr__(self):
        return "Vector({}, {}, {})".format(*self._val)

    def __str__(self):
        if self.theta is not None:
            return "<{:.3f}, {:.3f}, {:.3f}>".format(*self._val)
        return "<{:.3f}, {:.3f}>".format(*self._val[:2])

    def __hash__(self):
        return hash((self.x, self.y, self.theta))

    @property
    def packed_mm(self):
        p = self * 1000
        p.round()
        return struct.pack(">hhh", p.x, p.y, p.theta)

    @classmethod
    def unpack_mm(cls, dat):
        x, y, theta = struct.unpack(">hhh", dat)
        return Vector(x, y, theta) / 1000


class Segment(Vector):
    """Class representing a Segment, with two points"""

    def __init__(self, A, B):
        self.A = A
        self.B = B

        Vector.__init__(self, B - A)

    def __repr__(self):
        return "Segment({!r}, {!r})".format(self.A, self.B)

    def __str__(self):
        return "({} -> {})".format(self.A, self.B)

    def translate(self, dm):
        """Translate the segment by the given vector"""
        self.A += dm
        self.B += dm

    def intersect(self, other):
        """Compute the intersection point with another segment"""
        # http://ericleong.me/research/circle-line/
        a1 = self.y
        a2 = other.y
        b1 = -self.x
        b2 = -other.x
        d = a1*b2 - a2*b1
        if d == 0:
            return None

        c1 = a1*self.A.x + b1*self.A.y
        c2 = a2*other.A.x + b2*other.A.y

        i = Vector(c1*b2-b1*c2, a1*c2-c1*a2) / d

        alpha = (i-self.A).dot(self.unit)/self.xy_norm
        beta = (i-other.A).dot(other.unit)/other.xy_norm

        if (0 <= alpha <= 1) and (0 <= beta <= 1):
            return i

        # Out of bounds
        return None

    def closest_point(self, pt, respect_boundaries=False):
        """Find the closest point in the line to the given point"""

        dot = (pt-self.A).dot(self)

        on_line = 0
        if self.xy_norm:
            on_line = dot / self.xy_norm

        if respect_boundaries:
            if on_line < 0:
                return self.A
            if on_line > self.xy_norm:
                return self.B

        AC = on_line * self.unit
        return self.A + AC

    def circle_moving_collision(self, mov, radius):
        """Check if a circle with given radius travels over mov segment
        intersects this segment, and return collision point.
        """

        # a: intersection between line and movement
        a = self.intersect(mov)
        # b: closest on line of end of movement
#        b = self.closest_point(mov.B)   unused now
        # c, d: closest points on mov of the line
        c = mov.closest_point(self.A)
        d = mov.closest_point(self.B)

        if a is not None:
            # compute distance from start of mov to line
            dist = abs((mov.A - self.A).cross(self.unit))
            if dist == 0:
                # On start point
                return a
            # p2: point of collision with the line (may be outside segment)
            p2 = a - radius/dist * (mov.A - a).xy_norm * mov.unit

            # pC: projection of p2 over the line
            pC = self.closest_point(p2)

            # if pC is on the line, we are done
            dPc = (pC - self.A).dot(self.unit)
            if 0 <= dPc <= self.xy_norm:
                return p2

        ecol = None
        dcol = None

        # Weird case B

#        if (b - mov.B).xy_norm < radius:
#            # check if b is on the line
#            db = (b - self.A).dot(self.unit)
#            if 0 <= db <= self.xy_norm:
#                print("Type-b collision!!!")
#                # TODO

        dA = (self.A - c).xy_norm
        dB = (self.B - d).xy_norm

        # Collision with A
        if dA <= radius:
            # check if c is on mov
            dmov = (c - mov.A).dot(mov.unit)
            if 0 <= dmov <= mov.xy_norm:
                ecol = self.A
                dcol = dmov

        # Collision with B
        if dB <= radius:
            # check if d is on mov
            dmov = (d - mov.A).dot(mov.unit)
            if 0 <= dmov <= mov.xy_norm:
                # Check if collision is closer than A's
                if ecol is None or dmov < dcol:
                    ecol = self.B
                    dcol = dmov

        if ecol is None:
            # No collision
            return None

        # Compute endpoint collision
        x = mov.closest_point(ecol)
        dist = (x - ecol).xy_norm
        dx = math.sqrt(radius**2 - dist**2)

        p_col = x - dx * mov.unit
        return p_col
