import struct
import math

from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy import geo

from . import board

import logging
logger = logging.getLogger("MoveBoard")


class Access(IntEnum):
    PWM = 1
    ENCODERS = 2
    BUMP = 3

    ODO_CONFIG = 4
    ODO_GYRO_CALIB = 5
    ODO_POSITION_NOTIF = 6

    ODO_POSITION = 7
    ODO_SPEED = 8

    GOTO_SPEED_CONFIG = 9
    GOTO_GOAL_CONFIG = 10

    GOTO_STOP = 11
    GOTO_RAW = 12
    GOTO_SPEED = 13
    GOTO_TARGET = 14
    GOTO_TARGET_KEEPALIVE = 15
    GOTO_STATUS = 16

    GOTO_DEBUG = 17
    GOTO_BURN_OUT = 21


class Flags(IntEnum):
    LOG = 0x80
    TOL = 0x40
    KEEP = 0x20
    FINROT = 0x04
    BAK = 0x02
    ROT = 0x01


class MoveBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.move, _id)

        self._odo_notif_handler = None
        self._burn_notif_handler = None
        self._goto_target_reached_handler = None

        self._debug_points = []
        self.register_notif(
            Category.Move,
            Access.GOTO_DEBUG,
            self._debug)

        logger.info("Move Board #{} created!".format(_id))

    async def get_encoders(self):
        """Get the 3 encoder values, + gyro

        Return:
            e0, e1, e2, g: the three encoder values and gyro value
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Move,
            Access.ENCODERS,
            )
        e0, e1, e2, g = struct.unpack(">iiii", resp.data)
        return e0, e1, e2, g

    async def set_pwm(self, pwm0, pwm1, pwm2):
        """Set PWM values

        Arguments:
            pwmx the value to set, between -100 and 100
        """
        pwms = [pwm0, pwm1, pwm2]
        pwms = [round(x / 100 * 32767) & 0xFFFF for x in pwms]

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.PWM,
            dat=struct.pack(">HHH", *pwms)
            )

    async def set_odo_config(self, config):
        """Set the odometry configuration"""

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.ODO_CONFIG,
            dat=config.packed
            )

    async def set_odo_position(self, position):
        """Reset the odometry position"""

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.ODO_POSITION,
            dat=position.packed_mm
            )

    async def set_goto_speed_config(self, config):
        """Set the goto speed configuration"""

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_SPEED_CONFIG,
            dat=config.packed
            )

    async def set_goto_goal_config(self, config):
        """Set the goto goal configuration"""

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_GOAL_CONFIG,
            dat=config.packed
            )

    async def goto_stop(self):
        """Stop the GOTO now!"""
        logger.debug("MOVE STOP")

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_STOP,
            )

    async def goto_raw(self, pwm, dur=1, log=False):
        """Set RAW PWM, values between -100 and 100"""
        logger.debug("MOVE RAW X{}% TH{}%".format(pwm.x, pwm.theta))

        # Reset logs
        self._debug_points = []

        pwm *= 127/100
        pwm.round()

        flags = 0
        if log:
            flags |= Flags.LOG

        # Convert duration in 0.1s
        dur = round(dur * 10)

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_RAW,
            dat=struct.pack(">bbBB", pwm.x, pwm.theta, flags, dur)
            )

    async def goto_speed(self, speed, theta_en=True, dur=1, log=False):
        """Set Speed ref"""
        logger.debug("MOVE SPEED X {}m/s TH {}deg/s".format(
            speed.x, round(speed.theta/math.pi*180)))

        # Reset logs
        self._debug_points = []

        # Convert duration in 0.1s
        dur = round(dur * 10)

        flags = 0
        if log:
            flags |= Flags.LOG

        vx = round(speed.x * 1000)
        vth = round(speed.theta * 1000)

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_SPEED,
            dat=struct.pack(">hh?BB", vx, vth, theta_en, flags, dur)
            )

    async def goto_target(
            self, target, handler=None, rotate_first=False,
            backward_allowed=False, final_rotate=True,
            keep=False, tolerance=None, log=False):
        """Set a target to go to
        Arguments:
            target: the Vector coordinates
            handler: to be notified of target reached
            rotate_first: indicate if direction angle should be reached before
                translating
            backward_allowed: indicate if it is allowed to move backward if
                it is closer
            final_rotate: indicate if target angle must be matched
            keep: indicate if position should be kept after reached
            tolerance: if tolerance must be specified
            log: Flag to log it all

        """
        flags = 0
        if rotate_first:
            flags |= Flags.ROT
        if backward_allowed:
            flags |= Flags.BAK
        if final_rotate:
            flags |= Flags.FINROT
        if keep:
            flags |= Flags.KEEP
        if log:
            flags |= Flags.LOG
        if tolerance is not None:
            flags |= Flags.TOL

        self._debug_points = []

        # Prepare packed data
        dat = struct.pack(">B", flags) + target.packed_mm
        if tolerance is not None:
            dat += tolerance.packed

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_TARGET,
            dat=dat
            )
        self._goto_target_reached_handler = handler
        self.register_notif(
            Category.Move,
            Access.GOTO_TARGET,
            self._target_reached)

    async def _target_reached(self, msg):
        pos = geo.Vector.unpack_mm(msg.data[:6])
        tar = geo.Vector.unpack_mm(msg.data[6:])

        if self._goto_target_reached_handler:
            await self._goto_target_reached_handler(pos, tar)

    async def goto_keepalive(self):
        """Keepalive the current goto
        """
        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.GOTO_TARGET_KEEPALIVE
            )

    async def get_bumps(self):
        """Get the bumps values

        Return:
            6 bool for the 6 bumps
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Move,
            Access.BUMP,
            )
        bumps = resp.data[0]
        bumps = [bool(bumps & (1 << i)) for i in range(6)]
        return bumps

    async def get_position(self):
        """Get the current position

        Return:
            a Vector for the position
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Move,
            Access.ODO_POSITION,
            )
        return geo.Vector.unpack_mm(resp.data)

    async def set_position_notif(self, period, handler):
        """Set a position notif' handler"""
        period = round(period * 10)
        data = struct.pack(">B", period)

        await self.manager.can.request_set(
            self.id,
            Category.Move,
            Access.ODO_POSITION_NOTIF,
            dat=data
            )
        self._odo_notif_handler = handler
        self.register_notif(
            Category.Move,
            Access.ODO_POSITION,
            self._pos_notif)

    async def _pos_notif(self, msg):
        p = geo.Vector.unpack_mm(msg.data)
        if self._odo_notif_handler:
            await self._odo_notif_handler(p)

    def set_burn_notif(self, handler):
        """Set a burn notif' handler"""
        self._burn_notif_handler = handler
        self.register_notif(
            Category.Move,
            Access.GOTO_BURN_OUT,
            self._burn_notif)

    async def _burn_notif(self, msg):
        logger.debug("BURN")
        if self._burn_notif_handler:
            await self._burn_notif_handler()

    async def _debug(self, msg):
        ix, ref_x, ref_t, val_x, val_t, errsum_x, errsum_t, cmd_x, cmd_t = \
            struct.unpack(">Bbbbbbbbb", msg.data)

        if len(self._debug_points):
            while ix < self._debug_points[-1][0]:
                ix += 256

        ref = [ref_x * 1 / 127,
               ref_t * 2 * math.pi / 127]
        val = [val_x * 1 / 127,
               val_t * 2 * math.pi / 127]
        errsum = [errsum_x * 2 / 127,
                  errsum_t * 4 * math.pi / 127]
        cmd = [cmd_x * 1 / 127,
               cmd_t * 1 / 127]

        self._debug_points.append([ix] + ref + val + errsum + cmd)
