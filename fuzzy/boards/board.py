import asyncio
import struct
from enum import IntEnum
import logging
from fuzzy.itf.can_obj import Category

logger = logging.getLogger("Board")


class General(IntEnum):
    Reset = 1
    Discover = 2
    Power = 3


class Type(IntEnum):
    none = 0
    servos = 1  # 6 Servos
    motor = 2
    indus = 3
    move = 4
    futabas = 5
    pow = 6
    pumps = 7
    radio = 8
    servo = 9  # Single servo
    vlx = 10

    fakemove = 255


class Board:
    def __init__(self, manager, _type, _id):
        self.manager = manager
        self.type = _type
        self.id = _id

        self._handlers = {}
        self._booted_handler = None

    def __repr__(self):
        return "{}#{}".format(self.__class__.__name__, self.id)

    def set_booted_handler(self, handler):
        """Set a handler to be called when the board boots"""
        self._booted_handler = handler

    def register_notif(self, cat, res, handler):
        self._handlers[("NOTIF", cat, res)] = handler

    async def reset(self):
        """Reset the board"""

        try:
            await self.manager.can.request_set(
                self.id,
                Category.General,
                General.Reset
            )
        except asyncio.TimeoutError:
            # Normal error
            pass

    async def get_power(self):
        """Get current power setting

        Returns:
            power requested, as a bool
            power active, as a bool
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.General,
            General.Power
        )

        return struct.unpack("??", resp.data)

    async def power(self, on=True):
        """Control the board power setting
        """
        assert isinstance(on, bool)

        await self.manager.can.request_set(
            self.id,
            Category.General,
            General.Power,
            dat=struct.pack("?", on)
        )

    async def booted(self):
        """Notification of boot"""
        if self._booted_handler:
            await self._booted_handler(self.id)

    async def handle_get(self, msg):
        """Base method that processes a GET request"""
        handler = self._handlers.get(('GET', msg.cat, msg.resource), None)
        if handler:
            await handler(msg)

    async def handle_set(self, msg):
        """Base method that processes a SET request"""
        handler = self._handlers.get(('SET', msg.cat, msg.resource), None)
        if handler:
            await handler(msg)

    async def handle_notif(self, msg):
        """Base method that processes a NOTIF request"""
        handler = self._handlers.get(('NOTIF', msg.cat, msg.resource), None)
        if handler:
            await handler(msg)
        else:
            logger.warning("No handler for {}".format(msg))
