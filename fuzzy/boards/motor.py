import asyncio
import struct
from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy.devices import Motor as MotorDevice, Servo, ServoPosition

from . import board

import logging


class Motor(IntEnum):
    """resources"""
    PWM = 1
    POT = 2
    ENC = 3
    CUR = 4
    EOC = 5

    PID_CONFIG = 6
    PID_SET_REFERENCE = 7
    PID_STOP = 8

    BUMP_EOC = 9


class MotorPIDConfig:
    def __init__(self, fixed_pwm, kp, ki, kd):
        self.fixed_pwm = fixed_pwm
        self.kp = kp
        self.ki = ki
        self.kd = kd


class MotorBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.motor, _id)
        self._logger = logging.getLogger("Motor#{}".format(_id))

        self._logger.info("Created!".format(_id))

        # Register calibration done
        self.register_notif(Category.Motor, Motor.BUMP_EOC, self._bumped)

        self._motor = MotorDevice(self)
        self._motorservo = None

        self._bump_fut = None

        manager.fz.devices.add_motor(self._motor)

    def motor(self):
        """Simple access to the Motor device"""
        return self._motor

    def config_servo(self, full_scale, tolerance):
        if self._motorservo is not None:
            raise Exception("Motor servo already initialized")

        self._motorservo = MotorServo(self,
                                      full_scale=full_scale,
                                      tolerance=tolerance)
        self.manager.fz.devices.add_servo(self._motorservo)

    @property
    def servo_interface(self):
        """Simple access to the Servo device"""
        if self._motorservo is None:
            raise Exception("Motor servo not initialized")

        return self._motorservo

    async def set_pwm(self, pwm):
        """Set the PWM of a motor

        Arguments:
            pwm the value to set, between -100 and 100
        """
        assert isinstance(pwm, (int, float))
        pwm = self.convert_pwm(pwm)

        await self.manager.can.request_set(
            self.id,
            Category.Motor,
            Motor.PWM,
            dat=struct.pack(">h", pwm)
        )

    async def get_pwm(self):
        """Get the PWM of the motor

        Returns:
            pwm: the PWM value, between -100 and 100
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Motor,
            Motor.PWM
        )

        pwm, = struct.unpack(">h", resp.data)
        pwm = pwm / 32767 * 100
        return pwm

    async def get_current(self):
        """Get the current of the motor

        Returns:
            current: the Current value, between -100 and 100
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Motor,
            Motor.CUR
        )

        pwm, = struct.unpack(">h", resp.data)
        pwm = pwm / 32767 * 100
        return pwm

    async def get_encoder(self):
        """Get the encoder reading of the motor

        Returns:
            encoder: the encoder value, signed 32bit
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Motor,
            Motor.ENC
        )

        enc, = struct.unpack(">i", resp.data)
        return enc

    async def reset_encoder(self):
        """Reset the encoder to zero
        """

        await self.manager.can.request_set(
            self.id,
            Category.Motor,
            Motor.ENC
        )

    async def get_eocs(self):
        """Get the EOCs of the motor

        Returns:
            eoc_up: the first end of course
            eoc_down: the second end of course
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Motor,
            Motor.EOC
        )

        eoc1, eoc2, = struct.unpack(">??", resp.data)
        return eoc1, eoc2

    async def get_pid_config(self, direction):
        """Get the pid configuration for a direction

        Params:
            direction: "up" or "down"
        """
        assert direction in ("up", "down")

        direction = 0 if direction == "up" else 1

        dat = await self.manager.can.request_get(
            self.id,
            Category.Motor,
            Motor.PID_CONFIG,
            struct.pack(">B", direction)
            )

        fixed_pwm, kp, ki, kd = struct.unpack(">hfff", dat)
        return MotorPIDConfig(fixed_pwm, kp, ki, kd)

    async def set_pid_config(self, direction, config):
        """Set the pid configuration for a direction

        Params:
            direction: "up" or "down"
        """
        assert direction in ("up", "down")
        assert isinstance(config, MotorPIDConfig)

        direction = 0 if direction == "up" else 1
        dat = struct.pack(
            ">Bhfff",
            direction, config.fixed_pwm, config.kp, config.ki, config.kd)

        await self.manager.can.request_set(
            self.id,
            Category.Motor,
            Motor.PID_CONFIG,
            dat
        )

    async def set_pid_reference(self, reference, tolerance, speed):
        """Set the pid reference to reach

        Params:
            reference: a signed 32bit encoder value to reach
            tolerance: a 32bit encoder distance to be notified of reach
            speed: a 32bit encoder step to add/remove at each iteration
        """
        dat = struct.pack(">iii", reference, tolerance, speed)

        await self.manager.can.request_set(
            self.id,
            Category.Motor,
            Motor.PID_SET_REFERENCE,
            dat
        )

    async def stop_pid(self):
        """Stop PID
        """

        await self.manager.can.request_set(
            self.id,
            Category.Motor,
            Motor.PID_STOP
        )

    async def bump_to_eoc(self, pwm, reset_on_bump=False, wait=False):
        """Bump to end of course

        pwm between -100 and 100
        """

        if self._bump_fut and not self._bump_fut.done():
            self._bump_fut.cancel()

        self._bump_fut = asyncio.Future()
        self._reset_on_bump = reset_on_bump

        pwm = self.convert_pwm(pwm)

        self._logger.debug("Calibrating motor to EOC with pwm {}".format(pwm))
        dat = struct.pack(">h", pwm)
        await self.manager.can.request_set(
            self.id,
            Category.Motor,
            Motor.BUMP_EOC,
            dat
        )

        if wait:
            await self._bump_fut

    async def _bumped(self, msg):
        self._logger.info("Bumped")

        if self._bump_fut and not self._bump_fut.done():
            if self._reset_on_bump:
                self._reset_on_bump = False
                await self.reset_encoder()

            self._bump_fut.set_result(None)

    def convert_pwm(self, pwm):
        if pwm > 100:
            pwm = 100
        if pwm < -100:
            pwm = -100

        return round(pwm / 100 * 32767)


class MotorServo(Servo):
    def __init__(self, board, full_scale=1000000, tolerance=1000):
        Servo.__init__(self, "MotorServo{}".format(board.id))

        self._board = board
        self._full_scale = full_scale
        self._tolerance = tolerance
        self._prev_ref = None

    async def _set(self, position):
        ref = self._full_scale / 2 * (1 + position.position / 90)
        # compute travel speed depending on requested duration
        d = position.duration
        speed = self._full_scale / 2 / 100
        if self._prev_ref is not None:
            speed = abs(ref - self._prev_ref) / d / 100

        await self._board.set_pid_reference(ref, self._tolerance, speed)
        self._prev_ref = ref

    async def _release(self):
        await self._board.stop_pid()

    async def _get(self):
        enc = await self._board.get_encoder()
        pos = (enc / self._full_scale * 2 - 1) * 90
        return ServoPosition(pos, torque=100)
