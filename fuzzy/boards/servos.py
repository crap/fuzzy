import struct
from contextlib import contextmanager
from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy.devices import Servo, ServoPosition

from . import board

import logging
logger = logging.getLogger("Servos")


class ServosServo(Servo):
    def __init__(self, board, servo_id):
        Servo.__init__(self, "ServosServo{}-{}".format(board.id, servo_id))

        self._board = board
        self._servo_id = servo_id

    async def _set(self, position):
        await self._board.set_position(
            self._servo_id, position.position, position.duration)

    async def _release(self):
        await self._board.release(self._servo_id)

    async def _force(self, val=1):
        await self._board.force(self._servo_id, val)

    async def _get(self):
        active, position = await self._board.get_position(self._servo_id)

        return ServoPosition(position, torque=100 if active else 0)


class Servos(IntEnum):
    Servo0 = 0
    Servo1 = 1
    Servo2 = 2
    Servo3 = 3
    Servo4 = 4
    Servo5 = 5
    Bumps = 6


class ServosBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.servos, _id)

        logger.info("Servos Board #{} created!".format(_id))

        self._bump_handlers = set()
        self.register_notif(
            Category.Servos,
            Servos.Bumps,
            self._bump_notif)

        self._servos = [ServosServo(self, i) for i in range(6)]
        for s in self._servos:
            manager.fz.devices.add_servo(s)

    def servo(self, ix):
        """Simple access to a servo instance"""
        return self._servos[ix]

    async def set_position(self, servo_id, position=50, duration=0):
        """Set position of a servo, between -90° and 90°

        Arguments:
            servo_id: the ID of the servo (0-5)
            position: the position, between -90 and 90
            duration: the time to take to perform the move, in seconds
        """
        assert servo_id in range(6)
        assert isinstance(position, (int, float))
        assert isinstance(duration, (int, float))

        position = max(-90, min(position, 90))
        # assume mapping -90 -> 0, +90 -> 1000
        position = int((position + 90) / 180 * 1000)

        duration = max(0, min(duration, 10))
        duration = int(duration * 100)

        await self.manager.can.request_set(
            self.id,
            Category.Servos,
            Servos.Servo0 + servo_id,
            dat=struct.pack(">HH", position, duration)
        )

    async def release(self, servo_id):
        """Release a servo

        Arguments:
            servo_id: the ID of the servo (0-5)
        """
        assert servo_id in range(6)

        await self.manager.can.request_set(
            self.id,
            Category.Servos,
            Servos.Servo0 + servo_id
        )

    async def force(self, servo_id, value):
        """Force a servo

        Arguments:
            servo_id: the ID of the servo (0-5)
            value: the hash period, between 0 and 1
        """
        assert servo_id in range(6)
        assert 0 <= value <= 1

        await self.manager.can.request_set(
            self.id,
            Category.Servos,
            Servos.Servo0 + servo_id,
            dat=struct.pack(">B", int(value * 0xFF))
        )

    async def get_position(self, servo_id):
        """Get position of a servo, between -90 and 90

        Arguments:
            servo_id: the ID of the servo (0-5)
        Returns:
            active: bool indicating if servo is active
            position: the position, in °
        """
        assert servo_id in range(6)

        resp = await self.manager.can.request_get(
            self.id,
            Category.Servos,
            Servos.Servo0 + servo_id
        )

        active, position = struct.unpack(">?H", resp.data)
        logger.debug("Got P {}".format(position))
        if position != 0:
            position = position / 1000 * 180 - 90
        return active, position

    async def get_bumps(self):
        """Get value of the bumps

        Arguments:
        Returns:
            bump0: bool indicating if bump0 is clicked
            bump1: bool indicating if bump1 is clicked
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Servos,
            Servos.Bumps
        )

        bump0, bump1 = struct.unpack(">??", resp.data)
        return bump0, bump1

    def add_bump_handler(self, handler):
        self._bump_handlers.add(handler)

    def remove_bump_handler(self, handler):
        self._bump_handlers.remove(handler)

    @contextmanager
    def bump_handler(self, h):
        self.add_bump_handler(h)
        yield
        self.remove_bump_handler(h)

    async def _bump_notif(self, msg):
        bump, = struct.unpack(">B", msg.data)
        logger.debug("Servo {} Bump {}".format(self.id, bump))

        for h in self._bump_handlers:
            await h(bump)
