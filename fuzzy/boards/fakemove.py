import asyncio

from . import board

import logging
logger = logging.getLogger("FakeMoveBoard")


class FakeMoveBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.move, _id)

        self._odo_notif_handler = None
        self._burn_notif_handler = None
        self._goto_target_reached_handler = None

        logger.info("FAKE Move Board #{} created!".format(_id))

    async def set_odo_config(self, config):
        """Set the odometry configuration"""
        pass

    async def set_odo_position(self, position):
        """Reset the odometry position"""
        asyncio.ensure_future(self.fake_arrived(position))

    async def set_goto_calib(self, calib):
        """Set the goto calibration"""
        pass

    async def set_goto_config(self, config):
        """Set the goto configuration"""
        pass

    async def goto_stop(self):
        """Stop the GOTO now!"""
        pass

    async def goto_raw(self, pwm, dur=1):
        """Set RAW PWM, values between -100 and 100"""
        pass

    async def goto_speed(self, speed, dur=1, log=False):
        """Set Speed ref"""
        pass

    async def goto_target(
            self, mode, target, theta=False, cross=False, keep=False,
            handler=None, log=False, tolerance=None):

        asyncio.ensure_future(self.fake_arrived(target, 0.5, handler=handler))

    async def fake_arrived(self, pos, wait=0, odo=True, handler=None):
        await asyncio.sleep(wait)

        if self._odo_notif_handler:
            await self._odo_notif_handler(pos)

        if handler:
            await handler(pos)

    async def goto_keepalive(self):
        """Keepalive the current goto
        """
        pass

    async def set_position_notif(self, period, handler):
        """Set a position notif' handler"""
        self._odo_notif_handler = handler

    def set_burn_notif(self, handler):
        """Set a burn notif' handler"""
        self._burn_notif_handler = handler
