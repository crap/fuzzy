import struct
from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy.devices import Pump

from . import board

import logging
logger = logging.getLogger("Pumps")


class Pumps(IntEnum):
    Pump0 = 0
    Pump1 = 1
    Pump2 = 2
    Pump3 = 3
    Pump4 = 4
    Release = 5
    Sensor0 = 6
    Sensor1 = 7
    Sensor2 = 8
    Sensor3 = 9
    Sensor4 = 10


class PumpsBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.pumps, _id)

        logger.info("Pumps Board #{} created!".format(_id))

        self._pumps = [Pump(self, i) for i in range(6)]
        for s in self._pumps:
            manager.fz.devices.add_pump(s)

    def pump(self, ix):
        """Simple access to a pump instance"""
        return self._pumps[ix]

    async def set_pump(self, pump_id, on):
        """Set a pump, either on or off

        Arguments:
            pump_id: the ID of the pump (0-4)
            on: boolean for enable
        """
        assert pump_id in range(5)

        await self.manager.can.request_set(
            self.id,
            Category.Pumps,
            Pumps.Pump0 + pump_id,
            dat=struct.pack(">?", on)
        )

    async def get_pump(self, pump_id):
        """Get a pump status, either on or off

        Arguments:
            pump_id: the ID of the pump (0-4)
        Returns:
            on: boolean for enable
        """
        assert pump_id in range(5)

        resp = await self.manager.can.request_get(
            self.id,
            Category.Pumps,
            Pumps.Pump0 + pump_id
        )
        on, = struct.unpack(">?", resp.data)
        return on

    async def release(self, duration=1, pwm=20):
        """Release all non-active pumps
        """

        # Convert duration in 1/10s
        duration = int(duration * 10)

        await self.manager.can.request_set(
            self.id,
            Category.Pumps,
            Pumps.Release,
            dat=struct.pack(">BB", duration, pwm)
        )

    async def sucking(self, pump_id):
        """Get status of a pump

        Arguments:
            pump_id: the ID of the servo (0-4)
        Returns:
            on: bool indicating if pump is active
        """
        assert pump_id in range(5)

        resp = await self.manager.can.request_get(
            self.id,
            Category.Pumps,
            Pumps.Pump0 + pump_id
        )

        on, = struct.unpack(">?", resp.data)
        return on

    async def holding(self, pump_id):
        """Get status of a pump sensor

        Arguments:
            pump_id: the ID of the servo (0-4)
        Returns:
            on: bool indicating if pump is holding an object
        """
        assert pump_id in range(5)

        resp = await self.manager.can.request_get(
            self.id,
            Category.Pumps,
            Pumps.Sensor0 + pump_id
        )

        on, = struct.unpack(">?", resp.data)
        return on
