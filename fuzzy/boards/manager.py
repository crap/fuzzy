import asyncio
import logging
from collections import defaultdict

from fuzzy.itf.can_obj import Category, FrameType
from . import board

from .move import MoveBoard
from .fakemove import FakeMoveBoard
from .servos import ServosBoard
from .futabas import FutabasBoard
from .pumps import PumpsBoard
from .motor import MotorBoard
from .vlx import VlxBoard
from .pow import PowBoard
from .indus import IndusBoard

from .rest.manager import ManagerRest

logger = logging.getLogger("BoardManager")


class Manager:

    creator = {
        board.Type.move: MoveBoard,
        board.Type.servos: ServosBoard,
        board.Type.futabas: FutabasBoard,
        board.Type.pumps: PumpsBoard,
        board.Type.motor: MotorBoard,
        board.Type.vlx: VlxBoard,
        board.Type.pow: PowBoard,
        board.Type.indus: IndusBoard,

        board.Type.fakemove: FakeMoveBoard
    }

    def __init__(self, fz, _can, rest):
        self.fz = fz
        self.can = _can

        # Storage for internal methods
        self._boards = {}
        self._auto_power_on_boards = set()
        self._pending_req = {}
        self._disc_boards = defaultdict(list)

        # Register General category
        self.can.set_message_handler(self._handle_can)

        self._rest = ManagerRest(self, rest)

        asyncio.ensure_future(self._discover_all())

        # Delegate a few methods to can
        self.request_get = self.can.request_get
        self.request_set = self.can.request_set
        self.request_notif = self.can.request_notif

    def enable_auto_power(self, on=True):
        """Set the auto power on on known bards"""
        if on:
            self._auto_power_on_boards = set(self._boards.keys())
        else:
            self._auto_power_on_boards = set()

    async def _discover_all(self):
        boards = await self.discover()
        logger.info("Discovered Boards: {}".format(boards))

        for btype, bds in boards.items():
            for bd in bds:
                self.add_board(bd, btype)

        logger.debug("Known Boards: {}".format(self.boards))

    async def discover(self):
        """Discover the boards on bus, and return a dict of ID -> Type"""

        self._disc_boards.clear()
        await self.can.request_notif(
            0,
            Category.General,
            board.General.Discover)

        # Wait for response
        await asyncio.sleep(0.5)

        return self._disc_boards

    def add_board(self, board_id, board_type, notif_booted=False):
        """Add a board to the managed boards list"""

        if board_type not in self.creator:
            logger.warning("Unknown type {} to add".format(board_type))
            return

        if board_id in self._boards:
            # Board already exists, notify booted if such and return it as is
            bd = self._boards[board_id]
            if notif_booted:
                asyncio.ensure_future(self._handle_board_booted(bd))
            return bd

        # Instantiate the Board, and store it
        bd = self.creator[board_type](self, board_id)
        self._boards[board_id] = bd

        # On initial creation, process booted
        asyncio.ensure_future(self._handle_board_booted(bd))

        return bd

    async def _handle_board_booted(self, bd):
        # auto power on if matching
        if bd.id in self._auto_power_on_boards:
            logger.info("Powering ON board {}".format(bd))
            try:
                await bd.power(True)
            except asyncio.CancelledError:
                raise
            except Exception:
                logger.exception("Failed to power on board {}"
                                 .format(bd))

        # Notify the board it has booted
        await bd.booted()

    @property
    def boards(self):
        """Get the list of known boards"""
        return list(self._boards.values())

    def _boards_of_type(self, _type):
        return [b for b in self._boards.values() if b.type == _type]

    @property
    def servos_boards(self):
        """Get the list of known servos boards"""
        return self._boards_of_type(board.Type.servos)

    async def _handle_can(self, msg):
        if msg.frame_type == FrameType.Get:
            logger.debug("GET Request!!!")

            if msg.src_id in self._boards:
                await self._boards[msg.src_id].handle_get(msg)
            else:
                logger.warning("GET request {} from unknown board {}"
                               .format(msg, msg.src_id))

            return

        elif msg.frame_type == FrameType.Set:
            logger.debug("SET Request!!!")

            # Custom handling of discover frames
            if (msg.cat, msg.resource) == \
                    (Category.General, board.General.Discover):
                btype = board.Type(msg.data[0])
                logger.info("Board {} discovered, type {}"
                            .format(msg.src_id, btype.name))
                self._disc_boards[btype].append(msg.src_id)

            if msg.src_id in self._boards:
                await self._boards[msg.src_id].handle_set(msg)
            else:
                logger.warning("SET request {} from unknown board {}"
                               .format(msg, msg.src_id))

            return

        elif msg.frame_type == FrameType.Notif:

            # Custom handling of booted messages
            if (msg.cat, msg.resource) == \
                    (Category.General, board.General.Reset):
                btype = board.Type(msg.data[0])
                logger.info("Board {} has booted, type {}"
                            .format(msg.src_id, btype.name))

                self.add_board(msg.src_id, btype, notif_booted=True)

            if msg.src_id in self._boards:
                await self._boards[msg.src_id].handle_notif(msg)
            else:
                logger.warning("NOTIF request {} from unknown board {}"
                               .format(msg, msg.src_id))

            return

        else:
            logger.warning("WEIRD frame!:!!")
            return
