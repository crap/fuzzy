import logging
logger = logging.getLogger("IndusRest")


class IndusRest:
    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/indus",
            self._get_indus)
        self._rest.register_get(
            "/boards/{id:\d+}/indus/color",
            self._get_indus_color)
        self._rest.register_put(
            "/boards/{id:\d+}/indus/dig/{dig:\d+}",
            self._put_indus_dig)

    async def _get_indus(self, req):
        device_id = int(req.match_info['id'])

        try:
            indus = self._manager._boards[device_id]
            color = await indus.get_raw_color()
            dat = {
                'power': await self._get_power(req),
                'indus': {
                    'color': {"r": color.r, "g": color.g, "b": color.b,
                              "i": color.i},
                    'digital': [None] * 4,
                    'analog': [None] * 2
                }
            }

            for dig in range(4):
                val, trig = await indus.get_dig(dig)
                dat['indus']['digital'][dig] = {
                    'value': val,
                    'trigger': trig
                    }

            for an in range(2):
                val, trig = await indus.get_an(an)
                dat['indus']['analog'][an] = {
                    'value': val,
                    'trigger': {
                        'dir': trig.dir,
                        'value': trig.value
                    }
                }

            return dat

        except Exception as e:
            logger.exception("Get indus failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_indus_color(self, req):
        device_id = int(req.match_info['id'])

        try:
            indus = self._manager._boards[device_id]

            color = await indus.get_raw_color()

            return {
                'color': {
                    "r": color.r,
                    "g": color.g,
                    "b": color.b,
                    "i": color.i
                    }
            }

        except Exception as e:
            logger.exception("Get color failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_indus_dig(self, req):
        device_id = int(req.match_info['id'])
        dig_id = int(req.match_info['dig'])
        dat = await req.json()

        try:
            indus = self._manager._boards[device_id]
            await indus.set_dig(dig_id, bool(dat))

        except Exception as e:
            logger.exception("Put dif failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}
