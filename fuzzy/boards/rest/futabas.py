import logging
logger = logging.getLogger("FutabasRest")


class FutabasRest:

    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/futabas",
            self._get_futabas)
        self._rest.register_put(
            "/boards/{id:\d+}/futabas/{servo:\d+}",
            self._put_futabas_position)
        self._rest.register_get(
            "/boards/{id:\d+}/futabas/{servo:\d+}",
            self._get_futabas_position)

    async def _get_futabas(self, req):
        device_id = int(req.match_info['id'])

        try:
            futabas = self._manager._boards[device_id]

            dat = {
                'power': await self._get_power(req),
                'futabas': {},
            }

            for s in range(1, 6):
                try:
                    position, torque = await futabas.get_position(s)
                except Exception:
                    logger.exception("GET servo {} failed".format(s))
                    continue
                dat['futabas'][s] = {
                    'position': position,
                    'torque': torque
                }

        except Exception as e:
            logger.exception("Get futabas failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

        return dat

    async def _put_futabas_position(self, req):
        device_id = int(req.match_info['id'])
        servo_id = int(req.match_info['servo'])

        try:
            dat = await req.json()
            position = dat['position']
            torque = dat.get('torque', 100)
            duration = dat.get('duration', 0)

            futabas = self._manager._boards[device_id]

            await futabas.set_position(
                servo_id,
                position,
                torque,
                duration)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set position failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_futabas_position(self, req):
        device_id = int(req.match_info['id'])
        servo_id = int(req.match_info['servo'])

        try:
            futabas = self._manager._boards[device_id]

            position, torque = await futabas.get_position(servo_id)
            return {
                'position': position,
                'torque': torque
            }

        except Exception as e:
            logger.exception("Get position failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}
