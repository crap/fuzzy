import logging

from .servos import ServosRest
from .futabas import FutabasRest
from .vlx import VlxRest
from .pumps import PumpsRest
from .motor import MotorRest
from .pow import PowRest
from .indus import IndusRest

logger = logging.getLogger("ManagerRest")


class ManagerRest(ServosRest, FutabasRest, VlxRest, PumpsRest, MotorRest,
                  PowRest, IndusRest):
    def __init__(self, manager, rest):
        self._manager = manager
        self._rest = rest

        # Boards commands
        self._rest.register_get(
            "/boards",
            self._get)

        # Generic commands per board
        self._rest.register_put(
            "/boards/{id:\d+}/power",
            self._put_power)
        self._rest.register_put(
            "/boards/{id:\d+}/reset",
            self._put_reset)
        self._rest.register_get(
            "/boards/{id:\d+}/power",
            self._get_power)

        # All boards init
        ServosRest.__init__(self)
        FutabasRest.__init__(self)
        VlxRest.__init__(self)
        PumpsRest.__init__(self)
        MotorRest.__init__(self)
        PowRest.__init__(self)
        IndusRest.__init__(self)

    async def _get(self, req):
        return {
            k: {
                "type": v.type.name,
                "id": k,
                "name": str(v)}
            for (k, v) in self._manager._boards.items()}

    async def _put_reset(self, req):
        device_id = int(req.match_info['id'])

        try:
            await self._manager._boards[device_id].reset()
            return {"status": "ok"}

        except Exception as e:
            logger.exception("Reset failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_power(self, req):
        device_id = int(req.match_info['id'])

        try:
            dat = await req.json()
            req_on = dat['on']

            await self._manager._boards[device_id].power(on=req_on)
            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set power failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_power(self, req):
        device_id = int(req.match_info['id'])

        try:
            power_requested, power_active = \
                await self._manager._boards[device_id].get_power()

            return {
                'req': power_requested,
                'on': power_active
            }

        except Exception as e:
            logger.exception("Get power failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}
