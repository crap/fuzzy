from ..pow import LEDColor

import logging
logger = logging.getLogger("PowRest")


class PowRest:

    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/pow",
            self._get_pow)
        self._rest.register_get(
            "/boards/{id:\d+}/pow/jack",
            self._get_pow_jack)
        self._rest.register_put(
            "/boards/{id:\d+}/pow/leds/off",
            self._put_pow_leds_off)
        self._rest.register_put(
            "/boards/{id:\d+}/pow/leds/chaser",
            self._put_pow_leds_chaser)

    async def _get_pow(self, req):
        device_id = int(req.match_info['id'])

        try:
            pow = self._manager._boards[device_id]

            dat = {
                'power': await self._get_power(req),
                'pow': {
                    'jack': await pow.get_jack(),
                    'color': str(await pow.get_color()),
                },
            }
            return dat

        except Exception as e:
            logger.exception("Get pow failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_pow_jack(self, req):
        device_id = int(req.match_info['id'])

        try:
            pow = self._manager._boards[device_id]

            jack = await pow.get_jack()
            return jack

        except Exception as e:
            logger.exception("Get jack failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_pow_leds_off(self, req):
        dev_id = int(req.match_info['id'])
        try:
            pow = self._manager._boards[dev_id]
            await pow.leds_off()
            return "OK"

        except Exception as e:
            logger.exception("Set LEDs off failure to {}".format(dev_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_pow_leds_chaser(self, req):
        dev_id = int(req.match_info['id'])
        try:
            pow = self._manager._boards[dev_id]

            dat = await req.json()
            color = LEDColor.from_json(dat['color'])
            await pow.leds_chaser(color)
            return "OK"

        except Exception as e:
            logger.exception("Set LEDs off failure to {}".format(dev_id))
            return {'error': 'Board error', 'message': repr(e)}
