import logging
from ..motor import MotorPIDConfig


logger = logging.getLogger("MotorRest")


class MotorRest:

    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/motor",
            self._get_motor)
        self._rest.register_put(
            "/boards/{id:\d+}/motor/pwm",
            self._put_motor_pwm)
        self._rest.register_put(
            "/boards/{id:\d+}/motor/bump",
            self._put_motor_bump)
        self._rest.register_put(
            "/boards/{id:\d+}/motor/pid/config",
            self._put_motor_pid_config)
        self._rest.register_put(
            "/boards/{id:\d+}/motor/pid/set",
            self._put_motor_pid_set)
        self._rest.register_put(
            "/boards/{id:\d+}/motor/pid/stop",
            self._put_motor_pid_stop)

    async def _get_motor(self, req):
        device_id = int(req.match_info['id'])

        try:
            motor = self._manager._boards[device_id]

            dat = {
                'power': await self._get_power(req),
                'motor': {
                    'pwm': await motor.get_pwm(),
                    'current': await motor.get_current(),
                    'encoder': await motor.get_encoder(),
                    'eocs': await motor.get_eocs(),
                    'pid': {
                        'up': await motor.get_pid_config('up'),
                        'down': await motor.get_pid_config('down')
                    }
                },
            }
            return dat

        except Exception as e:
            logger.exception("Get motor failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_motor_pwm(self, req):
        device_id = int(req.match_info['id'])

        try:
            dat = await req.json()
            pwm = dat

            motor = self._manager._boards[device_id]
            await motor.set_pwm(pwm)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set pwm failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_motor_bump(self, req):
        device_id = int(req.match_info['id'])

        try:
            dat = await req.json()
            pwm = dat["pwm"]
            reset_on_bump = dat.get("reset_on_bump", False)

            motor = self._manager._boards[device_id]

            await motor.bump_to_eoc(pwm, reset_on_bump)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set motor bump failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_motor_pid_config(self, req):
        device_id = int(req.match_info['id'])

        try:
            dat = await req.json()
            motor = self._manager._boards[device_id]

            direction = dat["direction"]
            config = MotorPIDConfig(
                dat["fixed"],
                dat["kp"],
                dat["ki"],
                dat["kd"])
            await motor.set_pid_config(
                direction,
                config
            )

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set pid config failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_motor_pid_set(self, req):
        device_id = int(req.match_info['id'])

        try:
            dat = await req.json()
            ref = dat["ref"]
            tol = dat["tol"]
            spd = dat["spd"]

            motor = self._manager._boards[device_id]
            await motor.set_pid_reference(ref, tol, spd)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set pid reference failure to {}"
                             .format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_motor_pid_stop(self, req):
        device_id = int(req.match_info['id'])

        try:
            motor = self._manager._boards[device_id]
            await motor.stop_pid()
            return {"status": "ok"}

        except Exception as e:
            logger.exception("Stop pid failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}
