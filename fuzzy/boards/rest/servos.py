import logging
logger = logging.getLogger("ServosRest")


class ServosRest:

    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/servos",
            self._get_servos)
        self._rest.register_put(
            "/boards/{id:\d+}/servos/{servo:\d+}",
            self._put_servos_position)
        self._rest.register_get(
            "/boards/{id:\d+}/servos/{servo:\d+}",
            self._get_servos_position)

    async def _get_servos(self, req):
        device_id = int(req.match_info['id'])

        try:
            servos = self._manager._boards[device_id]

            dat = {
                'power': await self._get_power(req),
                'servos': [],
                'bumps': await servos.get_bumps(),
            }

            for s in range(6):
                active, position = await servos.get_position(s)
                dat['servos'].append({
                    'active': active,
                    'position': position
                })
            return dat

        except Exception as e:
            logger.exception("Get servos failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_servos_position(self, req):
        device_id = int(req.match_info['id'])
        servo_id = int(req.match_info['servo'])

        try:
            dat = await req.json()
            active = dat.get('active', True)
            position = dat['position']
            duration = dat.get('duration', 0)

            servos = self._manager._boards[device_id]

            if active:
                await servos.set_position(
                    servo_id,
                    position,
                    duration=duration)
            else:
                await servos.release(servo_id)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set position failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_servos_position(self, req):
        device_id = int(req.match_info['id'])
        servo_id = int(req.match_info['servo'])

        try:
            servos = self._manager._boards[device_id]

            active, position = await servos.get_position(servo_id)
            return {
                'active': active,
                'position': position
            }

        except Exception as e:
            logger.exception("Get position failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}
