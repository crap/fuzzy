import logging
logger = logging.getLogger("PumpsRest")


class PumpsRest:

    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/pumps",
            self._get_pumps)
        self._rest.register_put(
            "/boards/{id:\d+}/pumps/{pump:\d+}",
            self._put_pumps_enable)
        self._rest.register_get(
            "/boards/{id:\d+}/pumps/{pump:\d+}",
            self._get_pumps_enable)
        self._rest.register_put(
            "/boards/{id:\d+}/pumps/release",
            self._put_pumps_release)

    async def _get_pumps(self, req):
        device_id = int(req.match_info['id'])

        try:
            pumps = self._manager._boards[device_id]

            dat = {
                'power': await self._get_power(req),
                'pumps': [],
            }

            for s in range(5):
                on = await pumps.get_pump(s)
                dat['pumps'].append({
                    'on': on,
                })
            return dat

        except Exception as e:
            logger.exception("Get pumps failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_pumps_enable(self, req):
        device_id = int(req.match_info['id'])
        pump_id = int(req.match_info['pump'])

        try:
            dat = await req.json()
            on = dat['on']

            pumps = self._manager._boards[device_id]

            await pumps.set_pump(
                pump_id,
                on)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Set pump failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_pumps_enable(self, req):
        device_id = int(req.match_info['id'])
        pump_id = int(req.match_info['pump'])

        try:
            pumps = self._manager._boards[device_id]

            on = await pumps.get_pump(pump_id)
            return {
                'on': on
            }

        except Exception as e:
            logger.exception("Get pump failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_pumps_release(self, req):
        device_id = int(req.match_info['id'])

        try:
            dat = await req.json()
            duration = dat.get('duration', 1)
            pwm = dat.get('pwm', 20)

            pumps = self._manager._boards[device_id]

            await pumps.release(duration, pwm)

            return {"status": "ok"}

        except Exception as e:
            logger.exception("Release pumps failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}
