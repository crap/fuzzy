import aiohttp
import logging
import functools

logger = logging.getLogger("VlxsRest")


class VlxRest:

    def __init__(self):
        self._rest.register_get(
            "/boards/{id:\d+}/vlx",
            self._get_vlx)
        self._rest.register_get(
            "/boards/{id:\d+}/vlx/{sensor:\d}/distance",
            self._get_vlx_distance)
        self._rest.register_put(
            "/boards/{id:\d+}/vlx/{sensor:\d}/led",
            self._set_vlx_led)
        self._rest.register_get(
            "/boards/{id:\d+}/vlx/distances",
            self._get_vlx_distances)

        self._rest.register_get(
            "/boards/{id:\d+}/vlx/distances/ws",
            self._get_vlx_distances_ws)
        self._vxl_dist_ws = set()

    async def _get_vlx(self, req):
        device_id = int(req.match_info['id'])

        try:
            vlx = self._manager._boards[device_id]

            return {
                'power': await self._get_power(req),
                'distances': await vlx.get_distances(),
            }

        except Exception as e:
            logger.exception("Get vlx failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_vlx_distance(self, req):
        device_id = int(req.match_info['id'])
        sensor_id = int(req.match_info['sensor'])

        try:
            vlx = self._manager._boards[device_id]

            dat = await vlx.get_distance(sensor_id)
            return dat

        except Exception as e:
            logger.exception("Get distance failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _set_vlx_led(self, req):
        device_id = int(req.match_info['id'])
        sensor_id = int(req.match_info['sensor'])

        try:
            dat = await req.json()
            req_on = dat['on']
            vlx = self._manager._boards[device_id]

            dat = await vlx.set_led(sensor_id, req_on)
            return dat

        except Exception as e:
            logger.exception("Set led failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_vlx_distances(self, req):
        device_id = int(req.match_info['id'])

        try:
            vlx = self._manager._boards[device_id]

            dat = await vlx.get_distances()
            return dat

        except Exception as e:
            logger.exception("Get distances failure to {}".format(device_id))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_vlx_distances_ws(self, req):
        """Create a websocket for getting distances"""
        device_id = int(req.match_info['id'])
        ws = aiohttp.web.WebSocketResponse()
        await ws.prepare(req)
        logger.info("VLX distance websocket established")

        self._vxl_dist_ws.add(ws)

        try:
            handler = functools.partial(self._push_dist_to_ws, ws)
            self._manager._boards[device_id] \
                .register_distances_handler(handler)

            async for msg in ws:
                msg  # unused

        except Exception as e:
            logger.exception("WS Register Failure")
            ws.send_json({"status": "error", "error": repr(e)})

        try:
            self._manager._boards[device_id] \
                .unregister_distances_handler(handler)
        except Exception:
            logger.exception("WS Unregister Failure")

        logger.info('VLX distance websocket closed')
        self._vxl_dist_ws.remove(ws)

        return ws

    async def _push_dist_to_ws(self, ws, values):
        ws.send_json(values)
