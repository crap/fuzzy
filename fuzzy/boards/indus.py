import functools
import struct
from enum import Enum, IntEnum
from collections import namedtuple

from fuzzy.itf.can_obj import Category
from . import board

import logging
logger = logging.getLogger("Indus")


class IndusTriggerDir(Enum):
    OFF = 0
    RISING = 1
    FALLING = 2
    BOTH = 3


class Indus(IntEnum):
    ColorValue = 0

    DIGITAL_0 = 2
    DIGITAL_1 = 3
    DIGITAL_2 = 4
    DIGITAL_3 = 5

    ANALOG_0 = 6
    ANALOG_1 = 7

    DIGITAL_FORCE_0 = 8
    DIGITAL_FORCE_1 = 9
    DIGITAL_FORCE_2 = 10
    DIGITAL_FORCE_3 = 11


RawColor = namedtuple('RawColor', ['r', 'g', 'b', 'i'])
AnTrigger = namedtuple('AnTrigger', ['dir', 'value'])


class IndusBoard(board.Board):
    def __init__(self, manager, _id):
        super().__init__(manager, board.Type.indus, _id)

        logger.info("Indus Board #{} created!".format(_id))

        self._dig_trigs = {}
        for chid in range(4):
            self.register_notif(
                Category.Indus,
                Indus.DIGITAL_0 + chid,
                functools.partial(self._dig_trig_notif, chid)
            )

        self._an_trigs = {}
        for chid in range(2):
            self.register_notif(
                Category.Indus,
                Indus.ANALOG_0 + chid,
                functools.partial(self._an_trig_notif, chid)
            )

    async def get_raw_color(self):
        """Measure a raw color
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Indus,
            Indus.ColorValue
        )

        v = struct.unpack(">HHHH", resp.data)

        return RawColor(*(float(c)/65535 for c in v))

    async def get_dig(self, ix):
        """Get digital reading + trigger

        Returns:
            value: a bool
            trigger: the trigger mode
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Indus,
            Indus.DIGITAL_0 + ix
        )

        value, trigger = struct.unpack(">?B", resp.data)

        return value, trigger

    async def set_dig(self, ix, value):
        """Set digital output

        Params:
            ix: the digit
            value: a bool
        """
        await self.manager.can.request_set(
            self.id,
            Category.Indus,
            Indus.DIGITAL_FORCE_0 + ix,
            struct.pack(">?", value)
        )

    async def set_dig_trig(self, ix, trig_dir, handler):
        """Set analog trigger for ix

        Params:
            ix: the analog channel
            handler: handler to call in case of trigger
        """

        await self.manager.can.request_set(
            self.id,
            Category.Indus,
            Indus.DIGITAL_0 + ix,
            struct.pack(">B", trig_dir.value)
        )

        if trig_dir != IndusTriggerDir.OFF:
            self._dig_trigs[ix] = handler
        else:
            self._dig_trigs.pop(ix, None)

    async def _dig_trig_notif(self, chid, msg):
        hdl = self._dig_trigs.get(chid, None)

        if hdl is None:
            logger.warning("got a trigger on analog #{} without handler"
                           .format(chid))
            return

        val, = struct.unpack(">?", msg.data)

        await hdl(chid, val)

    async def get_an(self, ix):
        """Get analog reading + trigger

        Returns:
            value
            trigger: the analog trigger (contains dir and value)
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Indus,
            Indus.ANALOG_0 + ix
        )

        value, trig_dir, trig_val = struct.unpack(">HBH", resp.data)

        return value, AnTrigger(trig_dir, trig_val)

    async def set_an_trig(self, ix, trig_dir, trig_val, handler):
        """Set analog trigger for ix

        Params:
            ix: the analog channel
            trig_dir: trigger direction
            tig_val: trigger threshold (between 0 and 1)
            handler: handler to call in case of trigger
        """

        await self.manager.can.request_set(
            self.id,
            Category.Indus,
            Indus.ANALOG_0 + ix,
            struct.pack(">BH", trig_dir.value, int(trig_val*4095))
        )

        if trig_dir != IndusTriggerDir.OFF:
            self._an_trigs[ix] = handler
        else:
            self._an_trigs.pop(ix, None)

    async def _an_trig_notif(self, chid, msg):
        hdl = self._an_trigs.get(chid, None)

        if hdl is None:
            logger.warning("got a trigger on analog #{} without handler"
                           .format(chid))
            return

        bool_val, val = struct.unpack(">?H", msg.data)

        await hdl(chid, bool_val, val/4095)
