import struct
from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy.devices import Servo, ServoPosition

from . import board

import logging
logger = logging.getLogger("Futabas")


class FutabasServo(Servo):
    def __init__(self, board, servo_id):
        Servo.__init__(self, "FutabasServo{}-{}".format(board.id, servo_id))

        self._board = board
        self._servo_id = servo_id

    async def _set(self, position):
        await self._board.set_position(
                self._servo_id, position.position, position.torque,
                position.duration)

    async def _release(self):
        await self._board.release(self._servo_id)

    async def _get(self):
        position, torque = await self._board.get_position(self._servo_id)

        return ServoPosition(position, torque=torque)


class Futabas(IntEnum):
    Futaba0 = 0
    Futaba1 = 1
    Futaba2 = 2
    Futaba3 = 3
    Futaba4 = 4
    Futaba5 = 5


class FutabasBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.futabas, _id)

        logger.info("Futabas Board #{} created!".format(_id))

        self._servos = [FutabasServo(self, i) for i in range(1, 6)]
        for s in self._servos:
            manager.fz.devices.add_servo(s)

    def servo(self, ix):
        """Simple access to a servo instance"""
        return self._servos[ix - 1]

    async def set_position(self, servo_id, position=0, torque=100,
                           duration=0):
        """Set position of a servo, between -180° and +180°

        Arguments:
            servo_id: the ID of the servo (0-5)
            position the position, value -180/180°
            torque: the max torque to use, value 0-100
            duration: the time to take, in seconds
        """
        assert servo_id in range(6)
        assert isinstance(position, (int, float))
        assert isinstance(torque, (int, float))
        assert isinstance(duration, (int, float))

        # clip position to -150/150
        position = max(-150, min(position, 150))
        duration = max(0, min(duration, 10))
        torque = (max(0, min(torque, 100)))

        # convert position 0-100 to -1500 - 1500
        position = round(position * 10)
        torque = int(torque)
        duration = int(duration * 100)

        await self.manager.can.request_set(
            self.id,
            Category.Futabas,
            Futabas.Futaba0 + servo_id,
            dat=struct.pack(">hBH", position, torque, duration)
        )

    async def release(self, servo_id):
        """Release a servo"""
        assert servo_id in range(6)

        await self.manager.can.request_set(
            self.id,
            Category.Futabas,
            Futabas.Futaba0 + servo_id,
        )

    async def get_position(self, servo_id):
        """Get the position and current torque of a servo

        Arguments:
            servo_id: the ID of the servo (0-5)
        Returns:
            position the position, value -150/150
            torque: the max torque to use, value 0-100
        """
        assert servo_id in range(6)

        resp = await self.manager.can.request_get(
            self.id,
            Category.Futabas,
            Futabas.Futaba0 + servo_id
        )

        position, torque = struct.unpack(">hB", resp.data)
        # convert position -1500 - 1500 to 0-100
        position /= 10
        logger.info("Got pos {} tq {}".format(position, torque))

        return position, torque
