import struct
from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy.color import Color

from . import board

import logging
logger = logging.getLogger("Pow")


class LEDColor:
    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b

    @classmethod
    def from_json(cls, dat):
        return cls(dat['r'], dat['g'], dat['b'])

    @property
    def packed(self):
        return struct.pack('>BBB', int(self.r), int(self.g), int(self.b))

    @classmethod
    def off(cls):
        return cls(0, 0, 0)

    @classmethod
    def white(cls):
        return cls(255, 255, 255)

    @classmethod
    def turquoise(cls):
        return cls(0, 255, 255)

    @classmethod
    def red(cls):
        return cls(255, 0, 0)

    @classmethod
    def green(cls):
        return cls(0, 255, 0)

    @classmethod
    def orange(cls):
        return cls(0xff, 0xa5, 0)

    @classmethod
    def blue(cls):
        return cls(0, 0, 255)

    @classmethod
    def yellow(cls):
        return cls(255, 255, 0)


class Pow(IntEnum):
    """resources"""
    JACK = 2
    COLOR = 3
    STRAT = 4

    BTN0 = 5
    BTN1 = 6
    BTN2 = 7

    LED_OFF = 10
    LED_BLINK = 11
    LED_CHASER = 12
    LED_CUSTOM = 13


class PowBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.pow, _id)

        self.register_notif(Category.Pow, Pow.JACK, self._jack)
        self.register_notif(Category.Pow, Pow.COLOR, self._color)
        self.register_notif(Category.Pow, Pow.BTN0, self._btn0)
        self.register_notif(Category.Pow, Pow.BTN1, self._btn1)
        self.register_notif(Category.Pow, Pow.BTN2, self._btn2)

        self._jack_handlers = set()
        self._btns_handlers = [set(), set(), set()]

        logger.info("Pow Board #{} created!".format(_id))

    def add_jack_handler(self, h):
        self._jack_handlers.add(h)

    def remove_jack_handler(self, h):
        try:
            self._jack_handlers.remove(h)
        except KeyError:
            pass

    def add_btn_handler(self, btn, h):
        self._btns_handlers[btn].add(h)

    def remove_btn_handler(self, btn, h):
        try:
            self._btns_handlers[btn].remove(h)
        except KeyError:
            pass

    async def get_jack(self):
        """Get the jack

        Returns:
            the jack : True if in, False if missing
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Pow,
            Pow.JACK
        )

        jack, = struct.unpack(">?", resp.data)
        return jack

    async def get_color(self):
        """Get the color

        Returns:
            the color : True if in, False if missing
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Pow,
            Pow.COLOR
        )

        color, = struct.unpack(">?", resp.data)
        if color:
            return Color.orange
        else:
            return Color.green

    async def get_strat(self):
        """Get the strat

        Returns:
            the strat : True if in, False if missing
        """

        resp = await self.manager.can.request_get(
            self.id,
            Category.Pow,
            Pow.STRAT
        )

        strat, = struct.unpack(">?", resp.data)
        return strat

    async def leds_off(self):
        await self.manager.can.request_set(
            self.id,
            Category.Pow,
            Pow.LED_OFF
        )

    async def leds_blink(self, color1, dur1, color2, dur2):

        dat = b''.join([
            c.packed + struct.pack('>B', int(d*100)) for c, d in
            ((color1, dur1), (color2, dur2))
            ])

        await self.manager.can.request_set(
            self.id,
            Category.Pow,
            Pow.LED_BLINK,
            dat
        )

    async def leds_chaser(self, color, dur=1):

        dat = color.packed + struct.pack('>B', int(dur*100/5))

        await self.manager.can.request_set(
            self.id,
            Category.Pow,
            Pow.LED_CHASER,
            dat
        )

    async def leds_custom(self, colors):
        dat = b"".join([c.packed for c in colors])

        await self.manager.can.request_set(
            self.id,
            Category.Pow,
            Pow.LED_CUSTOM,
            dat
            )

    async def _color(self, msg):
        color, = struct.unpack(">?", msg.data)
        logger.info("COLOR {}".format(color))

    async def _jack(self, msg):
        jack, = struct.unpack(">?", msg.data)
        logger.info("JACK {}".format(jack))

        for handler in self._jack_handlers:
            await handler(jack)

    async def _btn0(self, msg):
        await self._btnx(0, msg)

    async def _btn1(self, msg):
        await self._btnx(1, msg)

    async def _btn2(self, msg):
        await self._btnx(2, msg)

    async def _btnx(self, btn, msg):
        val, = struct.unpack(">?", msg.data)
        logger.info("BTN {} {}".format(btn, val))

        for handler in self._btns_handlers[btn]:
            await handler(val)
