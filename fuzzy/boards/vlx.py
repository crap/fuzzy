import asyncio
import struct
from enum import IntEnum

from fuzzy.itf.can_obj import Category
from fuzzy import devices

from . import board

import logging
logger = logging.getLogger("Vlx")


class Vlx(IntEnum):
    Values = 0
    PushActive = 1
    Leds = 2
    Thres = 3


class VlxBoard(board.Board):
    def __init__(self, manager, _id):
        board.Board.__init__(self, manager, board.Type.vlx, _id)
        self._distances_handlers = set()

        self.register_notif(Category.Vlx, Vlx.Values, self._distances_notif)
        self.register_notif(Category.Vlx, Vlx.Thres, self._thres_notif)

        logger.info("Vlx Board #{} created!".format(_id))

        self._vlxs = [devices.Vlx(self, i) for i in range(6)]
        for s in self._vlxs:
            manager.fz.devices.add_vlx(s)

        self._thr_handlers = [None] * 6

    def vlx(self, ix):
        """Simple access to a pump instance"""
        return self._vlxs[ix]

    async def get_distances(self):
        """Get the values of the 6 VLX distance sensors

        Returns:
            distances: 6 values in mm
        """
        resp = await self.manager.can.request_get(
            self.id,
            Category.Vlx,
            Vlx.Values
        )

        distances = struct.unpack(">BBBBBB", resp.data)
        return distances

    async def get_distance(self, ix):
        """Get the value of a single VLX sensor

        Arguments:
            ix: the sensor index, between 0 and 5 included
        Returns:
            distance: the value in mm
        """
        assert ix in range(6)

        return (await self.get_distances())[ix]

    async def set_led(self, led, on):
        """Set the value of a LED

        Params:
            led: led index
            on: boolean
        """

        dat = struct.pack(">BB", 1 << led, on << led)
        await self.manager.can.request_set(
            self.id,
            Category.Vlx,
            Vlx.Leds,
            dat=dat
        )

    async def set_threshold(self, ix, value, hyst=5, handler=None):
        """Set the value of a LED

        Params:
            ix: sensor index
            value: threshold value, in mm
            hyst: hysteresis in mm
            handler: callable to call
        """
        assert ix in range(6)
        self._thr_handlers[ix] = handler

        dat = struct.pack(">BBB", ix, value, hyst)
        await self.manager.can.request_set(
            self.id,
            Category.Vlx,
            Vlx.Thres,
            dat=dat
        )

    def register_distances_handler(self, handler):
        """Request handler to be called on each new data

        Arguments:
            handler: method to call on new data
        """

        if len(self._distances_handlers) == 0:
            asyncio.ensure_future(self._config_notif(True))

        self._distances_handlers.add(handler)

    def unregister_distances_handler(self, handler):
        """Remove a handler"""
        try:
            self._distances_handlers.remove(handler)
        except KeyError:
            logger.warning("Trying to remove unknown handler {}"
                           .format(handler))

        if len(self._distances_handlers) == 0:
            asyncio.ensure_future(self._config_notif(False))

    async def _config_notif(self, enable):
        """Configure distance notification"""

        await self.manager.can.request_set(
            self.id,
            Category.Vlx,
            Vlx.PushActive,
            struct.pack(">?", enable)
        )

    async def _distances_notif(self, msg):
        values = struct.unpack(">BBBBBB", msg.data)
        for handler in self._distances_handlers:
            await handler(values)

    async def _thres_notif(self, msg):
        ix, dist, detect = struct.unpack(">BB?", msg.data)

        if self._thr_handlers[ix]:
            await self._thr_handlers[ix](dist, detect)
