import asyncio
import logging
from .itf import can, can_rest
from .itf import rest
from . import boards, devices
from .move import Move
from .obstacles import Obstacles
from .model import Big, Little, Fake
from .match import Match

from . import acts
from .conf import Config
from .color import Color

logger = logging.getLogger("FUZZY")


class Fuzzy:
    def __init__(
            self,
            dev="vcan0",
            loop=asyncio.get_event_loop(),
            rest_port=8080,
            conf=".",
            web_folder=None,
            hok_tty="/dev/ttyO4",
            is_big=True
            ):

        self._loop = loop
        self.radius = 0.2

        logger.info("Config Folder: {}".format(conf))
        self.config = Config(conf)
        if web_folder is None:
            web_folder = "web"

        # Create base interfaces
        self.can = can.CAN(dev, 1, self._loop)
        self.rest = rest.REST(self._loop, rest_port)

        # Devices manager
        self.devices = devices.Manager(self.rest)

        self.can_rest = can_rest.CANREST(self.can, self.rest)
        self.boards = boards.Manager(self, self.can, self.rest)

        self.actions = acts.Manager(self)
        self.match = Match(self)

        # team color
        self.color = Color.unknown

        logger.info("Fuzzy ready for action!")

        if is_big is True:
            self.model = Big(self)
        elif is_big is False:
            self.model = Little(self)
        else:
            self.model = Fake(self)

        self.move = Move(self, self.model.move_board)
        self.obstacles = Obstacles(self)
        self.obstacles.reload()
        self.move.recompute()

        # Make sure boards get powered automatically
        self.boards.enable_auto_power()

        # main endpoints
        self.rest.register_put("/color/{color}", self._put_color)

        # All REST registration must appear before!
        self.rest.enable_static(web_folder)

        # REST start is important
        self._loop.run_until_complete(self.rest.start())
        asyncio.ensure_future(self._start(), loop=self._loop)

    async def _start(self):
        await asyncio.sleep(3)
        # Start model!
        await self.model.start()

    def stop(self):
        self._loop.run_until_complete(self.rest.stop())

    def set_color(self, color):
        assert isinstance(color, Color)
        logger.info("NEW COLOR => {}".format(color))

        self.color = color

        # Reset initial position
        asyncio.ensure_future(
            self.move.reset_position(self.model.get_start_pos(color))
            )

        self.obstacles.reload()
        self.move.recompute()
        self.match.update_color()

    async def _put_color(self, req):
        color = req.match_info['color']
        self.set_color(Color[color])
