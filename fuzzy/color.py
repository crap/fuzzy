import enum


class Color(enum.Enum):
    unknown = 0
    green = 1
    orange = 2

    def is_valid(self):
        return self != Color.unknown

    def is_green(self):
        return self == Color.green

    def is_orange(self):
        return self == Color.orange
