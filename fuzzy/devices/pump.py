import asyncio


class Pump:
    def __init__(self, board, pump_id):
        self.name = "Pump#{}-{}".format(board.id, pump_id)
        self.description = ""

        self._board = board
        self._pump_id = pump_id

    def __repr__(self):
        return self.name

    async def suck(self, wait_duration=0):
        """Set the pump ON, and wait for it to be ready"""
        await self._board.set_pump(self._pump_id, True)
        await asyncio.sleep(wait_duration)

    async def release(self, duration=None, wait_duration=0):
        """Release the succion cup, and wait for it to be ready"""
        if duration is None:
            duration = 2

        await self._board.set_pump(self._pump_id, False)
        await self._board.release(duration)

        await asyncio.sleep(wait_duration)

    async def sucking(self):
        """Get the sucking status"""
        return await self._board.sucking(self._pump_id)

    async def holding(self):
        """Get the holding status"""
        return await self._board.holding(self._pump_id)
