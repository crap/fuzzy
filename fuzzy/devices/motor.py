class Motor:
    def __init__(self, board):
        self.name = "Motor#{}".format(board.id)
        self.description = ""

        self._board = board

    def __repr__(self):
        return self.name

    async def set_pwm(self, pwm):
        """Set the PWM value (-100/100)"""
        await self._board.set_pwm(pwm)

    async def get_pwm(self, pwm):
        """Get the PWM value (-100/100)"""
        return await self._board.get_pwm()
