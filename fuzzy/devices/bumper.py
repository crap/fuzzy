import asyncio
import math
import logging
import time


from fuzzy.move import MoveTimeout, MoveBurnouted

logger = logging.getLogger("Bumper")


class Bumper:
    VLX_AVERAGE = 3

    def __init__(self, vlx_left, vlx_right, spacing, angle, offset_left=0,
                 offset_right=0, description=""):
        """

        offset will be removed from left vlx measured distance before further
        computation
        """
        self.vlx_left = vlx_left
        self.vlx_right = vlx_right
        self.spacing = spacing
        self.angle = angle
        self.offset_left = offset_left
        self.offset_right = offset_right

        self.name = description
        self.description = description

    async def measure(self, raw=False):
        """
        Measure both VLXs and return expected mean distance to wall and angle
        """

        d_left, d_right = await asyncio.gather(
            self.vlx_left.get(Bumper.VLX_AVERAGE),
            self.vlx_right.get(Bumper.VLX_AVERAGE)
            )
        logger.debug("Read LEFT {:.3f} RIGHT {:.3f}".format(d_left, d_right))

        d_left -= self.offset_left
        d_right -= self.offset_right

        if raw:
            return d_left, d_right

        d_mean = (d_left + d_right) / 2
        d_diff = d_left - d_right
        angle = math.atan2(d_diff, self.spacing)

        logger.debug("Computed dist {:.3f} angle {:.3f}".format(d_mean, angle))

        return d_mean, angle

    async def adjust(self, dist_ref, move, angle_max=15*math.pi/180,
                     dist_max=0.1, keep=False):

        # Corrent angle
        dist, angle = await self.measure()
        p = move.position

        if abs(angle) > angle_max:
            raise Exception("Can't adjust angle, too big ({})".format(angle))

        logger.debug(
            "Angle correction {:.3f} -> {:.3f}"
            .format(p.theta, p.theta - angle))
        p.theta -= angle
        try:
            await move.move_to(p, keep=keep, timeout=2)
        except (MoveTimeout, MoveBurnouted):
            pass

        await asyncio.sleep(0.3)
        t0 = time.time()
        dist, angle = await self.measure()
        t1 = time.time()
        logger.debug("Measure Time {:.1f}s".format(t1-t0))

        # Get at the right distance to the border
        d = dist - dist_ref

        if abs(d) > dist_max:
            raise Exception("Can't adjust distance, too big ({})".format(d))

        p = move.position
        t = move.target_from_here(d, self.angle)

        logger.debug(
            "Dist correction {} -> {}"
            .format(p, t))
        try:
            await move.move_to(t, keep=keep, timeout=2)
        except (MoveTimeout, MoveBurnouted):
            pass
        await asyncio.sleep(0.3)

        return await self.measure()
