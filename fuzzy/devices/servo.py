import asyncio
import logging
import copy
from collections import OrderedDict

logger = logging.getLogger("ServoDevice")


class ServoPosition:

    def __init__(self, position, torque=100, duration=None):
        self.position = position
        self.torque = torque
        self.duration = duration

    @classmethod
    def from_json(cls, dat):
        return ServoPosition(
            dat["pos"],
            dat.get("trq", 100)
            )

    def json(self):
        dat = OrderedDict()
        dat["pos"] = self.position
        dat["trq"] = self.torque

        return dat

    def __repr__(self):
        return "ServoPos({})".format(str(self.__dict__))


class Servo:
    def __init__(self, name=None, description=""):
        self.name = name
        self.description = description

        # Here is the time it take to go from -90° to +90°
        self.full_scale_time = 0.8

        self._current_pos = None

    def __repr__(self):
        return self.name

    async def set(self, pos, slow_factor=1):
        """Set the servo position, and wait for it to be ready

        :param pos: the ServoPosition indicating where to go
        :param slow_factor: indicate how slower we should move
        """
        # We need a copy of the original object
        pos = copy.copy(pos)

        if pos.duration is None:

            # Compute duration it would take
            if self._current_pos is not None:
                move_range = abs(pos.position - self._current_pos.position)
            else:
                # Unknown start position, set zero
                move_range = 0

            # Assume 180 degree full scale
            pos.duration = move_range / 180 \
                * self.full_scale_time * slow_factor

        self._current_pos = pos

        await self._set(self._current_pos)
        await asyncio.sleep(self._current_pos.duration)

    async def release(self):
        """Release the servo"""
        await self._release(self)
        self._current_pos = None

    async def get(self):
        """Get the current servo position"""
        self._current_pos = await self._get()
        return self._current_pos
