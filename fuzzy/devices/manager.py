import asyncio
import logging
from collections import defaultdict
from .servo import ServoPosition

logger = logging.getLogger("DeviceManager")


class Manager:

    def __init__(self, rest):
        # Storage for internal methods
        self._devices = defaultdict(dict)

        rest.register_get(
            "/devices/{dtype}",
            self._get)

        rest.register_get(
            "/devices/servo/{sname}",
            self._get_servo)
        rest.register_put(
            "/devices/servo/{sname}",
            self._put_servo)

        rest.register_get(
            "/devices/vlx/{sname}/distance",
            self._get_vlx_distance)
        rest.register_put(
            "/devices/vlx/{sname}/led",
            self._put_vlx_led)

        rest.register_get(
            "/devices/bumper/{sname}",
            self._get_bumper)

        rest.register_put(
            "/devices/lighter/{sname}",
            self._put_lighter)

        rest.register_put(
            "/devices/lighter/{sname}/{color}",
            self._put_lighter)

    def add_servo(self, servo):
        self._devices['servos'][servo.name] = servo

    def add_pump(self, pump):
        self._devices['pumps'][pump.name] = pump

    def add_motor(self, motor):
        self._devices['motors'][motor.name] = motor

    def add_vlx(self, vlx):
        self._devices['vlxs'][vlx.name] = vlx

    def add_bumper(self, bumper):
        self._devices['bumpers'][bumper.name] = bumper

    def add_lighter(self, lighter):
        self._devices['lighters'][lighter.name] = lighter

    async def _get(self, req):
        dtype = req.match_info['dtype']
        if dtype in self._devices:
            return {
                k: {"description": v.description} for k, v in
                self._devices[dtype].items()}
        return {"status": "error", "error": "no such type %s" % dtype}

    async def _get_servo(self, req):
        sname = req.match_info['sname']

        try:
            servo = self._devices['servos'][sname]

            pos = await servo.get()
            return {
                'position': pos.position,
                'torque': pos.torque,
            }

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Get servo failure to {}".format(sname))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_servo(self, req):
        sname = req.match_info['sname']

        try:
            servo = self._devices['servos'][sname]

            dat = await req.json()
            active = dat.get('active', True)
            position = dat.get('position', 0)
            torque = dat.get('torque', 100)
            duration = dat.get('duration', None)
            if active:
                pos = ServoPosition(position, torque, duration)
                await servo.set(pos)
            else:
                await servo.release()

            return {"status": "ok"}

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Set servo failure to {}".format(sname))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_vlx_distance(self, req):
        sname = req.match_info['sname']

        try:
            vlx = self._devices['vlxs'][sname]

            return {
                "distance": await vlx.get()
                }

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Get vlx distance failure to {}".format(sname))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_vlx_led(self, req):
        sname = req.match_info['sname']

        try:
            vlx = self._devices['vlxs'][sname]

            dat = await req.json()
            on = dat['led']
            await vlx.set_led(on)

            return {"status": "ok"}

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Set servo failure to {}".format(sname))
            return {'error': 'Board error', 'message': repr(e)}

    async def _get_bumper(self, req):
        sname = req.match_info['sname']

        try:
            bumper = self._devices['bumpers'][sname]

            d, a = await bumper.measure()
            return {
                "distance": d,
                "angle": a
                }

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Get bumper failure to {}".format(sname))
            return {'error': 'Board error', 'message': repr(e)}

    async def _put_lighter(self, req):
        sname = req.match_info['sname']

        r, g, b = 0, 0, 0
        color = req.match_info.get('color', None)

        try:
            lighter = self._devices['lighters'][sname]
            if color == "off":
                await lighter.set_off()
            elif color == "blue":
                await lighter.set_blue()
            elif color == "yellow":
                await lighter.set_yellow()
            elif color == "white":
                await lighter.set_white()
            else:
                dat = await req.json()
                r = dat.get('r', r)
                g = dat.get('g', g)
                b = dat.get('b', b)
                await lighter.set_rgb(r, g, b)

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Put lighter failure to {}".format(sname))
            return {'error': 'Board error', 'message': repr(e)}
