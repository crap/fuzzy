import asyncio
import functools
import logging


class Vlx:
    def __init__(self, board, vlx_id, description=""):
        self.name = "Vlx{}-{}".format(board.id, vlx_id)
        self.description = description

        self._board = board
        self._vlx_id = vlx_id

        self._logger = logging.getLogger(self.name)

    def __repr__(self):
        return self.name

    async def get(self, avg=1, wait=0.1):
        """Get the VLX distance, in m
        """

        pts = []
        for _ in range(avg):
            pts.append(await self._board.get_distance(self._vlx_id))
            if wait:
                await asyncio.sleep(wait)

        dist = sum(pts) / avg * 1e-3
        return dist

    async def set_led(self, on):
        """Release the LED"""
        return await self._board.set_led(self._vlx_id, on)

    async def set_thr(self, dist, hyst=5, handler=None):
        """Set the VLX thrigger distance, in m

        if handler is a future, it will be set, otherwise called
        """
        if handler:
            handler = functools.partial(self._convert_from_mm, handler)

        return await self._board.set_threshold(
            self._vlx_id,
            int(dist * 1e3),  # convert distance in mm
            hyst=hyst,
            handler=handler)

    async def _convert_from_mm(self, handler, dist, detect):
        dist *= 1e-3
        self._logger.debug("VLX trig {}".format(dist))

        if isinstance(handler, asyncio.Future):
            handler.set_result(dist)
        else:
            await handler(dist, detect)
