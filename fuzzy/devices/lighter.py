import asyncio
import logging


logger = logging.getLogger("Lighter")


class Ctx:
    def __init__(self, parent, r, g, b):
        self.parent = parent
        self.rgb = (r, g, b)

    async def __aenter__(self):
        await self.parent.set_rgb(*self.rgb)
        await asyncio.sleep(0.3)

    async def __aexit__(self, *args):
        await self.parent.set_off()

    def __await__(self):
        """HACK to be able to await this object"""
        return self.__aenter__().__await__()


class Lighter:

    def __init__(self, indus):
        """
        a bright Lighter!
        """
        self.indus = indus

        self.name = "lighter"
        self.description = "lighter"

    def set_off(self):
        return Ctx(self, 0, 0, 0)

    def set_red(self):
        return Ctx(self, 1, 0, 0)

    def set_green(self):
        return Ctx(self, 0, 1, 0)

    def set_blue(self):
        return Ctx(self, 0, 0, 1)

    def set_yellow(self):
        return Ctx(self, 1, 1, 0)

    def set_white(self):
        return Ctx(self, 1, 1, 1)

    async def set_rgb(self, r, g, b):
        """
        Set the color to light
        all params are bool
        """

        logger.debug("Setting color {} {} {}".format(r, g, b))
        await asyncio.gather(
            self.indus.set_dig(2, bool(r)),
            self.indus.set_dig(1, bool(g)),
            self.indus.set_dig(0, bool(b)),
            return_exceptions=True
        )
