import logging

from fuzzy.geo import Segment
from fuzzy.color import Color
from fuzzy.match import objects
from .obs_types import DiskObstacle, PolyObstacle

logger = logging.getLogger("Obstacles")


class Obstacles:
    def __init__(self, fz):

        self._fz = fz
        self._obs = []
        self._robots = []
        self._scan_robots = []
        self._switch_robots = []
        self._fz.rest.register_get("/obstacles", self._get_obs)
        self._fz.rest.register_get("/obstacles/robots", self._get_robs)

    def __str__(self):
        return "Obstacles({})".format(self._obs)

    def reload(self):
        self._obs.clear()

        # create all distributors
        for color in (Color.green, Color.orange):
            distrib1 = DiskObstacle(
                objects[color].distrib, 0.1,
                name="distrib 1 " + str(color))
            self._obs.append(distrib1)

            distrib2 = DiskObstacle(
                objects[color].mixed_distrib, 0.1,
                name="distrib 2 " + str(color))
            self._obs.append(distrib2)

        logger.info("Color: {} obstacles {}"
                    .format(self._fz.color, self._obs))

    async def _get_obs(self, req):
        dat = []
        for i in self._obs:
            dat.append(i.to_json())
        return dat

    async def _get_robs(self, req):
        dat = []
        for r in self._robots:
            dat.append(r.to_json())
        return dat

    def first_obstacle_on_path(self, A, B, margin=0.001, static=True,
                               robots=True):
        """Get the first obstacle on path from A to B

        If only_robots is True, only robots are checked for collision

        Returns:
            the first Obstacle, or None if none
        """

        path = Segment(A, B)

        first = None
        first_dist = 1e6

        known_obs = []
        if static:
            known_obs += self._obs

        if robots:
            known_obs += self._robots

        for o in known_obs:
            dist = o.intersect(path, self._fz.radius - margin)
            if dist is None:
                continue

            if first_dist is None or dist < first_dist:
                first = o
                first_dist = dist

        return first

    def set_robots(self, robots, from_scan=True):
        """Set the list of robot obstacles"""
        valid_robots = []
        for (ix, rob) in enumerate(robots):
            if not (rob.x > -0.05 and rob.x < 2.05 and
                    rob.y > -0.05 and rob.y < 3.05):
                continue

            for cluster in objects.loc_high_objects:
                if (rob - cluster).xy_norm < 0.15:
                    break
            else:
                obs = DiskObstacle(rob, 0.2, "Robot-{}".format(ix))
                valid_robots.append(obs)

        if from_scan:
            self._scan_robots = valid_robots
        else:
            self._switch_robots = valid_robots

        self._robots = self._scan_robots + self._switch_robots

    def get_robots(self):
        """Return the list of robot obstacles"""
        return list(self._robots)
