import logging
import math

from fuzzy.geo import Segment

logger = logging.getLogger("Obstacles")


class Obstacle:
    def __init__(self, name):
        self.name = name

    def to_json(self):
        """Json ready representation of the Obstacle"""
        return {"name": self.name}

    def reversed(self, name=None):
        """Return a copy of self, reversed"""
        if name is None:
            name = self.name
        return Obstacle(name)

    def intersect(self, segment, robot_radius=0, on_right=True):
        """Check if the segment intersects the obstacle

        Returns:
            the distance on the segment of the intersection,
                or None if no intersect
        """

        raise Exception("Intersect Not Defined for {}"
                        .format(self.__class__.__name__))


class DiskObstacle(Obstacle):
    def __init__(self, center, radius, name=""):
        Obstacle.__init__(self, name)
        """Create a Disk Obstacle, with center and radius"""
        self.center = center
        self.radius = radius

    def reversed(self, name=None):
        """Return a copy of self, reversed"""
        if name is None:
            name = self.name

        return DiskObstacle(
            self.center.reversed(),
            self.radius,
            name=name)

    def to_json(self):
        dat = Obstacle.to_json(self)
        dat["type"] = "disk"
        dat["center"] = self.center.to_json()
        dat["radius"] = self.radius
        return dat

    def __repr__(self):
        return "Disk-{}({}, {})".format(self.name, self.center, self.radius)

    def intersect(self, segment, robot_radius=0, on_right=True):
        if segment.xy_norm == 0:
            # Doesn't intersect
            return None

        # Intersect if cross product is less than radius
        total_radius = self.radius + robot_radius

        PO = self.center - segment.A
        u = segment.unit

        a = u.cross(PO)
        if abs(a) > total_radius:
            # Way too far on the side
            return None

        b = u.dot(PO)
        if b < 0:
            # Obstacle behind!
            return None

        c = math.sqrt(total_radius**2 - a**2)
        if (b-c) > segment.xy_norm:
            # Stop before obstacle
            return None

        return b - c


class PolyObstacle(Obstacle):
    def __init__(self, A, B, C, *args, name=""):
        Obstacle.__init__(self, name)
        """Create a polygon obstacle, with a list of points"""
        self.edges = [A, B, C] + list(args)

    def reversed(self, name=None):
        """Return a copy of self, reversed"""
        if name is None:
            name = self.name

        edges = reversed([e.reversed() for e in self.edges])
        return PolyObstacle(*edges, name=name)

    def to_json(self):
        dat = Obstacle.to_json(self)
        dat["type"] = "poly"
        dat["edges"] = [e.to_json() for e in self.edges]
        return dat

    def _intersect_segment(self, mov_segment, robot_radius=0):
        """Return the list of intersecting segments, and the distance TODO"""
        # Intersect if any of the segment intersect
        first_dist = None
        first_segs = []

        for ix, pt in enumerate(self.edges):
            seg = Segment(pt, self.edges[(ix + 1) % len(self.edges)])

            # Check for direct intersection
            pi = seg.circle_moving_collision(mov_segment, robot_radius)
            dist = None
            if pi is not None:
                dist = (pi - mov_segment.A).xy_norm

            if dist:
                if first_dist is None or dist < first_dist:
                    first_dist = dist
                    first_segs = [ix]
                elif dist == first_dist:
                    # Append to list
                    first_dist = dist
                    first_segs.append(ix)

        return first_segs, first_dist

    def intersect(self, mov_seg, robot_radius=0):
        return self._intersect_segment(mov_seg, robot_radius)[1]

    def __repr__(self):
        return "Poly({})".format(self.name)
