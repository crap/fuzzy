import asyncio
import logging

from fuzzy.boards.indus import IndusTriggerDir
from fuzzy import geo

logger = logging.getLogger("IndusScanner")

ADV_RADIUS = 0.15


class IndusDevice:
    def __init__(self, name, sensor_loc, sensor_distance):
        self.name = name
        self.sensor_loc = sensor_loc
        self.sensor_distance = sensor_distance

    def set_detect_handler(self, handler):
        """Register handler to call on each detection"""
        raise NotImplementedError()

    @property
    def detects(self):
        """Returns True if currently detects a robot"""
        raise NotImplementedError()

    async def refresh(self):
        """Request a manual update of sensor reading"""
        raise NotImplementedError()

    def get_robot_location(self, position):
        """Get the location of the adversary robot if this sensor detects"""

        p = self.sensor_loc
        p += geo.Vector(self.sensor_distance + ADV_RADIUS, 0) \
            .rotated(self.sensor_loc.theta)

        return position + p.rotated(position.theta)


class IndusDigDevice(IndusDevice):
    def __init__(self, name, sensor_loc, sensor_distance,
                 board, channel, invert=False):
        IndusDevice.__init__(self, name, sensor_loc, sensor_distance)
        self._board = board
        self._ch = channel
        self._invert = invert

        self._detect = False
        self._handler = None

        asyncio.ensure_future(self._register())

    def set_detect_handler(self, handler):
        self._handler = handler

    @property
    def detects(self):
        return self._detect

    async def _register(self):
        """Register handler for changes"""
        while True:
            try:
                await self._board.set_dig_trig(
                    self._ch,
                    IndusTriggerDir.BOTH,
                    self._detected)
                break
            except Exception:
                logger.warning("Config of indus dig {} failed"
                               .format(self._board))
                await asyncio.sleep(6)

    async def refresh(self):
        """manual refresh sensor reading"""
        detect, _ = await self._board.get_dig(self._ch)
        await self._detected(self._ch, detect)

    async def _detected(self, ix, detect):
        """handler for detection"""
        # ignore ix

        detect = detect ^ self._invert
        if detect == self._detect:
            return

        # Change detected, store & notify
        self._detect = detect

        if self._handler is not None:
            self._handler()


class IndusAnaDevice(IndusDevice):
    def __init__(self, name, sensor_loc, sensor_distance,
                 board, channel, dist, invert=False):
        IndusDevice.__init__(self, name, sensor_loc, sensor_distance)
        self._board = board
        self._ch = channel
        self._dist = dist
        self._invert = invert

        self._detect = False
        self._handler = None

        asyncio.ensure_future(self._register())

    def set_detect_handler(self, handler):
        self._handler = handler

    @property
    def detects(self):
        return self._detect

    async def _register(self):
        """Register handler for changes"""
        while True:
            try:
                await self._board.set_an_trig(
                    self._ch,
                    IndusTriggerDir.BOTH,
                    self._detected)
                break
            except Exception:
                logger.warning("Config of indus an dig {} failed"
                               .format(self._board))
                await asyncio.sleep(6)

    async def refresh(self):
        """manual refresh sensor reading"""
        val, _ = await self._board.get_an(self._ch)
        detect = val >= self._dist

        await self._detected(self._ch, detect, val)

    async def _detected(self, ix, detect, val):
        """handler for detection"""
        # ignore ix

        detect = detect ^ self._invert
        if detect == self._detect:
            return

        # Change detected, store & notify
        self._detect = detect

        if self._handler is not None:
            self._handler()


class IndusVlxDevice(IndusDevice):
    def __init__(self, name, sensor_loc, sensor_distance,
                 vlx, dist):
        IndusDevice.__init__(self, name, sensor_loc, sensor_distance)
        self._vlx = vlx
        self._dist = dist

        self._detect = False
        self._handler = None

        asyncio.ensure_future(self._register())

    def set_detect_handler(self, handler):
        self._handler = handler

    @property
    def detects(self):
        return self._detect

    async def _register(self):
        """Register handler for changes"""
        while True:
            try:
                await self._vlx_raiser.set_thr(
                    self._dist, handler=self._detected)
                break
            except Exception:
                logger.warning("Config of indus vlx {} failed"
                               .format(self._board))
                await asyncio.sleep(6)

    async def refresh(self):
        """manual refresh sensor reading"""
        dist = await self._vlx.get()
        detect = dist < self._dist

        await self._detected(dist, detect)

    async def _detected(self, dist, detect):
        """handler for detection"""

        if detect == self._detect:
            return

        # Change detected, store & notify
        self._detect = detect

        if self._handler is not None:
            self._handler()


class IndusScanner:
    def __init__(self, fz, devices):
        self.fz = fz
        self.devices = devices

        for ix, d in enumerate(self.devices):
            d.set_detect_handler(self._notify)

        asyncio.ensure_future(self._periodic_watch())

    def _notify(self):
        robots = []
        for d in self.devices:
            if d.detects:
                robots.append(d.get_robot_location(self._fz.move.position))

        self.fz.obstacles.set_robots(robots, from_scan=False)

    async def _periodic_watch(self):
        while True:
            await asyncio.sleep(5)

            for d in self.devices:
                try:
                    await d.refresh()
                except Exception:
                    logger.warning("Refresh of {} failed".format(d.name))
                    continue
