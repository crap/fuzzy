import logging
import math

from .hokuyo import Scan
from .. import geo

from typing import List, Optional, Sequence

# hokuyo specs
VALID_DIST_MIN = 40
VALID_DIST_MAX = 4000
MIN_ANGLE = -2*math.pi/3
MAX_ANGLE = 2*math.pi/3
STEP_ANGLE = 2*math.pi/1024
CENTER_IX = 384

# cluster specs
CLUSTER_THRES_MM = 40


logger = logging.getLogger("Reco")


class Cluster:
    def __init__(self, start_ix, end_ix, center_ix, center_dist, width):
        self.start_ix = start_ix
        self.end_ix = end_ix
        self.center_ix = center_ix
        self.center_dist = center_dist
        self.width = width

    @property
    def center(self):
        """Access the location of center, as Vector"""
        # th = (self.center_ix - CENTER_IX) * STEP_ANGLE * (-1)  # upside down
        th = (self.center_ix - CENTER_IX) * STEP_ANGLE
        x = self.center_dist * math.cos(th)
        y = self.center_dist * math.sin(th)

        return geo.Vector(x, y, th)

    @property
    def json(self):
        return {
            "raw": {
                "start_ix": self.start_ix,
                "end_ix": self.end_ix,
                "center_ix": self.center_ix,
                "center_dist": self.center_dist,
                "width": self.width,
            },
            "center": self.center.to_json(),
        }

    @classmethod
    def from_json(cls, d):
        r = d["raw"]
        return cls(int(r["start_ix"]),
                   int(r["end_ix"]),
                   int(r["center_ix"]),
                   float(r["center_dist"]),
                   float(r["width"]))

    def __repr__(self):
        return "Cluster(center={})".format(self.center)


def cluster_compute(s: Scan, start: int, end: int,
                    dmin: float, dsum: float) -> Cluster:
    assert end > start

    start += s.start
    end += s.start
    size = end - start + 1

    center_ix = (start + end) // 2
    center_dist = dsum / size * 1e-3
    width = center_dist * size * STEP_ANGLE

    return Cluster(start, end, center_ix, center_dist, width)


def compute_clusters(s: Scan,
                     target_angle: float = 0,
                     target_dist: Optional[float] = None,
                     margin: Optional[float] = float('inf')) -> List[Cluster]:
    """Take a scan, return a list of clusters matching criteria

    Criteria in mm for distance and margin, angle in rad
    """

    if margin is None or target_dist is None:
        dmin, dmax = float('-inf'), float('inf')
        amin, amax = MIN_ANGLE, MAX_ANGLE
    else:
        dmin, dmax = target_dist - margin, target_dist + margin
        a_margin = margin/STEP_ANGLE/target_dist
        amin, amax = target_angle - a_margin, target_angle + a_margin
        amin, amax = max(amin, MIN_ANGLE), min(amax, MAX_ANGLE)

    clusters = []  # type: List[Cluster]
    cur_clust_start = None  # type: Optional[int]
    cur_clust_dmin = 0
    cur_clust_dsum = 0
    last_p = None  # type: Optional[int]

    for i, p in enumerate(s.points):
        if cur_clust_start is None:
            # no ongoing cluster

            if p < VALID_DIST_MIN or p > VALID_DIST_MAX:
                # bad point
                continue

            if p < dmin or p > dmax:
                # out of expected distance range
                continue

            angle = MIN_ANGLE + (i + s.start - 44) * STEP_ANGLE
            if angle < amin:  # TODO: check this
                continue

            if angle > amax:  # TODO: check this
                break

            cur_clust_start = i
            cur_clust_dmin = p
            cur_clust_dsum = p

        else:
            assert i > 0

            # distance from last point
            dist_square = (p - last_p)**2
            if p < dmin or p > dmax or dist_square > CLUSTER_THRES_MM**2:
                if cur_clust_start != i-1:
                    # end of cluster
                    clust = cluster_compute(s, cur_clust_start, i-1,
                                            cur_clust_dmin, cur_clust_dsum)
                    clusters.append(clust)

                cur_clust_start = None
                cur_clust_dmin = 0
                cur_clust_dsum = 0
            elif p < VALID_DIST_MIN or p > VALID_DIST_MAX:
                # add the mean
                cur_clust_dsum += cur_clust_dsum / (i - cur_clust_start)
            else:
                cur_clust_dsum += p
                cur_clust_dmin = min(p, cur_clust_dmin)

        last_p = p

    # finish last cluster if current not closed
    if cur_clust_start is not None and i-1 > cur_clust_start:
        clust = cluster_compute(s, cur_clust_start, i-1,
                                cur_clust_dmin, cur_clust_dsum)
        clusters.append(clust)

    return clusters


def filter_clusters(clusters: Sequence[Cluster], direction=0, beam=2*math.pi/3, dmax=3):
    """Filter clusters for thoses matching params"""
    new_clusters = []
    for c in clusters:
        if abs(c.center.xy_angle - direction) < beam/2 and c.center.xy_norm < dmax:
            new_clusters.append(c)
    return new_clusters


def obstacle_on_path(scan: Scan, direction=0, width=math.pi/8, threshold=0.5,
                     all_closer=False):
    """Check if an obstacle is in the way

    Arguments:
        scan: a Scan
        direction: the angle to look at, in rad. 0 for straight ahead
        width: th angle width to look at, in rad
        threshold: if object closed than this, trigger! in m
        all_closer: trigger mode, if True, all points must be closer,
            otherwise any point closer will trigger
    Returns:
        the distance, in m of the obstacle, or None if none
    """

    # Compute indices in given scan
    direction = int(384 + direction/(2*math.pi)*1024)
    angle = max(1, int(width/(2*math.pi)*1024))

    start_ix = direction - scan.start - angle // 2
    end_ix = start_ix + angle

    matches = []
    for i in range(start_ix, end_ix):
        di = scan.points[i] / 1000

        if di < 0.02:
            continue

        if di < threshold:
            matches.append(di)

    if all_closer and len(matches) < angle:
        return None

    if len(matches) == 0:
        return None

    return min(matches)
