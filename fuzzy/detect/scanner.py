import asyncio
import logging
import math

from .reco import compute_clusters, obstacle_on_path


logger = logging.getLogger('Scanner')


class Scanner:
    """Launch this as a task, cancel it when not needed"""
    def __init__(self, hok, loop=None):
        self._hok = hok
        self._loop = loop
        self._cluster_listeners = {}
        self._scan_listeners = {}
        self._trigger_listeners = {}
        self._task = None
        self._run = False
        self._nstarted = 0

        if self._hok:
            asyncio.ensure_future(self._hok.laser_enable(), loop=self._loop)

    def listen_to_scans(self, handler):
        """Register a handler to be called on each scan"""
        sub = Sub(self)
        self._scan_listeners[sub] = handler

        return sub

    def listen_to_clusters(self, handler):
        """Register a handler to be called on clusters"""
        sub = Sub(self)
        self._cluster_listeners[sub] = handler

        return sub

    async def wait_for_cluster(self):
        fut = asyncio.Future()

        def handler(clusters, scan):
            fut.set_result((clusters, scan))

        try:
            sub = self.listen_to_clusters(handler)

            self.start()

            clusters, scan = await fut

            return clusters, scan
        finally:
            self.unsubscribe(sub)
            self.stop()

    def trigger_on_threshold(self, handler, threshold, direction=0,
                             angle=math.pi/60):
        """Register a handler to be called on direct obstacle detection

        Arguments:
            handler: handler to call, prototype: handler(dist)
            threshold: distance to notify handler, in m
            direction: angle from hokuyo to look for (default=0), in rad
            angle: beam angle to look for (default: 3deg), in rad

        Return:
            a context manager to use with 'with' for auto unsubscribe
        """
        sub = TriggerSub(self, threshold, direction, angle)
        self._trigger_listeners[sub] = handler

        return sub

    def unsubscribe(self, sub):
        """Unsubscribe the subscription"""
        if sub in self._trigger_listeners:
            del self._trigger_listeners[sub]

    async def run(self):
        logger.info("Obstacle scanner started")
        self._run = True

        while self._run:
            # Scan and parse
            try:
                scan = await self._hok.scan_single()
                # logger.debug("Scan: {}".format(scan))
            except Exception:
                logger.error("Scan Failed")
                await asyncio.sleep(1)
                continue

            # Call scan handlers
            for sub, handler in self._scan_listeners.items():
                handler(scan)

            # Call cluster handlers
            clusters = compute_clusters(scan)
            # logger.debug("Clusters: {}".format(clusters))

            if len(clusters):
                # logger.info('{} clusters detected'.format(len(clusters)))
                for sub, handler in self._cluster_listeners.items():
                    handler(clusters, scan)  # also give the scan

            # Custom parsing
            for sub, handler in self._trigger_listeners.items():
                d = sub.compute(scan)
                if d is not None:
                    logger.info("Direct {} detected d={}".format(sub, d))
                    handler(d)

        logger.info("Obstacle scanner stopped")

    def start(self):
        logger.info("Scanner Start")
        if self._nstarted != 0:
            # already started, nothing to do
            self._nstarted += 1
            return

        self._task = asyncio.ensure_future(self.run())

    def stop(self):
        logger.info("Scanner Stop required")

        if self._nstarted == 0:
            # already stopped, nothing to do
            return

        self._nstarted -= 1

        if self._nstarted == 0:
            self._run = False
            self._task = None


class Sub:
    def __init__(self, scanner):
        self.scanner = scanner

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.scanner.unsubscribe(self)


class TriggerSub(Sub):
    def __init__(self, scanner, threshold, direction, angle):
        Sub.__init__(self, scanner)

        self._threshold = threshold
        self._direction = direction
        self._angle = angle

    def compute(self, scan):
        """Compute scan with stored parameters, call handler if necessary"""

        return obstacle_on_path(scan, self._direction, self._angle,
                                self._threshold)
