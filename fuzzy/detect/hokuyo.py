import asyncio
from contextlib import contextmanager
import traceback

# typing
from asyncio import AbstractEventLoop, Future  # noqa: F401
from typing import Callable, Generator, NamedTuple, Sequence
from serial import Serial


ByteFilter = Callable[[bytes], bool]


class CrcError(Exception):
    pass


def cksum(d: bytes) -> int:
    s = 0
    for c in d:
        s = (s + c) & 0x3f
    return s + 0x30


def split_response(resp: bytes) -> Sequence[bytes]:
    groups = resp.split(b'\n')

    for g in groups[1:]:
        assert len(g) != 0

        ck = cksum(g[:-1])
        rxck = g[-1]
        if ck != rxck:
            # for some info commands ('VV' for example), we need
            # to compute the sum without the last ';' char
            ck = cksum(g[:-2])
            if ck != rxck:
                raise CrcError('bad checksum on group {} ({:02x}/{:02x})'.
                               format(g, ck, rxck))

    return (groups[0],) + tuple([g[:-1] for g in groups[1:]])


def bn_dec(d: bytes) -> int:
    assert len(d) in (2, 3, 4)
    s = 0
    for k, v in enumerate(reversed(d)):
        s += ((v - 0x30) << (6*k))

    return s


def decode_array(data: bytes, enc: int):
    assert enc in (2, 3)

    values = []
    assert len(data) % enc == 0
    for k in range(0, len(data), enc):
        values.append(bn_dec(data[k:k+enc]))

    return values


class HokuyoConn:
    def __init__(self, loop: AbstractEventLoop = None,
                 filter: ByteFilter = None) -> None:
        self._q = asyncio.Queue()  # type: asyncio.Queue
        self._loop = loop
        self.filter = filter

    async def get(self, timeout: float = 0.2):
        task = asyncio.Task(self._q.get(), loop=self._loop)
        done, pending = await asyncio.wait([task], timeout=timeout)
        if task in pending:
            raise Exception("Timeout")

        return task.result()

    def put(self, d: bytes):
        self._q.put_nowait(d)


_Scan = NamedTuple('Scan', [('start', int),
                            ('end', int),
                            ('points', Sequence[int])])


class Scan(_Scan):
    @property
    def json(self):
        return {"start": self.start,
                "end": self.end,
                "points": self.points[:],
                }

    @classmethod
    def from_json(cls, d):
        return cls(int(d["start"]),
                   int(d["end"]),
                   [int(e) for e in d["points"]])


class HokuyoDev:
    """Send commands to an Hokuyo"""

    def __init__(self, dev: Serial, loop: AbstractEventLoop = None) -> None:
        self.dev = dev
        self.conns = set()  # type: Set[HokuyoConn]
        if loop is None:
            loop = asyncio.get_event_loop()

        self._loop = loop
        self._streamr = asyncio.streams.StreamReader(loop=self._loop)
        self._loop.add_reader(dev.fileno(), self._data_avail)
        self._cnt = 0   # counter for hokuyo command/response
        self._eof = False

        # bug: python/typeshed #794
        self._rtask = asyncio.ensure_future(self._reader(),  # type: ignore
                                            loop=self._loop)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc, tb):
        self.close()

    async def _reader(self) -> None:
        while not self._eof:
            try:
                d = await self._streamr.readuntil(b'\n\n')
            except (asyncio.streams.LimitOverrunError,
                    asyncio.streams.IncompleteReadError):
                continue

            d = d.replace(b'\n\n', b'')

            if len(d) == 0:
                continue

            for c in iter(self.conns):
                if c.filter is not None and not c.filter(d):
                    continue

                try:
                    c.put(d)
                except asyncio.QueueFull:
                    # drop data when the queue is full
                    pass

    def _data_avail(self) -> None:
        try:
            data = self.dev.read(self.dev.inWaiting())
        except Exception:
            traceback.print_exc()
            return

        self._streamr.feed_data(data)

    @contextmanager
    def connection(self, filter: ByteFilter = None) -> Generator[HokuyoConn, None, None]:
        c = HokuyoConn(loop=self._loop, filter=filter)
        self.conns.add(c)
        yield c
        self.conns.remove(c)

    def _build_command(self, cmd: bytes, params: bytes = b'') -> bytes:
        c = self._cnt
        self._cnt = (self._cnt + 1) % (10000)
        cnt_str = ';{:04d}'.format(c).encode('ascii')

        return cmd + params + cnt_str + b'\n'

    async def command(self, cmd: bytes, params: bytes = b'') -> Sequence[bytes]:
        msg = self._build_command(cmd, params)
        msglen = len(msg)

        def check_echo(s: bytes) -> bool:
            return len(s) > msglen and s[:msglen] == msg

        with self.connection(filter=check_echo) as c:
            self.dev.write(msg)
            resp = await c.get()

        return split_response(resp)

    def close(self) -> None:
        self._eof = True
        if self._streamr is not None:
            self._streamr.feed_eof()
        self._loop.run_until_complete(self._rtask)

    # -- commands --

    async def laser_enable(self, en: bool = True) -> bool:
        if en:
            _, status = await self.command(b'BM')
            return status in (b'00', b'02')
        else:
            _, status = await self.command(b'QT')
            return status == b'00'

    async def set_speed(self, speed: int) -> bool:
        _, status = await self.command(b'CR',
                                       '{:02d}'.format(speed).encode())
        return status in (b'00', b'03')

    async def last_scan(self, start: int = 44, end: int = 725) -> Scan:
        _, status, *r = await self.command(
                b'GD',
                '{:04d}{:04d}{:02d}'.format(start, end, 0)
                .encode())

        if status != b'00':
            raise RuntimeError('failed scan')

        r = r[1:]
        data = b''.join(r)

        values = decode_array(data, 3)

        assert len(values) == end - start + 1

        return Scan(start, end, values)

    async def scan_single(self, start: int = 44, end: int = 725) -> Scan:
        """Perform a single scan

        Arguments:
            start: index of scan start angle, default 44, min 44
            end: index of scan end angle, default 725, max 725

        Note:
            front facing is angle 384

        Returns:
            a Scan object
        """
        msg = self._build_command(
                b'MD',
                '{:04d}{:04d}{:02d}{:01d}{:02d}'.format(start, end, 0, 0, 1)
                .encode())
        msglen = len(msg)

        def check_scan_res(s: bytes) -> bool:
            return (len(s) > msglen and
                    s[:13] == msg[:13] and
                    s[15:msglen] == msg[15:msglen])

        with self.connection(filter=check_scan_res) as c:
            # launch scan
            self.dev.write(msg)

            # wait for ack
            resp = await c.get()
            _, status, *r = split_response(resp)

            if status != b'00':
                raise RuntimeError('failed scan')

            # wait for results
            resp = await c.get()
            _, status, *r = split_response(resp)

            if status != b'99':
                raise RuntimeError('failed scan')

            r = r[1:]
            data = b''.join(r)

            return Scan(start, end, decode_array(data, 3))

    async def set_baudrate(self, baudrate: int) -> None:
        assert baudrate in (19200, 38400, 57600, 115200,
                            250000, 500000, 750000)

        _, status = await self.command(b'SS',
                                       '{:06d}'.format(baudrate).encode())

        if status not in (b'00', b'03'):
            raise RuntimeError('failed baudrate change')

        self.dev.baudrate = baudrate
