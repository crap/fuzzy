#!/usr/bin/env python3

import argparse
import asyncio
import json
import serial

from fuzzy.detect import HokuyoDev


async def version(dev):
    resp = await dev.command(b'VV')
    for l in resp[2:]:
        print(l.decode('utf-8', errors='replace'))


async def scan(dev):
    s = await dev.scan_single()
    print(json.dumps(s._asdict()))


async def lscan(dev):
    r = await dev.laser_enable(True)
    assert r
    s = await dev.last_scan()
    print(json.dumps(s._asdict()))
    r = await dev.laser_enable(False)


async def baud(dev, baudrate):
    await dev.set_baudrate(int(baudrate))


def main():
    commands = {
            'v': version,
            'lscan': lscan,
            'scan': scan,
            'baud': baud
    }
    parser = argparse.ArgumentParser()
    parser.add_argument('hokd', metavar='HOKUYO_DEV')
    parser.add_argument('command', metavar='COMMAND', nargs='?',
                        choices=commands.keys(), default='v')
    parser.add_argument('cmd_args', metavar='CMD_ARGS', nargs='*', default=tuple())
    args = parser.parse_args()

    with HokuyoDev(serial.Serial(args.hokd)) as dev:
        asyncio.get_event_loop().run_until_complete(
                commands[args.command](dev, *args.cmd_args))
