import math

from colormath.color_objects import sRGBColor, LCHabColor
from colormath.color_conversions import convert_color


class ColorView:
    def __init__(self, w_r, w_g, w_b):
        self.white = tuple(v*1.2 for v in (w_r, w_g, w_b))
        print('WHITE: ', self.intensity(w_r, w_g, w_b))

    @staticmethod
    def intensity(r, g, b):
        return math.sqrt(r**2 + g**2 + b**2)

    def conv_raw(self, r, g, b):
        r /= self.white[0]
        g /= self.white[1]
        b /= self.white[2]

        i = self.intensity(r, g, b)

        c = sRGBColor(r/i, g/i, b/i)

        lch = convert_color(c, LCHabColor)

        lch.lch_l *= i
        if lch.lch_l > 100:
            lch.lch_l = 100

        return lch

    def color(self, lch):
        if lch.lch_c < 12.5:
            print("WHITE")
            return "white"

        if lch.lch_h > 190:
            print("BLUE")
            return "blue"

        print("YELLOW")
        return "yellow"

    def is_yellow(self, lch):
        return self.color(lch) == "yellow"

    def is_blue(self, lch):
        return self.color(lch) == "blue"
