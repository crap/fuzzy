#!/usr/bin/env python3

import argparse
import asyncio
import logging
import logging.handlers
import os

import fuzzy

log_formatter = '%(name)s:%(levelname)s:%(message)s'
console_log_formatter = '%(asctime)s %(name)s:%(levelname)s:%(message)s'


def config_logging(log_dir, verbose=False, extra_verbose=False):
    os.makedirs(log_dir, exist_ok=True)

    formatter = logging.Formatter(log_formatter)

    sl = logging.handlers.SysLogHandler("/dev/log")
    sl.setFormatter(formatter)

    cl_formatter = logging.Formatter(console_log_formatter)
    cl = logging.StreamHandler()
    cl.setFormatter(cl_formatter)

    fl_formatter = logging.Formatter(console_log_formatter)
    fl = logging.handlers.RotatingFileHandler(log_dir + '/fuzzy.log',
                                              backupCount=99)
    fl.doRollover()
    fl.setFormatter(fl_formatter)

    if extra_verbose:
        cl.setLevel(logging.DEBUG)
    else:
        cl.setLevel(logging.INFO)

    import __main__ as main
    progname = os.path.basename(main.__file__)

    sl.ident = progname + ": "

    rootlog = logging.getLogger()
    rootlog.setLevel(logging.DEBUG)
    rootlog.addHandler(sl)

    if verbose:
        rootlog.addHandler(cl)

    rootlog.addHandler(fl)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dev", dest="can_if", default="slcan0")
    parser.add_argument("-c", "--conf", default=None)
    parser.add_argument("-w", "--web", default=None)
    parser.add_argument("--hok-dev",
                        default="/dev/serial/by-id/usb-Hokuyo_Data_Flex_for_"
                                "USB_URG-Series_USB_Driver-if00")
    parser.add_argument("--little", dest="is_big", default=True,
                        action="store_false")

    parser.add_argument("-l", "--log-dir", dest="log_dir", default="./log")
    parser.add_argument("-v", "--verbose", action="count", default=0)

    parser.add_argument("--fake", default=False, action="store_true")

    args = parser.parse_args()
    if args.conf is None:
        if args.is_big:
            args.conf = "./conf/big"
        else:
            args.conf = "./conf/little"

    # Config for syslog + console log
    config_logging(args.log_dir, args.verbose, args.verbose > 1)

    if args.fake:
        args.can_if = "fake0"
        args.is_big = None

    fuzzy.Fuzzy(
        args.can_if,
        conf=args.conf,
        web_folder=args.web,
        hok_tty=args.hok_dev,
        is_big=args.is_big
        )

    logging.info("Fuzzy started")
    asyncio.get_event_loop().run_forever()
