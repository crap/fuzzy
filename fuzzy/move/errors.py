
class MoveNoPathFound(Exception):
    pass


class MoveTimeout(Exception):
    pass


class MoveCancelled(Exception):
    pass


class MoveBurnouted(Exception):
    pass


class MoveAvoided(Exception):
    pass
