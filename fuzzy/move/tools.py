import logging

logger = logging.getLogger("Move")


def path_length(start, path):
    """Compute a path length"""

    l = (path[0] - start).xy_norm
    for a, b in zip(path[:-1], path[1:]):
        l += (b-a).xy_norm
    return l


def optimize_path(start, path, obstacles, extra_dbg=False):
    """Optimize the path from start"""

    if extra_dbg:
        logger.info("Optimize {} -> {}".format(start, print_path(path)))

    # Optimize path
    new_path = [start]
    step_pending = None
    for ix, step in enumerate(path):

        if (step - new_path[-1]).xy_norm == 0 or \
                (step_pending is not None and
                    (step - step_pending).xy_norm == 0):
            pass

        elif step_pending and \
                obstacles.first_obstacle_on_path(new_path[-1], step):

            # Can't squeeze this, append previous
            new_path.append(step_pending)
        step_pending = step

    new_path.append(path[-1])

    # Remove start
    return new_path[1:]


def print_path(path):
    return "->".join("%s" % i for i in path)


def print_knownpaths(known):
    x = []
    items = list(known.items())
    items.sort(key=lambda k: len(k[1]))
    for k, v in items:
        x.append(
            "from %s: %s" % (k, print_path(v))
        )
    return "\n".join(x)
