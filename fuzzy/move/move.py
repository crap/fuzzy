import asyncio
import logging
import time
import math
import functools

from fuzzy import geo
from .rest import MoveRest
from .conf import GotoTolerance, GotoSpeedConfig, GotoGoalConfig, OdoConfig

from .tools import print_path, optimize_path
from .simple_kstra import SimpleKstra

from .errors import MoveTimeout, MoveAvoided, MoveCancelled, MoveNoPathFound, \
    MoveBurnouted

logger = logging.getLogger("Move")


class Move(MoveRest):

    DEFAULT_TOLERANCE = GotoTolerance(vmax=0.5, vmax_th=3.14)
    SLOW_TOLERANCE = GotoTolerance(vmax=0.25, vmax_th=3.14/3)
    TRAVEL_TOLERANCE = GotoTolerance(vmax=0.5, vmax_th=3.14,
                                     dist=0.1, theta=math.pi/4,
                                     num=1)

    def __init__(self, fz, mv_board):
        self._fz = fz
        self.board = mv_board

        MoveRest.__init__(self)

        # Load conf and register booted notif
        self._conf = self._fz.config.get("move.conf")
        self._parse_conf()

        self.board.set_booted_handler(self._config_board)

        asyncio.ensure_future(self.board.reset())

        # Initial position
        self._pos = geo.Vector(0, 0, 0)
        self._speed = geo.Vector(0, 0, 0)
        self._notif_time = 0

        self._targets = []

        self._board_goto_call = None
        self._board_goto_call_time = 0
        self._move_fut = None
        self._move_fut_exception = None

        # Current mode
        self._mode = None

        # Keep alive task
        self._ka_task = None

        # Tolerance for move
        self._tolerance = None

        # Preferred direction
        self._preferred_directions = None

        # Event listeners
        self._events = set()

        # periodic event caller
        self._events_caller = None
        self._notify()

        # Dijkstra computer
        self._simple_kstra = None
        # Future indicating when simple kstra is ready
        self._kstra_ready = None

        asyncio.ensure_future(self._avoid())

    @property
    def position(self):
        """Get current position"""
        return geo.Vector(self._pos)

    @property
    def speed(self):
        """Get current speed"""
        return geo.Vector(self._speed)

    def set_preferred_directions(self, dirs):
        self._preferred_directions = dirs

    async def reset_position(self, pos):
        """Reset the current position"""
        assert isinstance(pos, geo.Vector)
        logger.info("Position forced to {}".format(pos))
        self._pos = geo.Vector(pos)
        self._speed = geo.Vector()
        self._notif_time = 0

        try:
            await self.board.set_odo_position(pos)
        except asyncio.CancelledError:
            raise
        except Exception:
            logger.exception("Set position failed")

    def register_event(self, handler):
        """Add a handler to be notified on position change

        prototype: handler(Vector)
        """
        self._events.add(handler)

    def unregister_event(self, handler):
        """Remove a handler to be notified"""
        self._events.remove(handler)

    def _recompute_exec(self):
        self._simple_kstra = SimpleKstra(self._fz.obstacles)

    def recompute(self):
        """Need to be called by fuzzy when radius and obstacles are known"""
        if self._kstra_ready is not None:
            self._kstra_ready.cancel()
            self._kstra_ready = None

        self._kstra_ready = self._fz._loop.run_in_executor(
            None,
            self._recompute_exec)

    def _parse_conf(self):
        self._odo_config = OdoConfig.from_json(self._conf['odo_config'])
        self._goto_speed_config = \
            GotoSpeedConfig.from_json(self._conf['goto_speed_config'])
        self._goto_goal_config = \
            GotoGoalConfig.from_json(self._conf['goto_goal_config'])

    async def _config_board(self, board_id):
        """Called by fuzzy when a move board boots"""

        if board_id != self.board.id:
            logger.error("Move board ID mismatch!!!")
            return

        await asyncio.gather(
            self.board.set_odo_config(self._odo_config),
            self.board.set_goto_goal_config(self._goto_goal_config),
            self.board.set_goto_speed_config(self._goto_speed_config),
            self.board.set_position_notif(0.33, self._position_notif),
            return_exceptions=True)

        self.board.set_burn_notif(self._burnout)

        # Set power and load all configurations
        await self.board.power(True)

    async def _position_notif(self, pos):
        """Called each time position is updated by the move board"""
        now = time.time()

        if self._notif_time:
            dt = now - self._notif_time
            self._speed = (pos - self._pos) / dt

        self._notif_time = now
        self._pos = pos

        if len(self._targets) > 0 and self._board_goto_call_time > 0 and \
                time.time() - self._board_goto_call_time > 2:
            if self.speed.xy_norm < 0.01 and \
                    abs(self.speed.theta) / 3.14 * 180 < 2:
                logger.warning(
                    "Speed too low after more than 2 second of move: {}"
                    .format(self.speed))
                # await self.stop(MoveTimeout)

    def _notify(self):
        # Notify listeners of change
        for x in self._events:
            try:
                x(self.position)
            except Exception:
                logger.exception("Notify failure")
        self._events_caller = self._fz._loop.call_later(0.5, self._notify)

    def target_from_here(self, dist, angle):
        """Get a target position to move of 'dist' with 'angle'

        'angle' relative to the robot
        """

        p = self._pos

        x = geo.Vector(dist, 0, 0)
        x.rotate(angle + p.theta)

        return x + p

    async def move_to(self, target, mode="line", timeout=5,
                      tolerance=None, simulate=False, **kwargs):
        """Move to the given target position

        Arguments:
            target: vector for where to go, set theta to None if don't care
            mode: movement mode, either line (default), point, dijkstra
            timeout: if lock, max time to wait
            tolerance: tolerance for move

            log: bool to log all iterations
            keep: bool to keep after reach all iterations
            rotate_first
            backward_allowed
        Raises
            MoveTimeout
            MoveCancelled
        """

        if self._targets != []:
            logger.error("Targets not empty, someone is already moving")
            await self.stop(MoveCancelled)

        self._targets.clear()

        # Save tolerance
        self._tolerance = tolerance = tolerance or self.DEFAULT_TOLERANCE

        dm = target - self.position
        if dm.xy_norm < 0.01 and abs(dm.theta) < 0.01:
            logger.debug("Move from {} to {} : Already arrived!"
                         .format(self.position, target))
            return True

        if mode in ("line", "point"):
            if target.theta is None:
                target.theta = self.position.theta

            logger.info("Move from {} to {} with mode {}"
                        .format(self.position, target, mode))
            self._targets.append(target)

        elif mode == "dijkstra":
            try:
                path = await self._compute_dijkstra(target)
                if path is None:
                    raise Exception("Path is None")
            except Exception:
                logger.exception("Dijkstra failed")
                raise MoveNoPathFound()

            mode = "line"
            if len(self._targets) > 1:
                tolerance = self.TRAVEL_TOLERANCE

        else:
            raise NotImplementedError("Unknown move mode {}".format(mode))

        if simulate:
            return True

        self._mode = mode

        extras = {}
        if 'log' in kwargs:
            extras["log"] = kwargs['log']
        if 'keep' in kwargs:
            extras["keep"] = kwargs['keep']
        if 'backward_allowed' in kwargs:
            extras["backward_allowed"] = kwargs['backward_allowed']
        if 'rotate_first' in kwargs:
            extras["rotate_first"] = kwargs['rotate_first']
        if 'final_rotate' in kwargs:
            extras["final_rotate"] = kwargs['final_rotate']

        # Request move from board
        logger.debug("Step Move from {} to {} mode {} tol {} extra {}"
                     .format(self.position, self._targets[0], self._mode,
                             tolerance, extras))
        try:
            self._board_goto_call = functools.partial(
                self.board.goto_target,
                self._targets[0],
                handler=self._target_reached,
                tolerance=tolerance,
                **extras
                )
            self._board_goto_call_time = time.time()
            await self._board_goto_call()

        except asyncio.CancelledError:
            raise
        except Exception:
            logger.exception("Goto Target failed, hopping it will work...")

        # Start the keepalive mechanism
        if self._ka_task is not None:
            self._ka_task.cancel()
        self._ka_task = asyncio.ensure_future(self._keepalive())

        self._move_fut = asyncio.Future()
        self._move_fut_exception = None

        _, pending = await asyncio.wait(
            [self._move_fut],
            timeout=timeout)

        if self._ka_task is not None:
            self._ka_task.cancel()
            self._ka_task = None

        # Check if timeout occurred
        if self._move_fut in pending:
            self._move_fut.cancel()
            self._move_fut = None

            logger.warning("Move to {} (now {}) failed -- timeout"
                           .format(target, self.position))

            self._fz.model.led_timeout()
            await self.stop()
            raise MoveTimeout()

        elif self._move_fut.cancelled():
            self._move_fut = None
            exc = self._move_fut_exception
            self._move_fut_exception = None

            logger.warning("Move to {} (now {}) failed -- cancelled with {}"
                           .format(target, self.position, exc))
            if exc is None:
                exc = MoveCancelled
            raise exc()

        return True

    async def _compute_dijkstra(self, target):
        logger.info("DIJKSTRA compute")

        t0 = time.time()
        if self._simple_kstra is None:
            logger.warning("Waiting for dijkstra to be ready")
            await self._kstra_ready

        path = self._simple_kstra.compute_path_to(target, start=self.position)

        if path is not None:

            # Convert path to Vectors
            path = [x.pos for x in path]
            self._targets = optimize_path(self._pos, path, self._fz.obstacles)
            t1 = time.time()

            logger.info(
                "Path found: {}".format(print_path(self._targets))
                )

        else:
            self._targets = []
            t1 = time.time()
            logger.info("No path found")

        logger.info("in {:.1}s".format(t1 - t0))

        return path

    async def _keepalive(self):
        """Send periodically a keep alive to indicate the move board we want
        it to move"""

        await asyncio.sleep(1.5)

        while self._targets:
            logger.debug("Sending keep alive")
            try:
                await self.board.goto_keepalive()
            except asyncio.CancelledError:
                raise
            except Exception:
                logger.exception("GOTO keepalive failed")

            await asyncio.sleep(1.5)

    async def _target_reached(self, position, target):
        """Called when target is declared reached from move board"""
        if self._targets == []:
            logger.warning("Reached notif but no targets")
            return

        logger.info("Target {} reached! (pos {})"
                    .format(self._targets[0], self.position))

        self._targets.pop(0)
        tolerance = self._tolerance or self.DEFAULT_TOLERANCE

        if len(self._targets):
            if len(self._targets) > 1:
                tolerance = self.TRAVEL_TOLERANCE

            logger.debug("Step Move from {} to {} tol {}"
                         .format(self.position, self._targets[0],
                                 tolerance))

            # Request move from board
            try:
                self._board_goto_call = functools.partial(
                    self.board.goto_target,
                    self._targets[0],
                    handler=self._target_reached,
                    tolerance=tolerance,
                    )

                self._board_goto_call_time = time.time()
                await self._board_goto_call()

            except asyncio.CancelledError:
                raise
            except Exception:
                logger.exception("Move to next step failed, will it work?")

        else:
            if self._move_fut is not None:
                self._move_fut.set_result(None)

    async def stop(self, exception=None):
        """Stop all movement right now

        Arguments:
            exception: optional exception to raise from stopped process
        """

        self._targets = []
        try:
            await self.board.goto_stop()
        except asyncio.TimeoutError:
            pass

        try:
            if self._move_fut:
                self._move_fut_exception = exception
                self._move_fut.cancel()

            if self._ka_task is not None:
                self._ka_task.cancel()
                self._ka_task = None
        except Exception:
            logger.exception("Stop fail")

    async def line_to(self, target, vmax=0, vmax_th=0, dist=0, theta=0, num=0,
                      timeout=5, raises=False):
        """Perform a line move with given params"""

        tol = GotoTolerance(vmax, vmax_th, dist, theta, num)

        try:
            await self.move_to(
                target,
                mode="line",
                timeout=timeout,
                tolerance=tol,
                backward_allowed=True,
                rotate_first=True)

            return True

        except asyncio.CancelledError:
            raise
        except Exception as e:
            logger.exception("Exception while moving to {} with tolerance {}"
                             .format(target, tol))
            if raises:
                raise e

        return False

    async def _avoid(self):

        while True:
            await asyncio.sleep(0.3)

            if len(self._targets) == 0:
                # No move, nothing to avoid
                continue

            obs = self._fz.obstacles.first_obstacle_on_path(
                self.position,
                self._targets[0],
                static=False)  # Only robots, no static objects

            if obs is not None:
                self._fz.model.led_avoid()

                logger.warning(
                    "Obstacle {} on line to {}, stopping and waiting for move!"
                    .format(obs, self._targets[0]))

                # Stop board
                try:
                    self._board_goto_call_time = 0
                    await self.board.goto_stop()
                except asyncio.TimeoutError:
                    pass

                # Wait up to 10s for obstacle to move
                t = time.time()
                resumed = False
                while len(self._targets) and time.time() - t < 10:

                    obs = self._fz.obstacles.first_obstacle_on_path(
                        self.position,
                        self._targets[0],
                        static=False)  # Only robots, no static objects

                    if obs is None:
                        # All right, resume move
                        logger.warning(
                            "Path is clear, resuming move!!")
                        resumed = True
                        break

                    await asyncio.sleep(0.3)
                if resumed:
                    try:
                        await self._board_goto_call()
                        self._board_goto_call_time = time.time()

                    except asyncio.CancelledError:
                        raise
                    except Exception:
                        self._logger.exception("Failed resuming goto")
                else:
                    await self.stop(MoveAvoided)

    async def _burnout(self):
        """Called when move board indicates a burnout"""
        if len(self._targets) == 0:
            return

        logger.warning(
            "Burnout while going to {}, stopping!"
            .format(self._targets[0]))

        self._fz.model.led_burnout()

        await self.stop(MoveBurnouted)
