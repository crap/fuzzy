import aiohttp
import json
import time

from fuzzy import geo
from fuzzy.match import objects
from .conf import GotoTolerance
from .conf import GotoSpeedConfig, GotoGoalConfig

import logging
logger = logging.getLogger("MoveRest")


class MoveRest:
    def __init__(self):
        self._rest = self._fz.rest
        self._rest.register_get("/move", self._handle_get)
        self._rest.register_get("/move/encoders", self._handle_get_encoders)
        self._rest.register_put("/move/reset", self._handle_put_reset)
        self._rest.register_put("/move/to", self._handle_put_to)
        self._rest.register_get("/move/speedconfig",
                                self._handle_get_speed_config)
        self._rest.register_put("/move/speedconfig",
                                self._handle_put_speed_config)
        self._rest.register_put("/move/goalconfig",
                                self._handle_put_goal_config)
        self._rest.register_put("/move/stop", self._handle_put_stop)
        self._rest.register_put("/move/motors", self._handle_put_motor)
        self._rest.register_put("/move/raw", self._handle_put_raw)
        self._rest.register_put("/move/speed", self._handle_put_speed)
        self._rest.register_get("/move/log", self._handle_get_log)

        self._rest.register_put("/move/scan_locate",
                                self._handle_put_scan_locate)

        self._rest.register_websocket("/move/events",
                                      self._handle_ws_events)

        # List of connected websockets
        self._events_ws = []
        self._event_last_sent = time.time()

    async def _handle_get(self, req):
        dat = {
            "model": self._fz.model.__class__.__name__,
            "position": self._pos.to_json(),
            "targets": [x.to_json() for x in self._targets],
        }

        return dat

    async def _handle_get_encoders(self, req):
        return await self.board.get_encoders()

    async def _handle_get_speed_config(self, req):
        return self._goto_speed_config.to_json()

    async def _handle_put_speed_config(self, req):
        dat = await req.json()

        self._goto_speed_config = GotoSpeedConfig.from_json(dat)
        return await self.board.set_goto_speed_config(self._goto_speed_config)

    async def _handle_put_goal_config(self, req):
        dat = await req.json()

        cfg = GotoGoalConfig.from_json(dat)
        return await self.board.set_goto_goal_config(cfg)

    async def _handle_put_raw(self, req):
        dat = await req.json()
        pwm = geo.Vector.from_json(dat["pwm"], no_y=True)
        dur = dat.get("duration", 1)
        await self.board.goto_raw(pwm, dur, log=True)

    async def _handle_put_speed(self, req):
        dat = await req.json()
        speed = geo.Vector.from_json(dat["speed"], no_y=True)
        dur = dat.get("duration", 1)
        await self.board.goto_speed(speed, dur=dur, log=True)

    async def _handle_put_motor(self, req):
        dat = await req.json()
        await self.board.set_pwm(*dat["motors"])

    async def _handle_put_reset(self, req):
        dat = await req.json()

        await self.reset_position(geo.Vector.from_json(dat))
        return "OK"

    async def _handle_put_to(self, req):
        dat = await req.json()

        target = geo.Vector.from_json(dat["target"])
        mode = dat.get("mode", "dijkstra")
        simulate = dat.get("simulate", False)
        log = dat.get("log", False)
        keep = dat.get("keep", False)
        rotate_first = dat.get("rotate_first", False)
        backward_allowed = dat.get("backward_allowed", False)
        final_rotate = dat.get("final_rotate", True)
        timeout = dat.get("timeout", 5)

        tol = GotoTolerance.from_json(dat)

        await self.move_to(
            target,
            mode=mode,
            lock=False, simulate=simulate,
            timeout=timeout,
            log=log, keep=keep, tolerance=tol,
            backward_allowed=backward_allowed,
            final_rotate=final_rotate,
            rotate_first=rotate_first)

        return "OK"

    async def _handle_get_log(self, req):
        pts = self.board._debug_points[:]
        offset = pts[0][0]
        for pt in pts:
            pt[0] -= offset
            pt[0] /= 100

        return pts

    async def _handle_put_stop(self, req):
        await self.stop()
        return "OK"

    async def _handle_put_scan_locate(self, req):
        CLUST_SIZE_LIM = 0.15  # 15 cm

        clusters, scan = await self._fz.obst_scanner.wait_for_cluster()

        # only keep small clusters
        clusters = [c for c in clusters if c.width <= CLUST_SIZE_LIM]

        if self._fz.color.is_green():
            ref_objs = objects.loc_objects_green
        else:
            ref_objs = objects.loc_objects_orange

        # clusters in the model's referential
        cl_pos = [self._fz.model.get_hok_cluster_pos(cl) for cl in clusters]

        matches = []
        FACTOR = 0.05
        for c in cl_pos:
            for o in ref_objs:
                if (o-c).xy_norm <= (o-self.fz.move.position).xy_norm * FACTOR:
                    matches.append((c, o))

        if len(matches) < 3:
            return

        # TODO: do a magic computation

    async def _handle_ws_events(self, ws):
        if len(self._events_ws) == 0:
            self.register_event(self._event)

        self._events_ws.append(ws)
        while True:
            msg = await ws.receive()
            if msg.tp in (aiohttp.web.MsgType.closed,
                          aiohttp.web.MsgType.error):
                break

        print("WS closed: ", ws)
        self._events_ws.remove(ws)
        if len(self._events_ws) == 0:
            self.unregister_event(self._event)

    def _event(self, p):
        dt = time.time() - self._event_last_sent
        if dt < 0.3:
            return

        self._event_last_sent = time.time()

        dat = {
            "position": p.to_json(),
            "targets": [x.to_json() for x in self._targets],
            "robobstacles": [
                x.to_json() for x in self._fz.obstacles.get_robots()],
        }

        dat = json.dumps(dat)
        for ws in self._events_ws:
            try:
                ws.send_str(dat)
            except Exception:
                logger.exception("Removing websocket {}".format(ws))
                self._events_ws.remove(ws)
