import copy
import logging
import itertools

from fuzzy import geo

from .dijkstra import shortestPath

logger = logging.getLogger("Move")

extra_dbg = False


class Point:
    def __init__(self, pos, name):
        self.pos = pos
        self.name = name

    def __hash__(self):
        return hash((self.pos, self.name))

    def __repr__(self):
        return self.name

    def __lt__(self, other):
        return self.name < other.name

    def __eq__(self, other):
        return self.name == other.name

    def __le__(self, other):
        return self < other or self == other


pts_green = [
    Point(geo.Vector(0.4, 0.4), "StartZone"),
    Point(geo.Vector(0.84, 0.64), "Cross"),
    Point(geo.Vector(1.54, 0.64), "DistribMixed"),

    Point(geo.Vector(1.5, 1.2), "Station"),
    Point(geo.Vector(0.4, 1.3), "Switch"),
]

pts_orange = [
    Point(pt.pos.reversed(), pt.name + "'") for pt in pts_green
]
# All points
points = pts_green + pts_orange + [Point(geo.Vector(1, 1.5), "Center")]


class SimpleKstra:
    def __init__(self, obstacles):
        self.obstacles = obstacles

        self._base_graph = self._setup_graph()

    def _setup_graph(self):

        logger.info("Computing initial graph...")

        # Get all pairs of points from points
        combi = list(itertools.combinations(points, 2))

        # Make graph
        graph = {}

        for a, b in combi:
            graph[a] = graph.get(a, {})
            graph[b] = graph.get(b, {})

            obs = self.obstacles.first_obstacle_on_path(
                a.pos, b.pos, robots=False)
            if obs is not None:
                logger.debug("Obstacle between {} and {} : {}"
                             .format(a, b, obs))
                continue

            d = (b.pos - a.pos).xy_norm
            graph[a][b] = d
            graph[b][a] = d

        logger.info("Graph done: " + self._str_graph(graph))

        return graph

    def _update_graph(self, start, end):
        logger.info("Updating graph...")

        pt_start = Point(start, "Start")
        pt_end = Point(end, "End")

        # Make graph
        graph = copy.deepcopy(self._base_graph)

        # Remove obstructed paths
        remove_all = []
        for a, children in graph.items():

            for b in children:
                if (b, a) in remove_all:
                    continue

                obs = self.obstacles.first_obstacle_on_path(
                    a.pos, b.pos, static=False)
                if obs is None:
                    continue

                logger.warning(
                    "Obstacle between {} and {} : {}".format(a, b, obs))
                remove_all.append((a, b))

        for a, b in remove_all:
            del graph[a][b]
            del graph[b][a]

        # Add available start  & stops
        for a in (pt_start, pt_end):
            graph[a] = graph.get(a, {})

            # Search closest points
            pts = []

            for b in points:
                d = (b.pos - a.pos).xy_norm
                pts.append((d, b))

            pts.sort(key=lambda x: x[0])

            while len(pts) and len(graph[a]) < 3:
                d, b = pts.pop(0)

                obs = self.obstacles.first_obstacle_on_path(a.pos, b.pos)
                if obs is not None:
                    logger.debug(
                        "Obstacle between {} and {} : {}".format(a, b, obs))
                    continue

                graph[a][b] = d
                graph[b][a] = d
                break

        logger.info("Graph done: " + self._str_graph(graph))

        return graph, pt_start, pt_end

    def compute_path_to(self, target, start):
        """Compute smart path from start to given target"""

        if self.obstacles.first_obstacle_on_path(start, target) is None:
            return [Point(target, "End")]

        # Prepare graph
        graph, pt_start, pt_end = self._update_graph(start, target)

        return shortestPath(graph, pt_start, pt_end)

    def _str_graph(self, graph):
        _g = "\n"
        for a in graph:
            _g += "{}\n".format(a)
            for b in graph[a]:
                _g += "|->\t{}-{}\n".format(b, int(100*graph[a][b]))
        return _g
