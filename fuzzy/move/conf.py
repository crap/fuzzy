import struct
from fuzzy import geo


class OdoConfig:

    def __init__(self, freq=None, matrix=None, gyro_axis=0,
                 gyro_coeffs=None, speed_alpha=None):
        self.freq = freq
        self.matrix = matrix
        self.gyro_axis = gyro_axis
        self.gyro_coeffs = gyro_coeffs
        self.speed_alpha = speed_alpha

    def to_json(self):
        return {
            "freq": self.freq,
            "matrix": self.matrix,
            "gyro_axis": "XYZN"[self.gyro_axis],
            "gyro_coeffs": self.gyro_coeffs,
            "speed_alpha": self.speed_alpha
        }

    @classmethod
    def from_json(cls, dat):
        return cls(
            freq=dat["freq"],
            matrix=dat["matrix"],
            gyro_axis="XYZN".index(dat["gyro_axis"]),
            gyro_coeffs=dat["gyro_coeffs"],
            speed_alpha=dat["speed_alpha"]
            )

    def __eq__(self, other):
        if not isinstance(other, OdoConfig):
            return NotImplemented

        return self.__dict__ == other.__dict__

    def __repr__(self):
        return "OdoConfig(freq={}, matrix={}, gyro_axis={}, gyro_coeffs={}, " \
            "speed_alpha={})".format(self.freq, self.matrix, self.gyro_axis,
                                     self.gyro_coeffs, self.speed_alpha)

    @property
    def packed(self):
        return struct.pack(
            ">ffffffffffBffff",
            self.freq,
            *self.matrix[0],
            *self.matrix[1],
            *self.matrix[2],
            self.gyro_axis,
            *self.gyro_coeffs,
            self.speed_alpha
            )


class GotoSpeedConfig:

    def __init__(self, cmd_matrix=None, Kp=None, Ki=None, alpha=None,
                 amax=None):
        if cmd_matrix is None:
            cmd_matrix = [[0, 0], [0, 0]]
        self.cmd_matrix = cmd_matrix
        if Kp is None:
            Kp = geo.Vector()
        self.Kp = Kp
        if Ki is None:
            Ki = geo.Vector()
        self.Ki = Ki
        if alpha is None:
            alpha = geo.Vector()
        self.alpha = alpha
        if amax is None:
            amax = geo.Vector()
        self.amax = amax

    def to_json(self):
        return {
            "cmd_matrix": self.cmd_matrix,
            "Kp": self.Kp.to_json(no_y=True),
            "Ki": self.Ki.to_json(no_y=True),
            "alpha": self.alpha.to_json(no_y=True),
            "amax": self.amax.to_json(no_y=True),
        }

    @classmethod
    def from_json(cls, dat):
        return cls(
            cmd_matrix=dat["cmd_matrix"],
            Kp=geo.Vector.from_json(dat["Kp"], no_y=True),
            Ki=geo.Vector.from_json(dat["Ki"], no_y=True),
            alpha=geo.Vector.from_json(dat["alpha"], no_y=True),
            amax=geo.Vector.from_json(dat["amax"], no_y=True)
            )

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return "GotoSpeedConfig({})" % (self.__dict__)

    @property
    def packed(self):
        dat = b"".join(struct.pack('>ff', *l) for l in self.cmd_matrix)
        for vec in (self.Kp, self.Ki, self.alpha, self.amax):
            dat += struct.pack(">ff", vec.x, vec.theta)
        return dat


class GotoGoalConfig:

    def __init__(self, vmax=None, amax=None):
        if vmax is None:
            vmax = geo.Vector()
        self.vmax = vmax
        if amax is None:
            amax = geo.Vector()
        self.amax = amax

    def to_json(self):
        return {
            "vmax": self.vmax.to_json(no_y=True),
            "amax": self.amax.to_json(no_y=True)
        }

    @classmethod
    def from_json(cls, dat):
        return cls(
            vmax=geo.Vector.from_json(dat["vmax"], no_y=True),
            amax=geo.Vector.from_json(dat["amax"], no_y=True)
            )

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return "GotoGoalConfig({})".format(self.__dict__)

    @property
    def packed(self):
        dat = b""
        for vec in (self.vmax, self.amax):
            dat += struct.pack(">ff", vec.x, vec.theta)
        return dat


class GotoTolerance:
    def __init__(self, vmax=0, vmax_th=0, dist=0, theta=0, num=0):
        self.vmax = vmax
        self.vmax_th = vmax_th
        self.dist = dist
        self.theta = theta
        self.num = num

    @property
    def packed(self):
        return struct.pack(
            ">BBBBB",
            round(self.vmax / 0.01),
            round(self.vmax_th / 0.05),
            round(self.dist / 0.01),
            round(self.theta / 0.01),
            round(self.num)
            )

    @classmethod
    def from_json(cls, dat):
        tol = cls()
        tol.vmax = dat.get("vmax", 0)
        tol.vmax_th = dat.get("vmax_th", 0)
        tol.dist = dat.get("dist", 0)
        tol.theta = dat.get("theta", 0)
        tol.num = dat.get("num", 0)

    def __repr__(self):
        return "Tol(vmax={}, vmax_th={}, dist={}, theta={}, num={})"\
            .format(self.vmax, self.vmax_th, self.dist, self.theta, self.num)
