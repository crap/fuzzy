import asyncio
import logging
import math
import copy

from fuzzy import boards
from fuzzy.geo import Vector

from fuzzy.boards.motor import MotorPIDConfig

from fuzzy.acts.lift import Lift
from fuzzy.acts.pincer import Pincer

from fuzzy.obstacles.indus_scanner import IndusDigDevice, IndusScanner

from fuzzy.jobs.push_switch import SwitchPusher

from .model import Model


PID_MAX_VAL = 1000000
PID_RIGHT_UP = MotorPIDConfig(fixed_pwm=0, kp=10/PID_MAX_VAL, ki=0, kd=0)
PID_RIGHT_DOWN = copy.deepcopy(PID_RIGHT_UP)


class Big(Model):
    GREEN_START_POST = Vector(0.2, 0.25, math.pi / 2)

    def __init__(self, fz):
        self._logger = logging.getLogger("BIG")
        self._logger.info("BIG creating...")

        move_board = fz.boards.add_board(2, boards.board.Type.move)
        pow_board = fz.boards.add_board(11, boards.board.Type.pow)

        Model.__init__(self, fz, move_board, pow_board)

        self.servos_5v = self._fz.boards.add_board(8, boards.board.Type.servos)
        self.servos_7v = self._fz.boards.add_board(3, boards.board.Type.servos)

        self.futabas = self._fz.boards.add_board(7, boards.board.Type.futabas)
        self.pumps = self._fz.boards.add_board(10, boards.board.Type.pumps)

        self.motor1 = self._fz.boards.add_board(5, boards.board.Type.motor)
        self.motor2 = self._fz.boards.add_board(13, boards.board.Type.motor)

        self.indus1 = self._fz.boards.add_board(4, boards.board.Type.indus)

        # Configure motor servo
        self.motor1.config_servo(PID_MAX_VAL, 0.01 * PID_MAX_VAL)
        self.motor2.config_servo(PID_MAX_VAL, 0.01 * PID_MAX_VAL)

        # Scanner
        front_left = IndusDigDevice(
            "front_left",
            Vector(0.1, 0.15, 0),
            0.1,
            self.indus1,
            0
        )
        front_right = IndusDigDevice(
            "front_right",
            Vector(0.1, -0.15, 0),
            0.1,
            self.indus1,
            1
        )
        self.indus_scanner = IndusScanner(
            self._fz,
            [front_left, front_right])

        # Actions
        self.lift_left = Lift(
            self._fz.actions,
            "left",
            liftmotor=self.motor1,
            suckerservo=self.futabas.servo(1),
            sucker=self.pumps.pump(0)
            )
        self.lift_right = Lift(
            self._fz.actions,
            "right",
            liftmotor=self.motor2,
            suckerservo=self.futabas.servo(2),
            sucker=self.pumps.pump(1)
            )

        self.pincer = Pincer(
            self._fz.actions,
            servo_left=self.servos_7v.servo(0),
            servo_right=self.servos_7v.servo(1),
            servo_back_left=self.futabas.servo(3),
            servo_back_right=self.futabas.servo(4)
        )

        # Jobs !!!!!!!
        sp = SwitchPusher(self._fz)

        # Add them all
        self._fz.match.add_job(sp)

    async def start(self):
        """setup start, not match start"""
        await Model.start(self)

        # Configure lift with PID settings
        while True:
            try:
                await self.motor1.set_pid_config("up", PID_RIGHT_UP)
                await self.motor1.set_pid_config("down", PID_RIGHT_DOWN)
                break
            except Exception:
                self._logger.warning("Failed to configure PID, retrying soon")
                await asyncio.sleep(4)

    async def click_midd(self, on):
        if await Model.click_midd(self, on):
            return

        self._logger.debug("Can do what we want on MIDDLE")
        await self.lift_left.do_fold()
        await self.lift_right.do_fold()
        await self.pincer.do_fold()

    async def click_left(self, on):
        if await Model.click_left(self, on):
            return

        self._logger.debug("Can do what we want on LEFT")

    async def click_right(self, on):
        if await Model.click_right(self, on):
            return

        self._logger.debug("Can do what we want on RIGHT")

    async def jack(self, on):
        if await Model.jack(self, on):
            return

        self._logger.debug("Can do what we want on jack insert")
