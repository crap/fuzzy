import logging

from fuzzy import boards
from fuzzy.geo import Vector

from fuzzy.acts.canonball import CanonBall

from .model import Model

logger = logging.getLogger("LITTLE")


class Little(Model):
    GREEN_START_POST = Vector(0.5, 0.15, 0)

    def __init__(self, fz):
        logger.info("LITTLE creating...")

        move_board = fz.boards.add_board(2, boards.board.Type.move)
        pow_board = fz.boards.add_board(11, boards.board.Type.pow)

        self.servos_5v = fz.boards.add_board(8, boards.board.Type.servos)

        Model.__init__(self, fz, move_board, pow_board)

        # Actions
        self.canonball = CanonBall(
            fz.actions,
            rotservo_dir=self.servos_5v.servo(4),
            rotservo_pwm=self.servos_5v.servo(5),
            hatchservo=self.servos_5v.servo(1),
            guideservo=self.servos_5v.servo(2),
            ejectservo=self.servos_5v.servo(0),
            bumpservoboard=self.servos_5v,
            bump_ix=0
            )

        self._mode_left = "eject"
        self._mode_right = "eject"

        # Jobs !!!!!!!

    async def start(self):
        await Model.start(self)

        # Put actuators idle
        await self.canonball.do_fold()

    async def click_midd(self, on):
        if await Model.click_midd(self, on):
            return

        logger.debug("MIDDLE fold everything")
        await self.canonball.do_fold()

    async def click_left(self, on):
        if await Model.click_left(self, on):
            return

        logger.debug("Left => eject left")
        if self._mode_left == "eject":
            self._mode_left = "slide"
            await self.canonball.do_eject("left")
        else:
            self._mode_left = "eject"
            await self.canonball.do_slide_half("left")

    async def click_right(self, on):
        if await Model.click_right(self, on):
            return

        logger.debug("Right => eject right")
        if self._mode_right == "eject":
            self._mode_right = "slide"
            await self.canonball.do_eject("right")
        else:
            self._mode_right = "eject"
            await self.canonball.do_slide_half("right")

    async def jack(self, on):
        if await Model.jack(self, on):
            return

        logger.debug("Can do what we want on jack insert")
