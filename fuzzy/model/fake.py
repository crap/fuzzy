import logging
import math

from fuzzy.geo import Vector
from fuzzy.color import Color
from fuzzy import boards

logger = logging.getLogger("FAKE")


class Fake:

    def __init__(self, fz):
        self.fz = fz
        self.move_board = self.fz.boards.add_board(
            2, boards.board.Type.fakemove)

    def get_start_pos(self, color):
        green_pos = Vector(0.2035, 0.893, math.pi)
        if color.is_orange():
            return green_pos.reversed(mode="oppo")
        else:
            return green_pos

    async def start(self):
        logger.debug("Fetching color...")
        self.fz.set_color(Color.green)

    async def calibrate_start(self):
        pass
