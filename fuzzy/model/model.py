import time
import sys
import asyncio
import logging

from fuzzy import boards
from fuzzy.color import Color


class Model:
    def __init__(self, fz, move_board, pow_board):
        self._logger = logging.getLogger("Model")
        self._fz = fz

        self.move_board = move_board
        self.pow_board = pow_board
        self._led_color = boards.LEDColor.off()

        # Clicks handlers

        self._mid_clicked = False
        self._left_clicked = False
        self._right_clicked = False

        self._mid_start = 0
        self._mid_start_blink_task = None
        self._mid_count = 0

        self.pow_board.add_btn_handler(2, self.click_left)
        self.pow_board.add_btn_handler(1, self.click_midd)
        self.pow_board.add_btn_handler(0, self.click_right)

        self.pow_board.add_jack_handler(self.jack)

        # Match
        self._fz.match.add_stopped_handler(self.match_stopped)

    def get_start_pos(self, color):
        if color.is_orange():
            return self.GREEN_START_POST.reversed(mode="oppo")
        else:
            return self.GREEN_START_POST

    async def start(self):
        """setup start, not match start"""
        self._logger.debug("Fetching color...")
        try:
            col = await self.pow_board.get_color()
            self._fz.set_color(col)

            if col.is_green():
                self._led_color = boards.LEDColor.green()
            else:
                self._led_color = boards.LEDColor.orange()

        except asyncio.CancelledError:
            raise
        except Exception:
            self._logger.exception("Failed to get color")

            # Force green XXX
            self._fz.set_color(Color.green)

        self.led_chase()

    async def click_midd(self, on):
        self._mid_clicked = on

        # Auto reset
        now = time.time()
        if on:
            self._mid_start = now

            async def wait_and_blink():
                await asyncio.sleep(3)
                await self.pow_board.leds_blink(
                    boards.LEDColor.green(), 0.3,
                    boards.LEDColor.red(), 0.3)

            self._mid_start_blink_task = \
                asyncio.ensure_future(wait_and_blink())

        else:
            if now - self._mid_start > 3:
                self._logger.warning("RESETTING")
                sys.exit(1)

            self._mid_start_blink_task.cancel()
            self._mid_start_blink_task = None

        # Let descendent work on falling edge only
        if on:
            return True
        return False

    async def click_left(self, on):
        self._left_clicked = on

        # Let descendent work on falling edge only
        if on:
            return True
        return False

    async def click_right(self, on):
        self._right_clicked = on

        # Let descendent work on falling edge only
        if on:
            return True
        return False

    async def jack(self, on):
        self._logger.info("JACK {}".format(on))

        if not on:
            self._fz.match.start()
            return True

        # Let descendent work on falling jack insert only
        return False

    def led_chase(self):
        """Run the standard chase"""
        asyncio.ensure_future(
            self.pow_board.leds_chaser(self._led_color)
            )

    def led_start(self):
        asyncio.ensure_future(
            self.blink_then_restore(boards.LEDColor.white())
            )

    def led_avoid(self):
        asyncio.ensure_future(
            self.blink_then_restore(boards.LEDColor.red())
            )

    def led_burnout(self):
        asyncio.ensure_future(
            self.blink_then_restore(boards.LEDColor.yellow())
            )

    def led_timeout(self):
        asyncio.ensure_future(
            self.blink_then_restore(boards.LEDColor.blue())
            )

    def led_happy(self):
        asyncio.ensure_future(
            self.blink_then_restore(self._led_color)
            )

    async def blink_then_restore(self, color):
        try:
            await self.pow_board.leds_blink(
                color, 0.05, boards.LEDColor.off(), 0.05)
            await asyncio.sleep(0.5)
            await self.pow_board.leds_chaser(self._led_color)
        except asyncio.CancelledError:
            raise
        except Exception:
            self._logger.exception("blink and restore failed")

    async def match_stopped(self):
        self._logger.info("MATCH STOPPED!")

        for b in self._fz.boards.boards:
            try:
                await b.power(False)
            except asyncio.CancelledError:
                raise
            except Exception:
                self._logger.exception(
                    "Powering off board {} failed".format(b))

        self._logger.info("Final Exit")
        sys.exit(0)
