import asyncio
import collections
import logging
import copy

from fuzzy.devices import ServoPosition

logger = logging.getLogger("Actions")


class ActionSlider:
    def __init__(self, name, steps, servos, conf, custom=None):
        self.name = name
        self.steps = steps
        self.current = self.steps[0]

        self.servos = servos
        self._conf = None
        if conf is not None:
            self._conf = conf.get(name, collections.OrderedDict({}))
        self._custom = custom

        if self.servos is not None:
            # Setup full scale time
            for servo, conf in self._conf.get("_servos_", {}).items():
                self.servos[servo].full_scale_time = conf["full_scale_time"]

    def json(self):
        """Return a JSON representation of the slider"""
        servos = []
        if self.servos is not None:
            servos = list(self.servos.keys())

        return {
            "steps": self.steps,
            "current": self.current,
            "servos": servos
        }

    @property
    def positions(self):
        servos = []
        if self.servos is not None:
            servos = list(self.servos.keys())

        return {
            step: {
                servo: self._conf.get(step, {}).get(servo, ServoPosition(0))
                for servo in servos
            } for step in self.steps
        }

    async def set(self, position, slow_factor=1, delays={}, wait_before=0,
                  wait_after=0):
        """
        :param delays: specific delay before moving individual servos
        :param wait_before: explicit wait before move
        :param wait_after: explicit wait after move before returning
        """
        coro = []
        for s, p in self.positions[position].items():

            async def one(s, p):
                if s in delays:
                    await asyncio.sleep(delays[s])
                await self.servos[s].set(p, slow_factor=slow_factor)

            coro.append(one(s, p))

        if self._custom is not None and position in self._custom:
            coro.append(self._custom[position]())

        self.current = position
        await asyncio.sleep(wait_before)
        await asyncio.gather(
            *coro,
            return_exceptions=True
            )
        await asyncio.sleep(wait_after)

    async def test(self, values):
        coro = []
        for s, p in values.items():
            p = ServoPosition.from_json(p)
            coro.append(self.servos[s].set(p, slow_factor=0))

        await asyncio.gather(
            *coro,
            return_exceptions=True
            )


class Action:

    def __init__(self, manager, conf_path):
        self.manager = manager
        self._conf_path = conf_path

        self.sliders = collections.OrderedDict()
        self.clicks = collections.OrderedDict()

        self._load_conf()

        # Auto add all click handlers
        for attr in dir(self):
            if attr.startswith('do_'):
                f = getattr(self, attr)
                if callable(f):
                    self.add_clicks(getattr(self, attr))

    @property
    def name(self):
        return self.__class__.__name__

    def add_slider(self, slider):
        self.sliders[slider.name] = slider

    def add_clicks(self, *clicks):
        for click in clicks:
            self.clicks[click.__name__] = click

    def _load_conf(self):

        # Load conf
        conf = self.manager.fz.config.get(self._conf_path)

        # Convert conf
        for slider in conf:
            for position in conf[slider]:
                if position == '_servos_':
                    continue

                for servo, spos in conf[slider][position].items():
                    conf[slider][position][servo] = \
                        ServoPosition.from_json(spos)
        self._conf = conf

    def update_slider_conf(self, slider, pos, dat):
        if pos not in self._conf[slider]:
            self._conf[slider][pos] = {}

        for servo in dat:
            self._conf[slider][pos][servo] = \
                ServoPosition.from_json(dat[servo])

        # Copy conf and ensure json
        conf = copy.deepcopy(self._conf)
        for name, slider in conf.items():
            if name == '_servos_':
                continue
            for position in slider.values():
                for servo, spos in position.items():
                    if isinstance(spos, ServoPosition):
                        position[servo] = spos.json()

        self.manager.fz.config.save(self._conf_path, conf)

    def json(self):
        dat = {
            "sliders": collections.OrderedDict(),
            "clicks": collections.OrderedDict(),
        }

        for k, v in self.sliders.items():
            dat["sliders"][k] = v.json()

        for k, v in self.clicks.items():
            dat["clicks"][k] = {
                "help": v.__doc__
            }

        return dat


class Manager:
    def __init__(self, fz):
        self.fz = fz
        self._actions = collections.OrderedDict()

        self.fz.rest.register_get("/actions", self._get)
        self.fz.rest.register_put(
            "/actions/{action}/sliders/{slider}/{pos}",
            self._put_slider)

        self.fz.rest.register_get(
            "/actions/{action}/sliders/{slider}/{pos}/edit",
            self._get_slider_edit)
        self.fz.rest.register_put(
            "/actions/{action}/sliders/{slider}/{pos}/test",
            self._put_slider_test)
        self.fz.rest.register_put(
            "/actions/{action}/sliders/{slider}/{pos}/save",
            self._put_slider_save)

        self.fz.rest.register_put(
            "/actions/{action}/clicks/{click}",
            self._put_click)

    def add_action(self, action):
        name = action.name
        self._actions[name] = action
        logger.info("New Action available: {}"
                    .format(name))

        logger.debug(list(self._actions.keys()))

    async def _get(self, req):
        dat = collections.OrderedDict()
        for k, v in self._actions.items():
            dat[k] = v.json()
        return dat

    async def _put_slider(self, req):
        action = req.match_info["action"]
        slider = req.match_info["slider"]
        pos = req.match_info["pos"]

        logger.debug("PUT slider {} of {} to {}".format(slider, action, pos))
        await self._actions[action].sliders[slider].set(pos)

    async def _get_slider_edit(self, req):
        action = req.match_info["action"]
        slider = req.match_info["slider"]
        pos = req.match_info["pos"]

        logger.debug("GET slider edit {} of {} to {}"
                     .format(slider, action, pos))
        dat = self._actions[action].sliders[slider].positions[pos]
        dat = {servo: value.json() for servo, value in dat.items()}

        logger.info("Slider Edit pos {} ({})".format(dat, repr(dat)))
        return dat

    async def _put_slider_test(self, req):
        action = req.match_info["action"]
        slider = req.match_info["slider"]
        pos = req.match_info["pos"]

        dat = await req.json()
        logger.debug("PUT slider test {} of {} to {} dat: {}"
                     .format(slider, action, pos, dat))

        await self._actions[action].sliders[slider].test(dat)

    async def _put_slider_save(self, req):
        action = req.match_info["action"]
        slider = req.match_info["slider"]
        pos = req.match_info["pos"]

        dat = await req.json()
        logger.debug("PUT slider save {} of {} to {} dat: {}"
                     .format(slider, action, pos, dat))

        await self._actions[action].sliders[slider].test(dat)

        # Save conf
        self._actions[action].update_slider_conf(slider, pos, dat)

    async def _put_click(self, req):
        action = req.match_info["action"]
        click = req.match_info["click"]
        logger.debug("PUT click {} of {}".format(click, action))
        await self._actions[action].clicks[click]()
