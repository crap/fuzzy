import time
import asyncio
import functools
import logging

from fuzzy import geo
from fuzzy.acts.manager import Action, ActionSlider


class CanonBall(Action):
    PICK_LOC = {
        "right": geo.Vector(0, -0.7, 0),
        "left": geo.Vector(0, 0.7, 0),
    }
    OTHER_SIDE = {
        "right": "left",
        "right-strong": "left-strong",
        "left-strong": "right-strong",
        "left": "right"
    }

    def __init__(self, manager,
                 rotservo_dir, rotservo_pwm,
                 hatchservo, guideservo,
                 ejectservo,
                 bumpservoboard, bump_ix):
        Action.__init__(self, manager, "canonball.conf")

        self._name = "CanonBall"
        self._logger = logging.getLogger(self._name)
        self._logger.debug("Creating...")

        self.rotservo_dir = rotservo_dir
        self.rotservo_pwm = rotservo_pwm
        self.hatchservo = hatchservo
        self.guideservo = guideservo
        self.ejectservo = ejectservo
        self.bumpservoboard = bumpservoboard
        self.bump_ix = bump_ix

        self._inball_counter = 0

        self.rot = ActionSlider(
            "rot",
            steps=["left-strong", "left", "idle", "right", "right-strong"],
            servos=None,
            conf=None,
            custom={
                "idle": functools.partial(self._patate, 0),
                "right": functools.partial(self._patate, 0.5),
                "right-strong": functools.partial(self._patate, 1),
                "left": functools.partial(self._patate, -0.5),
                "left-strong": functools.partial(self._patate, -1)
            })

        self.add_slider(self.rot)

        self.hatch = ActionSlider(
            "hatch",
            steps=["closed", "open", "eject"],
            servos={"hatch": self.hatchservo},
            conf=self._conf
            )
        self.add_slider(self.hatch)

        self.guide = ActionSlider(
            "guide",
            steps=["closed", "middle", "open"],
            servos={"guide": self.guideservo},
            conf=self._conf
            )
        self.add_slider(self.guide)

        self.eject = ActionSlider(
            "eject",
            steps=["idle", "slow", "fast", "max"],
            servos={"eject": self.ejectservo},
            conf=self._conf
            )
        self.add_slider(self.eject)

        manager.add_action(self)

    @property
    def inball_counter(self):
        return self._inball_counter

    async def do_fold(self):
        await self.eject.set("idle")
        await self.do_close()
        await self.rot.set("idle")

    async def do_close(self, hatch_only=False):
        await self.guide.set("middle")
        await self.hatch.set("closed")
        await self.guide.set("closed")

    async def do_open_for_side(self, hatch=True):
        await self.guide.set("middle")
        await self.guide.set("open")
        if hatch:
            await self.hatch.set("open")

    async def do_open_for_eject(self):
        await self.guide.set("middle")
        await asyncio.gather(
            self.guide.set("middle"),
            self.hatch.set("eject"))

    async def _patate(self, cmd):
        d = int(cmd >= 0)
        pwm = 1 - min(1, abs(cmd))

        self._logger.debug("patate dir {} pwm {}".format(d, pwm))
        await self.rotservo_dir._force(d)
        await self.rotservo_pwm._force(pwm)

    async def rotate_one(self, side, timeout=2, move_back_on_fail=True):
        """Perform a single rotation"""
        backside = self.OTHER_SIDE[side]

        success = True
        await self.rot.set(side)

        fut = asyncio.Future()
        t0 = time.time()

        async def bumped(ix):
            if ix == self.bump_ix:
                if time.time() - t0 < 0.2:
                    self._logger.debug("skip")
                    return

                if not fut.done():
                    fut.set_result(None)

        with self.bumpservoboard.bump_handler(bumped):
            try:
                await asyncio.wait_for(fut, timeout=timeout)
            except asyncio.TimeoutError:
                self._logger.warning("Stopping, no turn in {} seconds"
                                     .format(timeout))
                success = False

        if not success and move_back_on_fail:
            self._logger.debug("Moving back because it failed")
            await self.rotate_one(backside, timeout,
                                  move_back_on_fail=False)

        await self.rot.set("idle")
        return success

    async def do_rotate_one_left(self):
        await self.rotate_one("left")

    async def do_rotate_one_left_strong(self):
        await self.rotate_one("left-strong")

    async def do_rotate_one_right(self):
        await self.rotate_one("right")

    async def do_rotate_one_right_strong(self):
        await self.rotate_one("right-strong")

    async def rotate_n(self, side, strong=False, n=8, max_failures=3,
                       handler=None):
        if strong:
            side += "-strong"

        count = 0
        nfail = 0
        for _ in range(int(n + max_failures)):
            self._logger.debug("Rotate {}/{} strong {}"
                               .format(count + 1, n, strong))
            success = await self.rotate_one(side)
            await asyncio.sleep(0.2)

            count += success
            nfail += (1 - success)

            if count == n or nfail == max_failures:
                break

            if success and handler:
                await handler(count)

        return count

    async def do_eject(self, side="right", speed="slow"):
        await self.eject.set(speed)
        await asyncio.sleep(2)

        await self.do_open_for_eject()

        # Rotate 10 to eject 8
        count = await self.rotate_n(side, strong=True, n=10)
        count = max(0, count - 2)

        await asyncio.sleep(2)

        await asyncio.gather(
            self.do_close(),
            self.eject.set("idle")
        )

        # Assume all ejected
        if not count:
            self._inball_counter = 0

        return count

    async def do_slide_half(self, side="right"):
        await self.do_open_for_side(hatch=False)

        self._other_color_count = 0

        async def step_handler(count):

            if count >= 2 and count % 2 == 1:
                self._logger.debug("count {} open!".format(count))
                # on hatch is not our color
                await self.hatch.set("open")
                await asyncio.sleep(1)
                await self.hatch.set("closed")
            else:
                self._logger.debug("count {} not open!".format(count))
                self._other_color_count += 1

        await self.rotate_n(side, n=10, strong=True,
                            handler=step_handler)

        # Let all roll
        await asyncio.sleep(2)

        # Close all
        await self.do_close()

        return self._other_color_count
