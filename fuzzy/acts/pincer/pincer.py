import asyncio
import logging

from fuzzy.acts.manager import Action, ActionSlider


class Pincer(Action):
    def __init__(self, manager, servo_left, servo_right, servo_back_left,
                 servo_back_right):
        Action.__init__(self, manager, "pincer.conf")

        self._name = "Pincer"
        self._logger = logging.getLogger(self._name)
        self._logger.debug("Creating...")

        self.servo_left = servo_left
        self.servo_right = servo_right
        self.servo_back_left = servo_back_left
        self.servo_back_right = servo_back_right

        self._logger.debug("CONF %r", self._conf)

        self.front = ActionSlider(
            "front",
            steps=["folded", "open", "push", "closed"],
            servos={"left": self.servo_left, "right": self.servo_right},
            conf=self._conf
            )
        self.add_slider(self.front)

        self.back = ActionSlider(
            "back",
            steps=["folded", "open", "push", "closed"],
            servos={"left": self.servo_back_left,
                    "right": self.servo_back_right},
            conf=self._conf
            )
        self.add_slider(self.back)

        manager.add_action(self)

    @property
    def name(self):
        return self._name

    async def do_fold(self):
        await asyncio.gather(
            self.back.set("folded"),
            self.front.set("folded")
        )

    async def do_open_for_insert(self):
        await asyncio.gather(
            self.back.set("push"),
            self.front.set("open")
        )

    async def do_push_in(self):
        await self.front.set("closed")
        await asyncio.sleep(0.5)
        await self.front.set("open")

    async def do_open_for_middle(self):
        await asyncio.gather(
            self.back.set("folded"),
            self.front.set("open")
        )
