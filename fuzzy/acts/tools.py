import asyncio
from fuzzy.geo import Vector


class MultiCM:
    def __init__(self, *cms):
        self.cms = cms

    async def __aenter__(self):
        await asyncio.wait([cm.__aenter__() for cm in self.cms])

    async def __aexit__(self, *args, **kwargs):
        await asyncio.wait([cm.__aexit__() for cm in self.cms])


def actions_compute_positions(target, p_act, d_start=0, d_stop=0, robot=None):
    """Compute start & stop position to grab an object

    Arguments:
        target: location of the object to grab on the table
        p_act: location of the actuator in the robot frame
        d_start: distance to the target to get
        d_stop: distance passed the targed to reach
        robot: current position of the robot, or None if target angle is to be
            used
    Returns:
        p_start: start position to grab
        p_end: end position to grab
    """
    attack_angle = target.theta

    if robot is not None:
        attack_angle = (target - robot).xy_angle

    # P_rob: robot location with target grabbed
    p_rob = target - Vector(p_act.xy_norm, 0, 0) \
        .rotated(attack_angle - p_act.theta + p_act.xy_angle)
    p_rob.theta = attack_angle - p_act.theta
    p_rob.mod2pi()

    p_start = p_rob - Vector(d_start, 0, 0).rotated(attack_angle)
    p_stop = p_rob + Vector(d_stop, 0, 0).rotated(attack_angle)

    return p_start, p_stop
