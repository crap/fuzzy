import asyncio
import logging

from fuzzy.acts.manager import Action, ActionSlider


class Lift(Action):
    def __init__(self, manager, name, liftmotor, suckerservo, sucker):
        Action.__init__(self, manager, "lift_" + name + ".conf")

        self._name = "Lift-" + name
        self._logger = logging.getLogger(self._name)
        self._logger.debug("Creating...")

        self.liftmotor = liftmotor
        self.suckerservo = suckerservo
        self.sucker = sucker

        self._logger.debug("CONF %r", self._conf)

        self.lift = ActionSlider(
            "lift",
            steps=["floor", "lvl1", "lvl2", "lvl3"],
            servos={"motor": self.liftmotor.servo_interface},
            conf=self._conf
            )
        self.add_slider(self.lift)

        self.arm = ActionSlider(
            "arm",
            steps=["middle", "cube1", "cube2", "folded", "storage"],
            servos={"servo": self.suckerservo},
            conf=self._conf
            )
        self.add_slider(self.arm)

        self.s = ActionSlider(
            "sucker",
            steps=["release", "suck"],
            servos=None,
            conf=None,
            custom={"release": self.sucker.release, "suck": self.sucker.suck}
            )
        self.add_slider(self.s)

        manager.add_action(self)

    @property
    def name(self):
        return self._name

    async def do_init(self):
        await self.arm.set("folded")
        await asyncio.sleep(1)
        await self.liftboard.bump_to_eoc(pwm=-20, reset_on_bump=True)

    async def do_fold(self):
        await self.arm.set("folded")
        await asyncio.sleep(1)
        await self.lift.set("lvl1")
