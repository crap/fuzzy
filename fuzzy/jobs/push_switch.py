import logging
import math

from fuzzy.match import Job, objects
from fuzzy.acts import actions_compute_positions
from fuzzy import geo


class SwitchPusher(Job):
    INITIAL_POINTS = 5
    MARKED_POINTS = 25
    PUSH_POS = geo.Vector(-0.1, 0, math.pi)

    def __init__(self, fz):
        super().__init__("SwitchPusher")

        self._logger = logging.getLogger("SwitchPusher")

        self._fz = fz

        self._p_start, self._p_stop = None, None
        self._switch = None

        # Reset all values
        self.reset()

        self._points_marked = self.INITIAL_POINTS

    def update_color(self, color):
        self._switch = objects[color].switch
        self._logger.info(" color -> {}, switch {}"
                          .format(color, self._switch))

    def start_position(self):
        """Compute the start position to do this job"""

        d_start = 0.2
        d_stop = 0.05

        self._p_start, self._p_stop = actions_compute_positions(
            self._switch,
            self.PUSH_POS,
            d_start,
            d_stop)

        self._logger.debug(
            "To '{}' start {} stop {}".format(
                self._switch,
                self._p_start,
                self._p_stop
                ))
        return self._p_start

    def doable(self):
        """Get the doability of the job"""
        return True

    async def execute(self):
        """Execute the job process

        The coroutine should return True of False wheter the job has
        succeeded or not.
        """
        self._logger.info("Executing on '{}'".format(self._switch))

        await self._fz.move.line_to(self._p_stop, vmax=0.2)
        self._fz.model.led_happy()
        await self._fz.move.line_to(self._p_start)

        self._points_marked += self.MARKED_POINTS
        self._logger.info("Done on '{}'".format(self._switch))

        return True

    def reset(self):
        """Reset to start state, cancel any pending action"""
        self._p_start, self._p_stop = None, None
        self._switch = None
