import logging

from fuzzy.match import Job, objects
from fuzzy.acts import actions_compute_positions
from fuzzy.color import Color


class EasyDistrib(Job):
    POINTS_FOR_DISTRIB = 10
    POINTS_PER_BALL_IN_TANK = 5

    def __init__(self, fz, canball):
        super().__init__("EasyDistrib")

        self._logger = logging.getLogger("EasyDistrib")

        self._fz = fz
        self._canball = canball

        self._p_start, self._p_stop = None, None
        self._distrib = None
        self._side = None

        # Reset all values
        self.reset()

    def update_color(self, color):
        self._distrib = objects[color].distrib

        self._side = {
            Color.green: "right",
            Color.orange: "left"
        }[color]

        self._logger.info(" color -> {}, distrib {}, side {}"
                          .format(color, self._distrib, self._side))

    def start_position(self):
        """Compute the start position to do this job"""

        d_start = 0.1
        d_stop = 0.05

        self._p_start, self._p_stop = actions_compute_positions(
            self._distrib,
            self._canball.PICK_LOC[self._side],
            d_start,
            d_stop)

        self._logger.debug(
            "To '{}' start {} stop {}".format(
                self._distrib,
                self._p_start,
                self._p_stop
                ))
        return self._p_start

    def doable(self):
        """Get the doability of the job"""
        return True

    async def execute(self):
        """Execute the job process

        The coroutine should return True of False wheter the job has
        succeeded or not.
        """
        self._logger.info("Executing on '{}'".format(self._distrib))

        await self._fz.move.line_to(self._p_stop, vmax=0.2)

        result = await self.canball.do_eject()

        self._fz.model.led_happy()

        await self._fz.move.line_to(self._p_start, vmax=0.2)

        # Assume all went well
        self._points_marked = self.POINTS_FOR_DISTRIB + \
            result * self.POINTS_PER_BALL_IN_TANK
        self._logger.info(
            "Done on '{}', result {}"
            .format(self._distrib, result))

        return True

    def reset(self):
        """Reset to start state, cancel any pending action"""

        self._p_start, self._p_stop = None, None
        self._distrib = None
        self._side = None
