import logging

from fuzzy.match import Job, objects
from fuzzy.acts import actions_compute_positions
from fuzzy import geo
from fuzzy.color import Color


class Butinater(Job):
    INITIAL_POINTS = 5
    MARKED_POINTS = 50

    def __init__(self, fz):
        super().__init__("Butinater")

        self._logger = logging.getLogger("Butinater")

        self._fz = fz
        self._push_pos = None

        self._p_start, self._p_stop = None, None
        self._bee = None
        self._act = None

        # Reset all values
        self.reset()

        self._points_marked = self.INITIAL_POINTS

    def update_color(self, color):
        self._bee = objects[color].bee_start
        self._act = {
            Color.green: geo.Vector(0, -0.2, 0),
            Color.orange: geo.Vector(0, 0.2, 0)
        }[color]

        self._logger.info(" color -> {}, bee {}, act {}"
                          .format(color, self._bee, self._act))

    def start_position(self):
        """Compute the start position to do this job"""

        d_start = 0.1
        d_stop = 0.2

        self._p_start, self._p_stop = actions_compute_positions(
            self._bee,
            self._act,
            d_start,
            d_stop)

        self._logger.debug(
            "To '{}' start {} stop {}".format(
                self._bee,
                self._p_start,
                self._p_stop
                ))
        return self._p_start

    def doable(self):
        """Get the doability of the job"""
        return True

    async def execute(self):
        """Execute the job process

        The coroutine should return True of False wheter the job has
        succeeded or not.
        """
        self._logger.info("Executing on '{}'".format(self._bee))

        # TODO pull arm out

        await self._fz.move.line_to(self._p_stop, vmax=0.2)
        self._fz.model.led_happy()

        # TODO pull arm in

        self._points_marked += self.MARKED_POINTS
        self._logger.info("Done on '{}'".format(self._bee))

        return True

    def reset(self):
        """Reset to start state, cancel any pending action"""
        self._p_start, self._p_stop = None, None
        self._bee = None
