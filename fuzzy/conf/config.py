import json
import collections
import os.path


class Config:

    def __init__(self, folder):
        self.folder = folder

    def get(self, path):
        fullpath = os.path.join(self.folder, path)
        try:
            with open(fullpath, 'r') as f:
                dat = json.load(f, object_pairs_hook=collections.OrderedDict)
            return dat
        except FileNotFoundError as e:
            parent = os.path.dirname(fullpath)
            if not os.path.exists(parent):
                raise e
            # create empty conf file
            return collections.defaultdict(collections.OrderedDict)

    def save(self, path, data):
        fullpath = os.path.join(self.folder, path)
        with open(fullpath, 'w') as f:
            json.dump(data, f, indent=4, separators=(',', ': '))
