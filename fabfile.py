from fabric.api import env, local, get, put, run, sudo

env.skip_bad_hosts = True
env.user = "crap"
env.password = 'crapo'

if env.hosts == []:
    env.hosts.append('192.168.7.2')


def pkg():
    out = ""
    with open('setup.py', 'r') as f:
        for l in f.readlines():
            ls = l.split('=')
            if ls[0].strip() == 'version':
                version = ls[1].strip().strip(',').strip("''")
                print("Version", version)
                versions = version.split('.')
                versions[-1] = str(int(versions[-1]) + 1)
                new_version = '.'.join(versions)
                print("New Version", new_version)
                l = l.replace(version, new_version)
            out += l
    with open('setup.py', 'w') as f:
        f.write(out)

    # create python pkg
    local('rm -rf dist')
    local('mkdir dist')
    local('python setup.py sdist')


def deploy(deps=False, noweb=False):
    pkg()

    nodeps = '--no-deps'
    if deps:
        nodeps = ''

    # copy the stuff
    run('rm crap-fuzzy-*.tar.gz || true')
    put('dist/crap-fuzzy-*.tar.gz', '.')
    sudo('pip3 install --upgrade {} crap-fuzzy-*.tar.gz'.format(nodeps))

    if not noweb:
        put('web', '.')
        sudo('rm -rf /var/www/fuzzy || true')
        sudo('mkdir -p /var/www')
        sudo('mv web /var/www/fuzzy')

    deploy_conf()


def deploy_conf():
    put('./conf', '.')

    run('sync')
    sudo('systemctl restart fuzzy.service')


def save_conf_big():
    get('conf/big', './conf/')


def save_conf_little():
    get('conf/little', './conf/')


def setup_systemctl(target="big"):
    run('mkdir -p .config/systemd/user')
    put('./conf/systemd/fuzzy-{}.service'.format(target), 'fuzzy.service')
    sudo('mv fuzzy.service /etc/systemd/system/')

    sudo('systemctl enable fuzzy.service')
    sudo('systemctl daemon-reload')
    sudo('systemctl restart fuzzy.service')


def setup_systemctl_little():
    setup_systemctl("little")
