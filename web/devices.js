(function(){
    devices = {};

    devices.show = function() {
        var content = $("#content");
        content.empty();
        content.addClass('device');

        $.get('http://' + window.location.hostname + ':8080/api/devices/servos', function(data) {
            $.each(data, function(name, val) {
                if (val.description !== "") {
                    var s = new Servo(name, val.description);
                    content.append(s.div);
                }
            });
        });
        $.get('http://' + window.location.hostname + ':8080/api/devices/vlxs', function(data) {
            $.each(data, function(name, val) {
                if (val.description !== "") {
                    var s = new Vlx(name, val.description);
                    content.append(s.div);
                }
            });
        });
    };

    devices.stop = function() {
        var content = $("#content");
        content.empty();
    };

    class Range {
        constructor(name, f=undefined, value=50) {
            this.div = $('<div/>');
            this.f = f;
            if (!this.f) {
                this.f = function(a) {return a;};
            }
            let that = this;
            $('<label/>')
                .text(name + ':')
                .appendTo(this.div);
            this.in = $('<input type="range" min="0" max="100" value="' + value + '"/>')
                .appendTo(this.div)
                .on('input', function(evt) {that.pos_changed(evt);})
                .on('change', function(evt) {that.pos_changed(evt);});
            this.txt = $('<label/>')
                .text(this.f(this.in[0].value))
                .appendTo(this.div);

            this.cb = {};
        }

        get val() {
            return this.f(this.in.val());
        }

        pos_changed(evt) {
            let val = this.f(evt.target.value);
            this.txt.text(val);

            if (this.cb.change) {
                this.cb.change(val);
            }
        }

        on(event, f) {
            this.cb[event] = f;
        }
    }

    class Step {
        constructor(name, val, range) {
            this.div = $('<div/>');
            this.range = range;

            let that = this;
            this.btn = $('<button/>')
                .text(name)
                .appendTo(this.div)
                .on('click', function(evt) {that.onclick(evt);});
            this.txt = $('<label/>')
                .text(val)
                .appendTo(this.div);

            this.cb = {};
        }
        get val() {
            return this.txt.text();
        }
        onclick(evt) {
            this.txt.text(this.range.val);
            if (this.cb.click) {
                this.cb.click();
            }
        }
        on(event, f) {
            this.cb[event] = f;
        }
    }

    class Servo {
        constructor(name, description) {
            this.url = 'http://' + window.location.hostname + ':8080/api/devices/servo/' + name;
            this.div = $('<div/>').addClass('servo');

            let that = this;

            // Append a header title
            $('<h2/>')
                .text(description)
                .appendTo(this.div);
            $('<h3/>')
                .text(name)
                .appendTo(this.div);

            this.pos = new Range("pos");
            this.pos.div.appendTo(this.div);
            this.min = new Step("min", 40, this.pos);
            this.min.div.appendTo(this.div);
            this.max = new Step("max", 60, this.pos);
            this.max.div.appendTo(this.div);
            this.dur = new Range("dur", function(a) {return parseFloat((a / 33.).toFixed(1));});
            this.dur.div.appendTo(this.div);

            this.btn = $('<button/>')
                .text("Start")
                .appendTo(this.div)
                .on('click', function(evt) {that.start_oscillating();});

            this._ready = true;
            this.pos.on('change', function(val) {
                that.put_pos(val);
            });
        }

        put_pos(val, duration=0, f=undefined) {
            if (this._ready) {
                this._ready = false;
                let that = this;
                $.put(
                    this.url,
                    {
                        active: true,
                        position: parseFloat(val),
                        duration: duration
                    },
                    null,
                    function() {
                        that._ready = true;
                        if (f) {
                            f();
                        }
                    });
            }
        }
        start_oscillating() {
            if (this.max.val <= this.min.val) {
                console.log("BAD inputs");
                return;
            }
            let dt = (this.max.val - this.min.val) / 100 * this.dur.val;
            console.log("Start oscillating between ", this.min.val, "and", this.max.val, "at", dt);

            let remain = 6;
            let that = this;
            function f() {
                let val = that.min.val;
                if (remain % 2 == 0) {
                    val = that.max.val;
                }
                remain -= 1;
                if (remain > 0) {
                    that.put_pos(val, dt, f);
                }
            }
            f();
        }
    }

    class Vlx {
        constructor(name, description) {
            this.url = 'http://' + window.location.hostname + ':8080/api/devices/vlx/' + name;
            this.div = $('<div/>').addClass('vlx');
            console.log("name, description", name, description);
            let that = this;

            // Append a header title
            $('<h2/>')
                .text(description)
                .appendTo(this.div);
            $('<h3/>')
                .text(name)
                .appendTo(this.div);
            let d = $('<div/>')
                .text('Led: ')
                .appendTo(this.div);

            this.chk = $("<input/>")
                .attr("type", "checkbox")
                .appendTo(d)
                .on('click', function(evt) {
                    that.check_changed();
                });

            this.dist = $("<div/>")
                .text("dist: ?")
                .appendTo(this.div)
                .on('click', function(evt) {
                    that.fetch_dist();
                });
        }
        check_changed() {
            $.put(
                this.url + "/led",
                {
                    led: this.chk[0].checked
                }
            );
        }
        fetch_dist() {
            let that = this;
            $.get(this.url + "/distance", function(data) {
                    that.dist.text("dist: " + data.distance);
                });
        }
    }
})();
