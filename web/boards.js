(function(){

    var pannels = {
        servos: servos,
        futabas: futabas,
        vlx: vlx,
        pumps: pumps,
        motor: motor,
    };

    boards = {};
    boards.show = function() {
        var content = $("#content");
        content.empty();
        base.host = window.location.hostname;

        $.get('http://' + window.location.hostname + ':8080/api/boards', function(data) {
            $.each(data, function(ix, val) {
                if (pannels.hasOwnProperty(val.type)) {
                    var board = new pannels[val.type].Board(val);
                    content.append(board.div);
                } else {
                    console.log("Unknown board ", val);
                }
            });
        });
    };

    boards.stop = function() {
        var content = $("#content");
        content.empty();
    };
})();
