(function(){

    $.put = function(url, data, success, complete) {
        $.ajax(
            {
                url: url,
                type: "PUT",
                data: JSON.stringify(data),
                success: success,
                complete: complete
            });
    };

    var current = null;

    var entries = {
        "Boards": boards,
        "Devices": devices,
        "Actions": actions,
        "Move": move,
    };

    var lis = {};

    function activate(entry) {
        if (current != null) {
            current.stop();
        }
        for(var li in lis) {
            lis[li].removeClass('selected');
        }
        lis[entry].addClass('selected');
        current = entries[entry];
        current.show();
    }

    function setup_menu() {
        var menu = $('nav');

        var lst = $('<ul/>').appendTo(menu);

        $.each(entries, function(entry) {
            var li = $('<li/>')
                .appendTo(lst)
                .text(entry)
                .on('click', function() {
                    activate(entry);
                });
            lis[entry] = li;
        });
    }

    setup_menu();
    activate("Move");
})();
