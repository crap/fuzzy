(function(){

    class Move {
        constructor(content) {
            this.url = 'http://' + window.location.hostname + ':8080/api/move';
            this.div = $('<div/>')
                .addClass('move')
                .html(' \
                <canvas id="move-canvas" width="3000" height="2000">HERE MAP</canvas> \
                <div id="move-controls"> \
                    <span>Color:</span> \
                    <button class="green">Green</button> \
                    <button class="orange">Orange</button> \
                    <span>Move:</span> \
                    <button class="movego">Go!</button> \
                </div>')
                .appendTo(content);

            paper.setup($('#move-canvas')[0]);

            // LOADING
            this.ratio = paper.view.size.width / 3;
            console.log("Ratio", this.ratio, paper.view.size.width);

            this.layers = {
                obstacles: new paper.Layer({name: 'Obstacles'}),
                path: new paper.Layer({name: 'Path'}),
                robot: new paper.Layer({name: 'Robot'}),
                target: new paper.Layer({name: 'Target'})
            };

            this.obstacles = [];

            this.rob = null;

            $.get(this.url, (data) => {
                console.log("GET", data);

                // Create Robot
                this.rob = new Robot(
                    this,
                    this.url + "/reset",
                    data.position,
                    data.model);
            });


            this.tar = new Target(this, this.url + "/to");
            this.tar.onPositionSelected = (pos) => {this.testTarget(pos);};

            this.path = new Path(this);

            paper.view.draw();

            // Fetch HTML elements
            this.$greenbtn = this.div.find("button.green");
            this.$orangebtn = this.div.find("button.orange");
            this.$gobtn = this.div.find("button.movego");

            this.$greenbtn.on('click', this.setColor('green'));
            this.$orangebtn.on('click', this.setColor('orange'));
            this.$gobtn.on('click' , () => {this.go();});

            // Start WS
            var ws_url = this.url.replace("http", "ws") + "/events";
            console.log("Opening WS", ws_url);
            this.ws = new WebSocket(ws_url);

            this.ws.onmessage = (evt) => {
                var data = JSON.parse(evt.data);

                this.rob.x = data.position.x;
                this.rob.y = data.position.y;
                this.rob.theta = data.position.theta;
                this.rob.update();

                this.path.reset(data.position, data.targets, 0.2);

                for (let o of this.obstacles) {
                    console.log("o", o);
                    o.p.remove();
                }

                this.obstacles = [];

                for (let o of data.robobstacles) {
                    this.obstacles.push(
                        new Obstacle(this, o)
                    );
                }
            };
        }

        stop() {
            this.ws.close();
        }

        toValue(val) {
            return val * this.ratio;
        }
        toPoint(obj) {
            return new paper.Point(this.toValue(obj.y), this.toValue(obj.x));
        }
        fromValue(val) {
            return val / this.ratio;
        }
        fromPoint(obj) {
            return {
                x: this.fromValue(obj.y),
                y: this.fromValue(obj.x),
                theta: [0, obj.theta]['theta' in obj]
            };
        }

        setColor(color) {
            return (evt) => {
                console.log ("Color " + color);
                let url = this.url.replace("move", "color") + "/" + color;
                $.put(
                    url,
                    {},
                    (data) => {
                        console.log("Color set to ", color);
                    }
                );
            };
        }

        testTarget() {
            let pos = {
                x: this.tar.x,
                y: this.tar.y,
                theta: this.rob.theta
            };
            console.log("Test Go to ", pos);
            this.tar.setPos(pos, true, true);
        }

        go() {
            let pos = {
                x: this.tar.x,
                y: this.tar.y,
                theta: this.rob.theta
            };

            console.log("Go to ", pos);
            this.tar.setPos(pos);
        }
    }


    class Robot {
        constructor(drawing, url, pos, model) {
            this.drawing = drawing;
            this.url = url;
            this.x = pos.x;
            this.y = pos.y;
            this.theta = pos.theta;

            this.drawing.layers.robot.activate();

            let center = this.drawing.toPoint(this);

            if (model === "Big") {
                const WIDTH = 0.35;
                const HEIGHT = 0.25;
                let from = this.drawing.toPoint({x:-HEIGHT/2, y: -WIDTH/2});
                let to = this.drawing.toPoint({x:HEIGHT/2, y: WIDTH/2});

                let rec = new paper.Path.Rectangle(from, to);

                let tri = new paper.Path();
                tri.add(this.drawing.toPoint({x:HEIGHT/2, y: 0.1}));
                tri.add(this.drawing.toPoint({x:0.03, y: 0}),
                        this.drawing.toPoint({x:HEIGHT/2, y: -0.1}));

                this.p = new paper.CompoundPath({
                    children: [rec, tri]
                });
            } else {
                // Little
                const RADIUS = 0.13;
                let circ = new paper.Path.Circle({
                    center: [0, 0],
                    radius: this.drawing.toValue(RADIUS)
                });
                let ahead = new paper.Path.Line(
                    this.drawing.toPoint({x: 0, y: 0}),
                    this.drawing.toPoint({x: RADIUS, y: 0})
                );
                this.p = new paper.CompoundPath({
                    children: [circ, ahead]
                });
            }
            this.p.position = center;

            this.prevAngle = 0;
            this.p.rotate(this.prevAngle);

            this.p.fillColor = 'gold';
            this.p.strokeColor = 'black';
            this.p.strokeWidth = this.drawing.toValue(0.005);

            this.p.onMouseDown = (evt) => {this.showText(evt);};
            this.p.onMouseDrag = (evt) => {this.move(evt);};
            this.p.onMouseUp = (evt) => {this.apply(evt);};

            paper.view.draw();

            this.text = null;

            this.updateAllowed = true;
            // this.listen-to  (this.model, "new-pos" , this.update);
        }

        update() {
            if (!this.updateAllowed) {
                return;
            }

            let pt =  this.drawing.toPoint({
                x: this.x,
                y: this.y
            });

            this.p.position = pt;

            let angle =  this.theta / Math.PI * -180;
            let diff = angle - this.prevAngle;

            this.p.rotate(diff);
            this.prevAngle = angle;
        }

        showText(evt) {
            this.updateAllowed = false;
            this.drawing.layers.robot.activate();

            if (this.text) {
                this.text.remove();
                this.text = null;
            }
            this.text = new paper.PointText(
                this.drawing.toPoint({x:0.2, y:1.5})
            );
            this.text.justification = 'center';
            this.text.fillColor = 'white';
            this.text.fontSize = this.drawing.toValue(0.1);
            this.text.fontWeight = 'bold';
        }

        move(evt) {
            this.p.position = evt.point;

            let pt = this.drawing.fromPoint(evt.point);
            this.text.content =
                "Robot: " + pt.x.toFixed(2) + "/" + pt.y.toFixed(2);
        }

        apply(evt) {
            this.updateAllowed = true;
            let pos = this.drawing.fromPoint(evt.point);
            console.log("Robot reset", pos);
            this.text.remove();

            // Save
            this.x = pos.x;
            this.y = pos.y;

            let data = {
                x: this.x,
                y: this.y,
                theta: this.theta
            };
            console.log("Putting ", data);

            $.put(this.url, data);
        }
    }

    class Target {
        constructor(drawing, url) {
            this.drawing = drawing;
            this.url = url;
            console.log("Target URL " + this.url);

            this.drawing.layers.target.activate();

            this.x = 1;
            this.y = 1;
            this.theta = 0;

            let pt = this.drawing.toPoint(this);
            let d = this.drawing.toPoint({x: 0.1, y:0});
            let circ = new paper.Path.Circle(pt, this.drawing.toValue(0.08));
            let cross = new paper.Path();
            cross.add(pt);
            cross.add(pt.add(d));
            cross.add(pt.subtract(d));
            cross.add(pt);
            d = d.rotate(90);
            cross.add(pt.add(d));
            cross.add(pt.subtract(d));
            this.p = new paper.CompoundPath({
                children: [circ, cross]
            });

            this.p.fillColor = '#A0A0A0';
            this.p.fillColor .alpha = 0.6;
            this.p.strokeColor = 'black';
            this.p.strokeWidth = this.drawing.toValue(0.01);

            paper.view.draw();

            this.text = null;
            this.onPositionSelected = null;

            this.p.onMouseDown = (evt) => {this.showText(evt);};
            this.p.onMouseDrag = (evt) => {this.move(evt);};
            this.p.onMouseUp = (evt) => {this.apply(evt);};
        }

        showText(evt) {
            this.drawing.layers.target.activate();
            if (this.text) {
                this.text.remove();
                this.text = null;
            }

            this.text = new paper.PointText(
                this.drawing.toPoint({x:0.2, y:1.5})
            );
            this.text.justification = 'center';
            this.text.fillColor = 'white';
            this.text.fontSize = this.drawing.toValue(0.1);
            this.text.fontWeight = 'bold';

            this.move(evt);
        }

        move(evt) {
            this.p.position = evt.point;

            let pt = this.drawing.fromPoint(evt.point);
            this.text.content =
                "Target: " + pt.x.toFixed(2)  + "/" + pt.y.toFixed(2);

            this.x = pt.x;
            this.y = pt.y;
        }

        apply(evt) {
            this.text.remove();
            if (this.onPositionSelected) {
                this.onPositionSelected(this);
            }
        }

        force(p) {
            this.p.position = this.drawing.toPoint(p);
            paper.view.draw();
        }

        setPos(target, simulate=false, log=false) {
            let data = {
                target: target,
                simulate: simulate,
                log: log
            };
            $.put(this.url, data);
        }
    }

    class Obstacle {
        constructor(drawing, data) {
            this.drawing = drawing;
            this.drawing.layers.obstacles.activate();

            this.x = data.center.x;
            this.y = data.center.y;
            this.theta = 0;

            let pt = this.drawing.toPoint(this);
            this.p = new paper.Path.Circle(pt, this.drawing.toValue(data.radius));
            this.p.fillColor = '#A0A0A0';
            this.p.fillColor.alpha = 0.6;
            this.p.strokeColor = 'red';
            this.p.strokeWidth = this.drawing.toValue(0.01);

            paper.view.draw();
        }
    }

    class Path {

        constructor(drawing) {
            this.drawing = drawing;

            drawing.layers.path.activate();

            this.g = new paper.Group();
            this.p = new paper.Path();
            this.p.strokeColor = 'green';
            this.p.strokeColor.alpha = 0.3;

            this.g.addChild(this.p);
            paper.view.draw();
        }

        reset(rob, points, radius) {
            radius = this.drawing.toValue(radius);

            // Remove all children but the path
            this.g.removeChildren(1);

            this.p.segments = [];
            this.p.strokeWidth = 2 * radius;

            let pt = this.drawing.toPoint(rob);
            this.p.add(pt);

            points.forEach((point) => {
                pt = this.drawing.toPoint(point);
                this.p.add(pt);

                let step = new paper.Path.Circle(pt, radius);
                step.fillColor = 'green';
                step.fillColor .alpha = 0.8;
                this.g.addChild(step);
            });

            paper.view.draw();
        }
    }
    /* ******************************** */

    let _move = null;
    move = {};
    move.show = function() {
        var content = $("#content");
        content.empty();

        _move = new Move(content);
    };

    move.stop = function() {
        if (_move) {
            _move.stop();
        }

        var content = $("#content");
        content.empty();
    };
})();
