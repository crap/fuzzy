(function(){
    class ServoEdit {
        constructor(name, position, onchange) {
            this.name = name;
            this.position = position;
            this.onchange = onchange;

            this.div = $('<div/>')
                .addClass('slider-edit-servo');

            $('<p>' + this.name + ': <p/>')
                .appendTo(this.div);

            this.range = $('<input type="range"/>')
                .attr("min", -90)
                .attr("max", 90)
                .val(position)
                .appendTo(this.div);

            this.val = $("<label/>")
                .text(this.position)
                .appendTo(this.div);

            var that = this;
            this.range.on("change", function(evt){that.input_changed(evt, true);});
            this.range.on("input", function(evt){that.input_changed(evt, false);});

            this.redraw();
        }

        redraw() {
            this.range.val(this.position);
            this.val.text(this.position);
        };

        input_changed(evt, apply) {
            this.position = parseInt(evt.target.value);
            this.redraw();

            if (apply && this.onchange) {
                this.onchange();
            }
        };
    }

    class ActionSliderEdit {
        constructor(name, baseurl, onSaved) {
            this.name = name;
            this.url = baseurl + '/' + this.name;
            this.onSaved = onSaved;

            this.div = $('<div/>')
                .addClass('slider-edit');

            var that = this;

            let btn = $('<button>')
                .text("Save")
                .on('click', function() {that.save(false); that.onSaved();})
                .appendTo(this.div);

            this.servos = {};
            this.servoedits = {};

            $.get(this.url + '/edit', function(data) {
                that.servos = data;

                for (let servo in data) {
                    let servoedit = new ServoEdit(
                        servo,
                        data[servo].pos,
                        function() {that.save(true);}
                    );
                    that.servoedits[servo] = servoedit;
                    btn.before(servoedit.div);
                }
            });
        }

        save(testonly=true) {
            for (let servo in this.servos) {
                this.servos[servo].pos = this.servoedits[servo].position;
            }

            let url = this.url + (testonly ? '/test' : '/save');
            $.put(url, this.servos);
        }
    }

    class ActionSlider {
        constructor(name, slider, baseurl) {
            this.div = $('<div/>').addClass('slider');
            this.steps = slider.steps;
            this.url = baseurl + '/sliders/' + name;

            var that = this;
            var ix = slider.steps.indexOf(slider.current);

            $('<h4/>')
                .text(name + ": ")
                .appendTo(this.div);

            let txt = $('<label/>')
                .addClass('position')
                .text(slider.steps[ix])
                .attr('title', "Click to edit this position")
                .appendTo(this.div);
            let inp = $('<input/>')
                .attr('type', 'range')
                .attr('min', 0)
                .attr('max', slider.steps.length - 1)
                .val(ix)
                .appendTo(this.div);

            // Input is when click is maintained
            inp.on('input', function(evt) {
                txt.text(slider.steps[evt.target.value]);
            });
            inp.on('change', function(evt) {
                $.put(
                    that.url  + '/' + txt.text(),
                    null);
                });

            txt.on('click', function(evt) {
                that.div.toggleClass('editing');
                let editing = that.div.hasClass('editing');

                inp.attr('disabled', editing);

                if (editing) {
                    txt._edit = new ActionSliderEdit(txt.text(), that.url, function() {
                        txt.click();
                    });
                    that.div.append(txt._edit.div);
                    txt.text(that.steps[inp.val()] + " (editing)");
                    $(document).on('keyup', function(e) {
                        if (e.keyCode == 27) {
                            txt.click();
                        }
                    });
                } else {
                    txt.text(that.steps[inp.val()]);
                    txt._edit.div.remove();
                    $(document).unbind('keyup');
                }
            });
        }
    }

    class Action {
        constructor(name, data) {
            this.name = name;
            this.clicks = data.cicks;
            this.sliders = data.sliders;

            this.div = $('<div/>').addClass('action');
            this.url = 'http://' + window.location.hostname + ':8080/api/actions/' + this.name;

            var that = this;
            // Append a header title
            $('<h2/>')
                .text(this.name)
                .appendTo(this.div);

            for (let name in data.sliders) {
                let slider = new ActionSlider(name, data.sliders[name], this.url);
                this.div.append(slider.div);
            }

            let clickers_div = $('<div/>').addClass('clickers').appendTo(this.div);
            for (let name in data.clicks) {
                let click = data.clicks[name];

                let btn = $('<button/>')
                    .text(name)
                    .prop('title', click.help)
                    .appendTo(clickers_div);
                btn.on('click', function(evt) {
                    $.put(
                        that.url + '/clicks/' + name,
                        null
                    );
                });
            }
        }
    }

    actions = {};
    actions.show = function() {
        var content = $('#content').empty();
        console.log("Content:", content);

        $.get('http://' + window.location.hostname + ':8080/api/actions', function(data) {
            $.each(data, function(action, val) {
                console.log("ACT", action, val);

                var act = new Action(action, val);
                content.append(act.div);
            });
        });
    };

    actions.stop = function() {
    };
})();
