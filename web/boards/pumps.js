(function(){
    class PumpsBoard extends base.Board {
        init(data) {
            super.init(data);
            var that = this;

            data = Object.assign(
                {
                    power: {
                        req: false,
                        on: false
                    },
                    pumps: [
                        {on: false},
                        {on: false},
                        {on: false},
                        {on: false},
                        {on: false},
                    ]
                },
                data);

            var table = $("<table/>").appendTo(this.content_div);
            $("<tr/>")
                .append("<th>0</th><th>1</th><th>2</th><th>3</th><th>4</th>")
                .appendTo(table);

            let row = $("<tr/>").appendTo(table);

            for (let i=0; i<5; i++) {
                let onchk = $("<input/>")
                    .attr("type", "checkbox")
                    .prop("checked", data.pumps[i].on);

                row.append($("<td/>").append(onchk));

                let pump = new Pump(this.url, i, onchk);
            }

            $("<button/>")
                .text("RELEASE")
                .appendTo(this.content_div)
                .on("click", function(evt) {
                    $.put(that.url + "/release", {});
                });
        }
    }


    class Pump {
        constructor(base_url, pump_id, onchk) {
            // Storage
            var url = base_url + "/" + pump_id;

            // Input Elements
            onchk.on("click", function(evt) {
                $.put(url, {on: evt.target.checked});
            });
        }
    }

    pumps = {Board: PumpsBoard};
})();
