
(function(){
    class VlxBoard extends base.Board {

        init(data) {
            super.init(data);

            var canvas = $("<canvas/>")
                .appendTo($("<div/>").appendTo(this.content_div));

            var chart = new smoothie.SmoothieChart({maxValue:265, minValue:-5});
            chart.streamTo(canvas[0]);

            this.colors = [
                "rgba(255, 0, 0, 1)",
                "rgba(0, 255, 0, 1)",
                "rgba(0, 0, 255, 1)",
                "rgba(255, 255, 0, 1)",
                "rgba(0, 255, 255, 1)",
                "rgba(255, 0, 255, 1)"
            ];

            // Data
            var lines = [];
            for (var i = 0; i < 6; i++) {
                var line = new smoothie.TimeSeries();
                lines.push(line);
                // Add to SmoothieChart
                chart.addTimeSeries(line, { strokeStyle: this.colors[i], lineWidth: 4 });
            }

            var ws_url = this.url.replace("http", "ws") + "/distances/ws";
            this.ws = new WebSocket(ws_url);

            this.ws.onmessage = function (evt) {
                var data = JSON.parse(evt.data);

                let dleft = data[1] - 85.67;
                let dright = data[2];
                let avg = (dleft + dright) / 2 - 10;
                let diff = dleft - dright;
                let angle = Math.atan2(diff, 190.61);

                // console.log("dist", avg.toFixed(0), "angle", (angle / Math.PI * 180).toFixed(0));

                var t = new Date().getTime();
                for (var i=0; i<6; i++) {
                    lines[i].append(t, data[i]);
                }
            };
        }

        stop() {
            console.log("Stopping");
            this.ws.close();
        }
    }
    vlx = {Board: VlxBoard};
})();
