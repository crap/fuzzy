(function() {
    class FutabasBoard extends base.Board {

        init(data) {
            super.init(data);

            var table = $("<table/>").appendTo(this.content_div);
            $("<tr/>")
                .appendTo(table)
                .append("<th>ID</th><th>position</th>");

            for (let ix in data.futabas) {
                let fut = data.futabas[ix];

                let label = $("<label/>")
                    .text(ix);
                let valrange = $("<input/>")
                    .attr("type", "range")
                    .attr("min", 0)
                    .attr("max", 100);

                let val = $("<label/>");

                let row = $("<tr/>");

                row.append($("<td/>").append(label));
                row.append($("<td/>").append(valrange).append(val));

                row.appendTo(table);

                let servo = new Servo(
                    this.url,
                    ix,
                    fut.position,
                    valrange,
                    val);
                servo.redraw();
            }
        }
    }

    class Servo {
        constructor(base_url, servo_id, position, valrange, valtxt) {
            // Storage
            this.url = base_url + "/" + servo_id;

            // Real data to send
            this.position = parseFloat(position.toFixed(1));

            // Input Elements
            this.valrange = valrange;
            this.valtxt = valtxt;

            this._ready = true;

            var that = this;

            this.valrange.on("change", function(evt){that.input_changed(evt);});
            this.valrange.on("input", function(evt){that.input_changed(evt);});
        }

        update() {
            if (this._ready) {
                this._ready = false;
                let that = this;
                $.put(this.url,
                    {
                        position: this.position,
                        duration: 0
                    },
                    null,
                    function() {
                        that._ready = true;
                    }
                    );
            }
        };

        redraw() {
            this.valrange.val(this.position);
            this.valtxt.text(this.position);
        };

        input_changed(evt) {
            this.position = parseInt(evt.target.value);
            this.update();
            this.redraw();
        };
    }


    futabas = {Board: FutabasBoard};
})();
