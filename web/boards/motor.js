(function(){
    class MotorBoard extends base.Board {

        init(data) {
            console.log("Motor Init data", data);
            super.init(data);

            data = Object.assign({
                motor: {
                    pwm: 0,
                    current: 0,
                    encoder: 0,
                    eocs: [false, false],
                    pid: {
                        up: {
                            fixed: 0,
                            kp: 0,
                            ki: 0,
                            kd: 0
                        },
                        down: {
                            fixed: 0,
                            kp: 0,
                            ki: 0,
                            kd: 0
                        }
                    }
                }
            },
                data);

            this._pwm = parseFloat(data.motor.pwm.toFixed(1));
            this._ready = true;

            let table = $('<table/>').appendTo(this.content_div);

            let txt = $("<label/>")
                .text("PWM:");
            this.valrange = $("<input/>")
                .attr("type", "range")
                .attr("min", -100)
                .attr("max", 100)
                .val(this._pwm);
            this.val = $("<label/>")
                .text(this._pwm);
            $("<tr/>")
                .append($("<td/>").append(txt))
                .append($("<td/>").append(this.valrange))
                .append($("<td/>").append(this.val))
                .appendTo(table);

            var that = this;
            this.valrange.on("change", function(evt){that.reset(evt);});
            this.valrange.on("input", function(evt){that.input_changed(evt);});


            $("<tr/>")
                .append($("<td/>").text("Current:"))
                .append($("<td/>").text(data.motor.current.toFixed(1)))
                .appendTo(table);

            $("<tr/>")
                .append($("<td/>").text("Encoder:"))
                .append($("<td/>").text(data.motor.encoder))
                .appendTo(table);

            $("<tr/>")
                .append($("<td/>").text("EOCs:"))
                .append($("<td/>").text(data.motor.eocs))
                .appendTo(table);


            this.content_div.append($("<span>PID</span>"));
            let pid = $('<table/>').appendTo(this.content_div);

            this.pid = {
                calib_pwm: $("<input/>").addClass("small").val(-20),
                calib: $("<button/>").text("calib"),

                up: {
                    fixed: $("<input/>").addClass("small").val(0),
                    kp: $("<input/>").addClass("small").val(0),
                    ki: $("<input/>").addClass("small").val(0),
                    kd: $("<input/>").addClass("small").val(0),
                },
                down: {
                    fixed: $("<input/>").addClass("small").val(0),
                    kp: $("<input/>").addClass("small").val(0),
                    ki: $("<input/>").addClass("small").val(0),
                    kd: $("<input/>").addClass("small").val(0),
                },
                apply: $("<button/>").text("Apply"),
                max: $("<input/>").val(10000),
                speed: $("<input/>").val(1000),
                move: $("<input/>")
                    .attr("type", "range")
                    .attr("min", 0)
                    .attr("max", 100)
                    .val(0),
                move_val: $("<span/>").text(0)
            };

            $("<tr/>")
                .append($("<td/>").text("calib.:"))
                .append($("<td/>").append(this.pid.calib_pwm))
                .append($("<td/>").append(this.pid.calib))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("direc.:"))
                .append($("<td/>").text("up"))
                .append($("<td/>").text("down"))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("fixed:"))
                .append($("<td/>").append(this.pid.up.fixed))
                .append($("<td/>").append(this.pid.down.fixed))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Kp:"))
                .append($("<td/>").append(this.pid.up.kp))
                .append($("<td/>").append(this.pid.down.kp))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Ki:"))
                .append($("<td/>").append(this.pid.up.ki))
                .append($("<td/>").append(this.pid.down.ki))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Kd:"))
                .append($("<td/>").append(this.pid.up.kd))
                .append($("<td/>").append(this.pid.down.kd))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Apply"))
                .append($("<td/>").append(this.pid.apply))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Max"))
                .append($("<td/>").attr("colspan", 2).append(this.pid.max))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Speed"))
                .append($("<td/>").attr("colspan", 2).append(this.pid.speed))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Move"))
                .append($("<td/>").attr("colspan", 2).append(this.pid.move))
                .appendTo(pid);
            $("<tr/>")
                .append($("<td/>").text("Val"))
                .append($("<td/>").attr("colspan", 2).append(this.pid.move_val))
                .appendTo(pid);

            this.pid.calib.on("click", (evt) => this.calib_eoc(evt));
            this.pid.apply.on("click", (evt) => this.apply_pid(evt));
            this.pid.move.on("input", (evt) => this.pid_move(evt));
        }

        reset(evt) {
            this._pwm = 0;
            this.update();
            this.redraw();
        };

        input_changed(evt) {
            this._pwm = parseInt(evt.target.value);
            this.update();
            this.redraw();
        };

        pid_move(evt) {
            let val = parseInt(evt.target.value) / 100.;
            let max = parseFloat(this.pid.max.val());
            let speed = parseFloat(this.pid.speed.val());

            this.pid.move_val.text((val * max).toFixed(0));

            if (this._ready) {

                let dat = {
                    ref: val * max,
                    tol: 0,
                    spd: speed
                };

                this._ready = false;
                console.log("Putting PID ", dat);
                $.put(this.url + '/pid/set', dat, undefined, () => {
                    this._ready = true;
                });
            }
        };

        update() {
            if (this._ready) {
                this._ready = false;
                var that = this;
                console.log("Putting", this._pwm);
                $.put(this.url + '/pwm', this._pwm, undefined, function() {
                    that._ready = true;
                });
            }
        };

        redraw() {
            this.valrange.val(this._pwm);
            this.val.text(this._pwm);
        };

        calib_eoc(evt) {
            let pwm = parseFloat(this.pid.calib_pwm.val());
            let dat = {
                pwm: pwm,
                reset_on_bump: true
            };
            console.log("Calib EOC", dat);
            $.put(this.url + '/bump', dat, undefined, () => {
                console.log("Calib Sent!");
            });
        }

        apply_pid(evt) {
            let args_up = {
                direction: "up",
                fixed: parseFloat(this.pid.up.fixed.val()),
                kp: parseFloat(this.pid.up.kp.val()),
                ki: parseFloat(this.pid.up.ki.val()),
                kd: parseFloat(this.pid.up.kd.val()),
            };
            let args_down = {
                direction: "up",
                fixed: parseFloat(this.pid.down.fixed.val()),
                kp: parseFloat(this.pid.down.kp.val()),
                ki: parseFloat(this.pid.down.ki.val()),
                kd: parseFloat(this.pid.down.kd.val()),
            };
            console.log("Set PID", args_up, args_down);
            $.put(this.url + '/pid/config', args_up, undefined, () => {
                console.log("Put UP!");
            });
            $.put(this.url + '/pid/config', args_down, undefined, () => {
                console.log("Put DOWN!");
            });
        }
        stop_pid(evt) {
            console.log("Stop PID", args);
            $.put(this.url + '/pid/stop', {}, undefined, () => {
                console.log("Stopped!");
            });
        }
    }

    motor = {Board: MotorBoard};
})();
