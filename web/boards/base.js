(function() {
    class Board {
        constructor(board) {
            this.board = board;
            this.div = $("<div/>").addClass('board');
            this.base_url = 'http://' + window.location.hostname + ':8080/api/boards/' + this.board.id;
            this.url = this.base_url + "/" + this.board.type;

            var that = this;
            // Append a header title
            $("<h2/>")
                .text(this.board.type + " board #" + this.board.id)
                .appendTo(this.div)
                .on("click", function(){
                    that.reload();
                });
            // Append a content div
            this.content_div = $("<div/>").appendTo(this.div);
        }

        reload() {
            this.stop();
            this.content_div.empty();

            var that = this;
            $.get(this.url, function(data) {
                var dat = {
                    power: {
                        on: false
                    }
                };
                data = Object.assign(dat, data);
                if ("error" in data) {
                    console.log("Error for board loading, default only");
                    //return;
                }
                that.init(data);
            });
        }
        init(data) {
            var that = this;
            $("<label>")
                .text("Power:")
                .appendTo(this.content_div);

            $("<input/>")
                .attr("type", "checkbox")
                .prop("checked", data.power.on)
                .appendTo(this.content_div)
                .on("click", function(evt) {
                    var url = that.base_url + "/power";
                    $.put(url, {on: evt.target.checked});
            });
        }

        stop() {
            // Just in case
        }
    }
    base = {Board};
})();
