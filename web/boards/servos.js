(function(){
    class ServosBoard extends base.Board {

        init(data) {
            super.init(data);

            var table = $("<table/>").appendTo(this.content_div);
            $("<tr/>")
                .appendTo(table)
                .append("<th>ID</th><th>position</th><th>ON</th>");

            for (let i=0; i<6; i++) {
                let label = $("<label/>")
                    .text(i);
                let valrange = $("<input/>")
                    .attr("type", "range")
                    .attr("min", -90)
                    .attr("max", 90);

                let val = $("<label/>");

                let chk = $("<input/>")
                    .attr("type", "checkbox");

                let row = $("<tr/>");
                row.append($("<td/>").append(label));
                row.append($("<td/>").append(valrange).append(val));
                row.append($("<td/>").append(chk));

                row.appendTo(table);

                let dat = {
                    active: false,
                    position: 0
                };
                if ("servos" in data) {
                    Object.assign(dat, data.servos[i]);
                }

                let servo = new Servo(
                    this.url,
                    i,
                    dat.active,
                    dat.position,
                    valrange,
                    val,
                    chk);
                servo.redraw();
            }

            if (data.bumps === undefined) {
                data.bumps = [false, false]
            }
            let label = $("<label/>")
                .text(`Bumps: ${data.bumps[0]} ${data.bumps[1]}`)
                .appendTo(this.content_div);
        }
    }

    class Servo {
        constructor(base_url, servo_id, active, position, valrange, valtxt, chk) {
            // Storage
            this.url = base_url + "/" + servo_id;

            // Real data to send
            this.active = active;
            this.position = parseFloat(position.toFixed(1));

            // Input Elements
            this.valrange = valrange;
            this.valtxt = valtxt;
            this.chk = chk;

            this._ready = true;

            var that = this;

            this.valrange.on("change", function(evt){that.input_changed(evt);});
            this.valrange.on("input", function(evt){that.input_changed(evt);});
            this.chk.on("click", function(evt) {
                that.active = evt.target.checked;
                that.update();
                that.redraw();
            });
        }

        update() {
            if (this._ready) {
                this._ready = false;
                let that = this;
                $.put(this.url,
                    {
                        active: this.active,
                        position: this.position,
                        duration: 0
                    },
                    null,
                    function() {
                        that._ready = true;
                    });
            }
        };

        redraw() {
            this.valrange.val(this.position);
            this.valtxt.text(this.position);
            this.chk.attr('checked', this.active);
        };

        input_changed(evt) {
            this.position = parseInt(evt.target.value);
            this.update();
            this.redraw();
        };
    }

    servos = {Board: ServosBoard};
})();
