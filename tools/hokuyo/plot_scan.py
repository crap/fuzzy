#!/usr/bin/env python3

import argparse
import asyncio
import aiohttp
import os
import json
import math

from matplotlib import pyplot as plt


async def fetch_scan(addr):
    r = await aiohttp.get('http://%s:8080/api/hokuyo/scan' % addr)
    value = await r.json()
    r.close()
    return value


def fetch_file(name):
    with open(name, "r") as f:
        return json.load(f)


if __name__ == "__main__":
    default_gw = "192.168.233.37"
    if os.path.exists("host"):
        with open("host", "r") as f:
            default_gw = f.read().strip()

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", default=None, help="Fetch scan from file")

    args = parser.parse_args()

    if args.file is not None:
        dat = fetch_file(args.file)
    else:
        dat = asyncio.get_event_loop().run_until_complete(fetch_scan(default_gw))

    dth = 2 * math.pi/1024
    center = 384

    x = []
    y = []
    for ix, val in enumerate(dat["points"]):
        th = (dat["start"] + ix - center) * dth
        _x = val * math.cos(th) * 1e-3
        _y = val * math.sin(th) * 1e-3

        x.append(_x)
        y.append(_y)

    plt.plot(x, y, "-.")
    plt.grid()
    plt.xlim(-5, 5)
    plt.ylim(-5, 5)
    plt.show()
