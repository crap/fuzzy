#!/usr/bin/env python3

import json
import math
import sys
from fuzzy.detect.reco import Scan, compute_clusters
from fuzzy.geo import Vector


def hok_cluster_pos(position, cluster):
    HOK_POS = Vector(0.04, 0, math.pi)

    # Location in robot's frame
    c = HOK_POS + cluster.center.rotated(HOK_POS.theta)

    # Location in table's frame
    rob_pos = position
    return rob_pos + c.rotated(rob_pos.theta)


out = []
for line in sys.stdin:
    pos, _, scan = line.partition('>, ')
    pos = Vector(*[float(c) for c in pos[1:].split(', ')])
    scan = eval(scan)

    clusters = compute_clusters(scan)

    # outputs: pos, clusters and clusters_in_table
    clusters_in_table = [hok_cluster_pos(pos, c) for c in clusters]

    out.append({
        'pos': pos.to_json(),
        'clusters': [c.json for c in clusters],
        'clusters_in_table': [c.to_json() for c in clusters_in_table]
    })

print(json.dumps(out, indent=2))
