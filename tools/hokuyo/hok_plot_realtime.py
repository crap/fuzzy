#!/usr/bin/env python3

import argparse
import asyncio
import json
import math
import serial
import threading
from queue import Queue, Empty

import aiohttp

from matplotlib import pyplot as plt
import matplotlib.animation as animation

from fuzzy.detect import HokuyoDev, Scan, Cluster


def plot_scan_and_clusters(lines, scan, clusters=[]):
    line_pt, line_clust = lines

    dth = 2 * math.pi/1024
    center = 384

    # points
    x = []
    y = []
    for ix, val in enumerate(scan.points):
        if val < 20:  # skip error codes
            continue
        th = (scan.start + ix - center) * dth
        _x = val * math.cos(th) * 1e-3
        _y = val * math.sin(th) * 1e-3

        x.append(_x)
        y.append(_y)

    line_pt.set_xdata(x)
    line_pt.set_ydata(y)

    # clusters
    x = []
    y = []

    for cl in clusters:
        c = cl.center
        x.append(c[0])
        y.append(c[1])

    line_clust.set_xdata(x)
    line_clust.set_ydata(y)


# serial mode
async def plot_loop(dev, plotq):
    while True:
        scan = await dev.scan_single()
        plotq.put([scan, []])


def aio_serial_thread(hokd, loop, end_evt, pq):
    with HokuyoDev(serial.Serial(hokd), loop=loop) as dev:
        asyncio.ensure_future(plot_loop(dev, pq), loop=loop)
        loop.run_until_complete(end_evt.wait())


# ws mode
async def ws_loop(ws_uri, plotq, loop):
    session = aiohttp.ClientSession(loop=loop)
    async with session.ws_connect(ws_uri) as ws:
        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                dat = json.loads(msg.data)

                scan = Scan.from_json(dat['scan'])
                clusters = [Cluster.from_json(c) for c in dat['clusters']]

                plotq.put([scan, clusters])
            elif msg.type == aiohttp.WSMsgType.CLOSED:
                break
            elif msg.type == aiohttp.WSMsgType.ERROR:
                break


async def ws_autoreco(ws_uri, plotq, loop):
    while True:
        try:
            await ws_loop(ws_uri, plotq, loop)
        except aiohttp.errors.ClientError:
            await asyncio.sleep(1)


def aio_ws_thread(ws_uri, loop, end_evt, pq):
    loopt = asyncio.ensure_future(ws_autoreco(ws_uri, pq, loop=loop), loop=loop)
    loop.run_until_complete(asyncio.gather(end_evt.wait(), loopt, loop=loop))


# read from the queue and plot
def get_and_show(dat, line, ax, data_queue):
    try:
        scan, clust = data_queue.get_nowait()
    except Empty:
        return

    plot_scan_and_clusters(line, scan, clust)


def stop(loop, end_evt):
    loop.call_soon_threadsafe(end_evt.set)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    dg = parser.add_mutually_exclusive_group(required=True)
    dg.add_argument('--hokd', metavar='HOKUYO_DEV', nargs='?')
    dg.add_argument('--ws', metavar='WEBSOCKET', nargs='?')
    args = parser.parse_args()

    fig, ax = plt.subplots()
    line_pt, = ax.plot([], [], 'o')
    line_clust, = ax.plot([], [], 'o')
    ax.grid()
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)

    loop = asyncio.SelectorEventLoop()
    end_evt = asyncio.Event(loop=loop)
    data_queue = Queue()

    if args.hokd:
        # poll the serial port mode
        threading.Thread(target=aio_serial_thread,
                         args=(args.hokd, loop, end_evt, data_queue)).start()
    else:
        # listen to aiohttp mode
        threading.Thread(target=aio_ws_thread,
                         args=(args.ws, loop, end_evt, data_queue)).start()

    ani = animation.FuncAnimation(fig, get_and_show,
                                  fargs=([line_pt, line_clust], ax, data_queue), interval=100)
    plt.show()
