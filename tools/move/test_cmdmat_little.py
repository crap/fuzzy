#!/usr/bin/env python3

import asyncio
import aiohttp
import os
import json


import math

matrix = \
    [ #    X           TH
        [1,           1], # M0
        [-1,          1],    # M1
    ]

thm = max([abs(i[1]) for i in matrix])
for i in range(len(matrix)):
    matrix[i][1] /= thm

@asyncio.coroutine
def apply(host):
    # Change config
    dat = { "matrix": matrix }
    r = await aiohttp.put(
        'http://{}:8080/api/boards/move/2/goto/calib'.format(host),
        data=json.dumps(dat))
    value = await r.json()
    r.close()
    print("Code {} data {}".format(r.status, value))

import sys
host = "localhost"
if len(sys.argv) == 2: host = sys.argv[1]
asyncio.get_event_loop().run_until_complete(apply(host))
