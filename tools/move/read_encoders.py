#!/usr/bin/env python3

import os
import sys
import asyncio
import aiohttp
import select


async def read_encoders(session, addr):
    async with session.get('http://%s:8080/api/move/encoders' % addr) as r:
        value = await r.json()
        r.close()
        return value


async def run(gw):
    offset = [0, 0, 0]
    next_reset = True

    async with aiohttp.ClientSession() as session:
        while True:
            coders = await read_encoders(session, gw)

            if next_reset:
                offset = coders
            val = [coders[i] - offset[i] for i in range(len(coders))]

            print("\t".join([str(i) for i in val]))

            next_reset = False

            r, _, _ = select.select((sys.stdin,), (), (), 0.5)
            if sys.stdin in r:
                input()
                next_reset = True

if __name__ == "__main__":
    default_gw = "192.168.7.2"

    if os.path.exists("host"):
        with open("host", "r") as f:
            default_gw = f.read().strip()

    if len(sys.argv) == 2:
        default_gw = sys.argv[1]

    asyncio.get_event_loop().run_until_complete(run(default_gw))
