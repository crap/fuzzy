#!/usr/bin/env python3

import argparse
import asyncio
import aiohttp
import os


async def stop(gw):

    print("Sending stop")
    async with aiohttp.ClientSession() as session:
        async with session.put('http://%s:8080/api/move/stop' % gw) as r:
            value = await r.json()
            r.close()
            print("Code {} data {}".format(r.status, value))


if __name__ == "__main__":
    default_gw = "192.168.233.37"
    if os.path.exists("host"):
        with open("host", "r") as f:
            default_gw = f.read().strip()

    parser = argparse.ArgumentParser()
    parser.add_argument("--gw", "--gateway", default=default_gw)

    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(stop(args.gw))
