#!/usr/bin/env python3

import argparse
import asyncio
import aiohttp
import os
import json


async def move(args):
    async with aiohttp.ClientSession() as session:
        async with session.get(
                'http://%s:8080/api/move/speedconfig' % args.gw) as r:
            value = await r.json()

        if "error" in value:
            print("ERROR: failed to get current value")
            return

        for subs in ('Kp', 'Ki', 'alpha', 'amax', 'vmax'):
            val = args.__getattribute__(subs)
            if val is not None:
                print(value, args.type, subs, val)
                value[subs][args.type] = val

        dat = json.dumps(value, sort_keys=True, indent=4)

        print("Testing new config\n{}".format(dat))
        async with session.put(
                'http://%s:8080/api/move/speedconfig' % args.gw,
                data=dat) as r:
            if r.status != 200:
                print("ERROR ", end="")
            text = await r.text()

        print(text)

if __name__ == "__main__":
    default_gw = "192.168.233.37"
    if os.path.exists("host"):
        with open("host", "r") as f:
            default_gw = f.read().strip()

    parser = argparse.ArgumentParser()
    parser.add_argument("--gw", "--gateway", default=default_gw)

    parser.add_argument("type", choices=["x", "theta"])

    parser.add_argument('--Kp', type=float)
    parser.add_argument('--Ki', type=float)
    parser.add_argument('--alpha', type=float)
    parser.add_argument('--amax', type=float)
    parser.add_argument('--vmax', type=float)

    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(move(args))
