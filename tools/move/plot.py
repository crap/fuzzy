#!/usr/bin/env python3

import sys
import asyncio
import aiohttp
import os


async def fetch(addr):
    async with aiohttp.ClientSession() as session:
        async with session.get("http://%s:8080/api/move/log" % addr) as resp:
            dat = await resp.json()

    with open("/tmp/plot.dat", "w") as f:
        for l in dat:
            f.write(" ".join([str(x) for x in l]) + "\n")

    # format:
    # 1 ix, 2 ref_x, 3 ref_t, 4 val_x, 5 val_t, 6 errsum_x, 7 errsum_t
    # 8 cmd_x, 9 cmd_t
    with open("/tmp/plot.gnu", "w") as f:
        f.write("""
set terminal pdf
set output "/tmp/plot.pdf"

set title "X"
set grid
set y2tics
set xlabel "time (s)"
set ylabel "speed(m/s)"
plot "/tmp/plot.dat" u 1:2 w {t} title "ref", \
    "/tmp/plot.dat" u 1:4 w {t} title "val",  \
    "/tmp/plot.dat" u 1:($2-$4) w {t} title "err",  \
    "/tmp/plot.dat" u 1:6 w {t} axis x1y2 title "err sum"

set title "Theta"
set ylabel "speed(rad/s)"
set y2label
unset y2tics
set grid
plot "/tmp/plot.dat" u 1:3 w {t} title "ref", \
    "/tmp/plot.dat" u 1:5 w {t} title "val", \
    "/tmp/plot.dat" u 1:($3-$5) w {t} title "err",  \
    "/tmp/plot.dat" u 1:7 w {t} axis x1y2 title "err sum"

set title "Command"
set ylabel "val"
set y2label
unset y2tics
set yrange [-1.1:1.1]
set grid
plot "/tmp/plot.dat" u 1:8 w {t} title "X", \
    "/tmp/plot.dat" u 1:9 w {t} title "theta"
        """.format(t='l'))

    os.system("gnuplot /tmp/plot.gnu")


if len(sys.argv) == 1:
    with open("host", "r") as f:
        host = f.read().strip()
else:
    host = sys.argv[1]

asyncio.get_event_loop().run_until_complete(fetch(host))
