#!/usr/bin/env python3

import argparse
import asyncio
import aiohttp
import os
import json


async def raw(session, args):
    dat = {
        "pwm": {
            "x": args.x,
            "theta": args.th,
        },
        "duration": args.duration
        }

    print("Sending raw with dat", dat)
    async with session.put('http://%s:8080/api/move/raw' % args.gw,
                           data=json.dumps(dat)) as r:
        value = await r.json()
        r.close()
        print("Code {} data {}".format(r.status, value))


async def speed(session, args):
    dat = {
        "speed": {
            "x": args.x,
            "theta": args.th,
        },
        "duration": args.duration
        }

    print("Sending speed with dat", dat)
    async with session.put('http://%s:8080/api/move/speed' % args.gw,
                           data=json.dumps(dat)) as r:
        value = await r.json()
        r.close()
        print("Code {} data {}".format(r.status, value))


async def run(args):
    async with aiohttp.ClientSession() as session:
        if args.raw:
            await raw(session, args)
        else:
            await speed(session, args)


if __name__ == "__main__":
    default_gw = "192.168.233.24"
    if os.path.exists("host"):
        with open("host", "r") as f:
            default_gw = f.read().strip()

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--raw", default=False, action="store_true")
    parser.add_argument("x", type=float)
    parser.add_argument("th", type=float)
    parser.add_argument("-d", "--duration", type=float, default=1)
    parser.add_argument("--gw", "--gateway", default=default_gw)

    args = parser.parse_args()
    asyncio.get_event_loop().run_until_complete(run(args))
