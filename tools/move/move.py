#!/usr/bin/env python3

import argparse
import asyncio
import aiohttp
import os
import json
import math


async def move(session, args):
    async with session.get('http://%s:8080/api/move' % args.gw) as r:
        value = await r.json()

    pos = value["position"]
    print("Value", pos)

    r = math.hypot(args.dx, args.dy)
    a = math.atan2(args.dy, args.dx)
    a += pos["theta"]

    dx = r * math.cos(a)
    dy = r * math.sin(a)

    pos["x"] += dx
    pos["y"] += dy
    pos["theta"] += args.dth

    dat = {
        "mode": "line",
        "target": pos,
        "log": True,
        "keep": args.keep,
        "rotate_first": args.rotatefirst,
        "final_rotate": not args.nofinalrotate,
        "backward_allowed": args.backwardallowed
        }

    if args.maxspeed:
        dat["max_speed"] = args.maxspeed

    if args.maxspeedth:
        dat["max_speed_th"] = args.maxspeedth

    if args.dist:
        dat["dist"] = args.dist
    if args.theta:
        dat["theta"] = args.theta
    if args.num:
        dat["num"] = args.num

    print("Sending move with dat", dat)
    async with session.put(
            'http://%s:8080/api/move/to' % args.gw,
            data=json.dumps(dat)) as r:

        value = await r.json()
    print("Code {} data {}".format(r.status, value))


async def run(args):
    async with aiohttp.ClientSession() as session:
        await move(session, args)


if __name__ == "__main__":
    default_gw = "localhost"
    if os.path.exists("host"):
        with open("host", "r") as f:
            default_gw = f.read().strip()

    parser = argparse.ArgumentParser()
    parser.add_argument("dx", type=float)
    parser.add_argument("dy", type=float)
    parser.add_argument("dth", type=float)
    parser.add_argument("--gw", "--gateway", default=default_gw)
    parser.add_argument("--rotatefirst", "-r", default=False,
                        action="store_true")
    parser.add_argument("--backwardallowed", "-b", default=False,
                        action="store_true")
    parser.add_argument("--nofinalrotate", "-n", default=False,
                        action="store_true")
    parser.add_argument("--keep", default=False, action="store_true")
    parser.add_argument("--maxspeed", "--ms", default=None, type=float)
    parser.add_argument("--maxspeedth", "--mst", default=None, type=float)
    parser.add_argument("--dist", default=None, type=float)
    parser.add_argument("--theta", default=None, type=float)
    parser.add_argument("--num", default=None, type=float)

    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(run(args))
