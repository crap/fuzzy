#!/usr/bin/env python3

import asyncio
import aiohttp
import json


import math

# For X and Y
alpha = math.pi / 180 * (50 + 90)
ca = math.cos(alpha)
sa = math.sin(alpha)

# For theta
d0 = 125e-3
d1 = 88e-3
cb = math.cos(13.96/180*math.pi)

matrix = \
    [    # X           Y          TH
        [0,            1,          -d1],     # M0
        [ca,           -sa,        -d0*cb],  # M1
        [-ca,          -sa,        -d0*cb]   # M2
    ]

thm = max([abs(i[2]) for i in matrix])
for i in range(3):
    matrix[i][2] /= thm

print("MATRIX", matrix)

async def apply():
    # Change config
    dat = {"matrix": matrix}
    r = await aiohttp.put(
        'http://localhost:8080/api/boards/move/2/goto/calib',
        data=json.dumps(dat))
    value = await r.json()
    r.close()
    print("Code {} data {}".format(r.status, value))

asyncio.get_event_loop().run_until_complete(apply())
