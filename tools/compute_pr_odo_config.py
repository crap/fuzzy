#!/usr/bin/env python3

import math

angle_rad = 4. * 2 * math.pi
angle_gyro = -2328178

line_m = 0.562
line_codeur = -4744

rel_gyro = 3261652
rel_codeur = -4191

c_y = 0.
c_th = 0.
g_y = 0.

def compute():
    #coef x for codeur
    c_x = line_m / line_codeur

    #coef th for gyro
    g_th = angle_rad / angle_gyro

    #coef x for gyro (compense la roue folle qui n'est pas au milieu)
    g_x = - c_x * rel_codeur / rel_gyro

    return (c_x, g_x, g_th)

c_x, g_x, g_th = compute()
print("codeur: %e %e %e" % (c_x, c_y, c_th))
print("gyro: %e %e %e" % (g_x, g_y, g_th))
