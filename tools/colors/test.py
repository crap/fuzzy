#!/usr/bin/env python3

from colormath.color_objects import sRGBColor, HSLColor
from colormath.color_conversions import convert_color

import numpy as np
from matplotlib import pyplot as plt

files = ["blue", "white", "yellow"]

dat = {}


def gimmecolor(r, g, b):
    rgb = sRGBColor(*l)
    hsv = convert_color(rgb, HSLColor)

    if hsv.hsl_s < 0.3:
        return "WHITE"
    elif hsv.hsl_h > 130:
        return "BLUE"
    else:
        return "YELLOW"

for fname in files:
    dat[fname] = [[], [], []]
    with open(fname, "r") as f:
        for l in f.readlines():
            l = [float(x) for x in l.split()]
            ll = np.array(l)
            # ll /= np.linalg.norm(ll)

            print(fname, gimmecolor(*ll))

            rgb = sRGBColor(*ll)
            hsv = convert_color(rgb, HSLColor)

            dat[fname][0].append(hsv.hsl_h)
            dat[fname][1].append(hsv.hsl_s)
            dat[fname][2].append(hsv.hsl_l)

plt.plot(
    dat["blue"][0], dat["blue"][1], "ob",
    dat["yellow"][0], dat["yellow"][1], "oy",
    dat["white"][0], dat["white"][1], "ok"
    )
plt.xlabel("hue")
plt.ylabel("sat")

plt.show()
